/*insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (1, 'Gutmann', 'Effertz', 'Maximillia Dach', 'M', to_date('01/04/1969', 'DD/MM/YYYY'), to_date('22/04/2016', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '217-370-3466 x91460', 'june71@example.org', 1);*/

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (2, 'Oberbrunner', 'Howell', 'Reed Lindgren III', 'M', to_date('14/11/1961', 'DD/MM/YYYY'), to_date('11/11/2015', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '289-878-3103', 'era.mosciski@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (3, 'McGlynn', 'Fadel', 'Eusebio Hickle I', 'F', to_date('15/11/1946', 'DD/MM/YYYY'), to_date('26/01/2016', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '1-340-730-3760', 'roy.conroy@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (4, 'Ratke', 'Daugherty', 'Tavares Emmerich', 'M', to_date('13/10/1942', 'DD/MM/YYYY'), to_date('16/01/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '528.324.3580 x730', 'kole.mcclure@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (5, 'Beier', 'Sanford', 'Dr. Loma Cronin IV', 'F', to_date('14/12/1961', 'DD/MM/YYYY'), to_date('12/10/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '1-573-339-1076 x6388', 'hodkiewicz.ernesto@example.net',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (6, 'Cummerata', 'Russel', 'Julius Stark', 'M', to_date('25/06/1989', 'DD/MM/YYYY'), to_date('28/01/2016', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '(526) 318-1298', 'blake84@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (7, 'Rempel', 'Adams', 'Florencio Hagenes', 'F', to_date('05/12/1974', 'DD/MM/YYYY'), to_date('03/04/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '1-930-497-0035 x8924', 'delpha75@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (8, 'Harvey', 'D''Amore', 'Rusty Morissette', 'M', to_date('03/06/1956', 'DD/MM/YYYY'), to_date('04/11/2018', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '1-891-938-4600', 'imraz@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (9, 'Kovacek', 'Crist', 'Virgil Kreiger', 'M', to_date('25/09/1941', 'DD/MM/YYYY'), to_date('12/05/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '(801) 461-5703', 'willie74@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (10, 'Schimmel', 'Hammes', 'Pascale Boyer', 'F', to_date('14/10/1951', 'DD/MM/YYYY'), to_date('22/01/2017', 'DD/MM/YYYY'), 'San Jose', 'Limon', '(624) 299-3448', 'nking@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (11, 'Wisoky', 'Luettgen', 'Prof. Jarret Lemke II', 'M', to_date('04/05/1946', 'DD/MM/YYYY'), to_date('25/05/2015', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '785-678-6379 x5202', 'jschoen@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (12, 'West', 'Mertz', 'Ms. Kathryn Auer Sr.', 'F', to_date('12/11/1988', 'DD/MM/YYYY'), to_date('18/04/2018', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '(652) 668-9713 x024', 'hwindler@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (13, 'Deckow', 'Pfannerstill', 'Maurine Buckridge', 'M', to_date('29/11/1940', 'DD/MM/YYYY'), to_date('29/04/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '-5452', 'bogan.avery@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (14, 'Boyer', 'Zemlak', 'Wilhelmine O''Reilly I', 'F', to_date('18/04/1955', 'DD/MM/YYYY'), to_date('18/06/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '4.977.838.007', 'bheathcote@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (15, 'Boehm', 'Ondricka', 'Mr. Braeden Upton', 'M', to_date('29/03/1968', 'DD/MM/YYYY'), to_date('09/03/2012', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '468.972.1536 x47027', 'rosalind.balistreri@example.org',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (16, 'Sawayn', 'Grimes', 'Ms. Eliza Hodkiewicz', 'M', to_date('15/08/1961', 'DD/MM/YYYY'), to_date('31/08/2014', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '2.727.730.543', 'lowe.taurean@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (17, 'Braun', 'Quitzon', 'Nikki Schowalter', 'F', to_date('25/08/2005', 'DD/MM/YYYY'), to_date('24/05/2018', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '+1 (904) 702-8924', 'heaney.carter@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (18, 'Johnston', 'Blanda', 'Miss Sabrina Wehner', 'M', to_date('30/03/2004', 'DD/MM/YYYY'), to_date('10/07/2014', 'DD/MM/YYYY'), 'Liberia', 'Limon', '(775) 309-9656 x616', 'langosh.angeline@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (19, 'Fay', 'Lockman', 'Stuart Klocko', 'F', to_date('21/08/1967', 'DD/MM/YYYY'), to_date('14/04/2010', 'DD/MM/YYYY'), 'Liberia', 'Limon', '(547) 580-9926', 'kareem.prohaska@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (20, 'Will', 'Rodriguez', 'Dr. Vena Walker DVM', 'M', to_date('21/05/1998', 'DD/MM/YYYY'), to_date('08/11/2009', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '1-201-314-7822', 'megane30@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (21, 'Robel', 'Schultz', 'Mr. Ruben Wolff PhD', 'F', to_date('07/10/1993', 'DD/MM/YYYY'), to_date('27/01/2017', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '+1 (846) 417-4155', 'trolfson@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (22, 'Nicolas', 'Gutmann', 'Asha Roob', 'M', to_date('28/10/1966', 'DD/MM/YYYY'), to_date('05/11/2018', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '-1819', 'dimitri91@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (23, 'Ebert', 'Walsh', 'Mr. Scotty Watsica PhD', 'M', to_date('20/06/1953', 'DD/MM/YYYY'), to_date('14/04/2010', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '8.878.141.991', 'felicity56@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (24, 'Boyer', 'Ankunding', 'Tiara Schowalter IV', 'F', to_date('02/09/1965', 'DD/MM/YYYY'), to_date('11/06/2015', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '(875) 665-0019 x2736', 'jana03@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (25, 'Kemmer', 'Gerlach', 'Lou Borer', 'M', to_date('10/05/1996', 'DD/MM/YYYY'), to_date('30/09/2018', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '941.677.4360 x5818', 'johnson.alberto@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (26, 'Kuhic', 'Cummings', 'Katarina Ortiz', 'M', to_date('04/09/1991', 'DD/MM/YYYY'), to_date('10/09/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '+1 (585) 897-5492', 'sporer.alyson@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (27, 'Wilkinson', 'Mohr', 'Prof. Eulah Koelpin IV', 'F', to_date('21/09/1948', 'DD/MM/YYYY'), to_date('28/11/2014', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '3.436.545.833', 'brett.hackett@example.net',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (28, 'Langosh', 'Haley', 'Ms. Patricia Kassulke', 'M', to_date('29/09/1943', 'DD/MM/YYYY'), to_date('13/02/2013', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '485.824.3105 x616', 'uebert@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (29, 'Cole', 'O''Conner', 'Elbert Kozey', 'F', to_date('12/05/1945', 'DD/MM/YYYY'), to_date('06/02/2019', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '541-524-0745 x289', 'dorothy.barrows@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (30, 'Gislason', 'Feest', 'Kiana Romaguera', 'M', to_date('25/02/1967', 'DD/MM/YYYY'), to_date('11/04/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '18.309.959.718', 'sbeier@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (31, 'Mitchell', 'Torphy', 'Cade Swaniawski IV', 'F', to_date('16/12/1939', 'DD/MM/YYYY'), to_date('25/04/2015', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '1-317-336-2439', 'sunny.wilderman@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (32, 'Casper', 'Heathcote', 'Dr. Kyla O''Kon', 'M', to_date('11/06/1969', 'DD/MM/YYYY'), to_date('16/10/2019', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '-9554', 'lmoore@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (33, 'Blanda', 'Torp', 'Edd Kertzmann', 'M', to_date('22/12/1958', 'DD/MM/YYYY'), to_date('17/01/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '(871) 296-8857 x7414', 'brooklyn71@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (34, 'Hill', 'Stark', 'Norval West IV', 'F', to_date('16/03/1988', 'DD/MM/YYYY'), to_date('23/04/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '18.849.756.997', 'arianna57@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (35, 'Keeling', 'Schneider', 'Kaci Fay', 'M', to_date('09/07/1964', 'DD/MM/YYYY'), to_date('30/03/2015', 'DD/MM/YYYY'), 'San Jose', 'Limon', '1-469-884-2771 x1260', 'eden.johnson@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (36, 'Champlin', 'Daugherty', 'Aurelie Kuhlman', 'F', to_date('29/08/1962', 'DD/MM/YYYY'), to_date('27/01/2011', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '8.385.180.666', 'lori93@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (37, 'Pacocha', 'Skiles', 'Torrey Huels', 'M', to_date('29/03/2008', 'DD/MM/YYYY'), to_date('01/05/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '830.975.3404 x53185', 'bblick@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (38, 'Gaylord', 'Herman', 'Terence Turcotte DDS', 'F', to_date('24/12/1981', 'DD/MM/YYYY'), to_date('18/12/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '+1 (431) 953-2243', 'ethelyn.bartoletti@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (39, 'Wiza', 'Harber', 'Mrs. Electa Weimann Jr.', 'M', to_date('15/09/1965', 'DD/MM/YYYY'), to_date('06/03/2014', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '1-630-535-6826 x0217', 'aurelia55@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (40, 'Weber', 'Conroy', 'Audrey Kris V', 'M', to_date('01/09/1941', 'DD/MM/YYYY'), to_date('18/05/2014', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '359.630.6177 x001', 'jonas52@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (41, 'Lang', 'Gerlach', 'Prof. Demetrius Medhurst IV', 'F', to_date('30/09/1941', 'DD/MM/YYYY'), to_date('22/07/2017', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '1-887-952-3676 x41291', 'armstrong.anika@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (42, 'Muller', 'Keeling', 'Prof. Ottis Crooks DVM', 'M', to_date('09/06/1989', 'DD/MM/YYYY'), to_date('24/11/2010', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '1-357-802-7584', 'leo.pacocha@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (43, 'Renner', 'Beier', 'Elmer Yost', 'F', to_date('06/05/1974', 'DD/MM/YYYY'), to_date('05/08/2017', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '368-283-1591 x955', 'tessie74@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (44, 'King', 'Lebsack', 'Mr. Adan Waters III', 'M', to_date('18/12/2002', 'DD/MM/YYYY'), to_date('22/04/2012', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '(263) 898-8245', 'golda08@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (45, 'Strosin', 'Price', 'Miss Pearlie Stehr', 'F', to_date('18/05/1942', 'DD/MM/YYYY'), to_date('31/05/2017', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '647-676-2049 x4643', 'tristin.stark@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (46, 'Hoeger', 'Jacobson', 'Jannie Walsh', 'M', to_date('29/05/1981', 'DD/MM/YYYY'), to_date('06/01/2013', 'DD/MM/YYYY'), 'Esparza', 'Limon', '-5222', 'wmoore@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (47, 'Fay', 'Metz', 'Anastasia Bartoletti', 'M', to_date('28/10/2000', 'DD/MM/YYYY'), to_date('16/09/2014', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '5.743.519.903', 'eortiz@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (48, 'Braun', 'Pacocha', 'Dr. Kenneth Abbott IV', 'F', to_date('14/06/2005', 'DD/MM/YYYY'), to_date('07/04/2014', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '1-837-214-1595 x8357', 'duncan.bernier@example.org',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (49, 'Bashirian', 'Reynolds', 'Nadia Dibbert', 'M', to_date('02/07/2008', 'DD/MM/YYYY'), to_date('03/01/2017', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '1-982-657-2437 x090', 'vena13@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (50, 'King', 'Abernathy', 'Ned Aufderhar', 'M', to_date('28/01/1997', 'DD/MM/YYYY'), to_date('04/10/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '(485) 561-2852', 'piper.terry@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (51, 'Harber', 'Rolfson', 'Milan Haag', 'F', to_date('20/01/1950', 'DD/MM/YYYY'), to_date('30/04/2016', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '667.894.0164 x2327', 'hkling@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (52, 'Strosin', 'Streich', 'Prof. Paris Ortiz DDS', 'M', to_date('20/08/2007', 'DD/MM/YYYY'), to_date('08/01/2013', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '507-806-7940 x0121', 'zakary.rogahn@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (53, 'Torp', 'Leannon', 'Abe Gibson', 'F', to_date('14/07/1944', 'DD/MM/YYYY'), to_date('14/02/2014', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '297-680-6857 x7013', 'bahringer.dorris@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (54, 'Greenholt', 'Nitzsche', 'Prof. Dangelo Ledner V', 'M', to_date('09/01/1966', 'DD/MM/YYYY'), to_date('25/06/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '594-506-0504 x89568', 'jackeline.hand@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (55, 'Strosin', 'Schoen', 'Ms. Mya McKenzie', 'F', to_date('26/07/1960', 'DD/MM/YYYY'), to_date('07/11/2016', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '(663) 543-7477', 'donato82@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (56, 'Hilpert', 'Quitzon', 'Keven Jacobi', 'M', to_date('29/11/1996', 'DD/MM/YYYY'), to_date('03/10/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '(574) 393-6882 x881', 'cgreenholt@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (57, 'Gaylord', 'Turner', 'Destin Reinger', 'M', to_date('19/02/1982', 'DD/MM/YYYY'), to_date('03/08/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '15176986676', 'leatha36@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (58, 'Jerde', 'Eichmann', 'Haylie Crist', 'F', to_date('16/04/1962', 'DD/MM/YYYY'), to_date('09/10/2016', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '7.755.533.379', 'feil.easton@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (59, 'Padberg', 'Monahan', 'Prof. Shana Lueilwitz Sr.', 'M', to_date('20/12/1996', 'DD/MM/YYYY'), to_date('16/02/2015', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '648.969.4430 x2838', 'willow94@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (60, 'Bins', 'Boehm', 'Ellis Langosh', 'F', to_date('13/05/1978', 'DD/MM/YYYY'), to_date('06/11/2014', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '253-756-1082 x0066', 'byost@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (61, 'Carroll', 'Walsh', 'Rosalia Kautzer', 'M', to_date('01/01/1970', 'DD/MM/YYYY'), to_date('05/11/2009', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '865-528-1411 x0988', 'eulalia97@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (62, 'Zulauf', 'Quitzon', 'Ms. Kaci Gulgowski', 'F', to_date('26/06/1975', 'DD/MM/YYYY'), to_date('07/12/2009', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '4.799.833.347', 'noemy70@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (63, 'Gusikowski', 'Bergnaum', 'Virginia Howell', 'M', to_date('07/12/1954', 'DD/MM/YYYY'), to_date('29/03/2016', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '17.923.198.247', 'lbecker@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (64, 'Murphy', 'Stark', 'Reyna Douglas', 'M', to_date('08/07/1954', 'DD/MM/YYYY'), to_date('06/07/2013', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '18.062.214.590', 'dana70@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (65, 'Becker', 'O''Kon', 'Allie Waters', 'M', to_date('04/11/1999', 'DD/MM/YYYY'), to_date('08/04/2011', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '(689) 998-9337 x487', 'senger.yessenia@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (66, 'Rutherford', 'McKenzie', 'Armand Funk', 'F', to_date('24/02/2004', 'DD/MM/YYYY'), to_date('09/06/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '(671) 315-5148', 'abraham89@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (67, 'Greenfelder', 'Simonis', 'Jerald Mayert', 'M', to_date('09/05/1976', 'DD/MM/YYYY'), to_date('06/12/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '(673) 776-6652', 'harrison.pagac@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (68, 'Nicolas', 'Bogisich', 'Romaine Stracke', 'F', to_date('22/09/1945', 'DD/MM/YYYY'), to_date('30/08/2015', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '1-991-317-3351 x8092', 'friesen.leopoldo@example.org',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (69, 'Sawayn', 'Nicolas', 'Kristina Hermiston PhD', 'M', to_date('25/08/1952', 'DD/MM/YYYY'), to_date('09/01/2016', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '765-256-1680 x0103', 'mjacobi@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (70, 'Schmitt', 'Kovacek', 'Meagan Anderson', 'F', to_date('19/04/1988', 'DD/MM/YYYY'), to_date('05/09/2019', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '(842) 594-0329 x5601', 'sophia.howell@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (71, 'Haag', 'Kutch', 'Ernie Nader I', 'M', to_date('05/12/1957', 'DD/MM/YYYY'), to_date('12/12/2009', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '13916481156', 'lschinner@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (72, 'Littel', 'Steuber', 'Gustave Sipes', 'M', to_date('06/05/1967', 'DD/MM/YYYY'), to_date('10/06/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '+1 (642) 746-5454', 'rhand@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (73, 'Mertz', 'Champlin', 'Prof. Gino Gleichner IV', 'F', to_date('14/01/1997', 'DD/MM/YYYY'), to_date('20/11/2010', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '394.846.2935 x9044', 'nkulas@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (74, 'Boyer', 'Ziemann', 'Domenick Stroman', 'M', to_date('22/09/1950', 'DD/MM/YYYY'), to_date('10/07/2010', 'DD/MM/YYYY'), 'San Jose', 'Limon', '1-450-890-1296 x59301', 'lillie.ullrich@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (75, 'Stehr', 'Prosacco', 'Mrs. Anissa Grimes Sr.', 'F', to_date('16/04/1985', 'DD/MM/YYYY'), to_date('17/12/2009', 'DD/MM/YYYY'), 'San Jose', 'Limon', '14.292.557.090', 'lerdman@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (76, 'Gottlieb', 'Kunde', 'Kristoffer Kerluke', 'M', to_date('18/01/1951', 'DD/MM/YYYY'), to_date('10/09/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '3.572.287.531', 'kaci.ryan@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (77, 'Kunze', 'Johnson', 'Effie Daugherty', 'F', to_date('23/09/1946', 'DD/MM/YYYY'), to_date('21/01/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '(406) 358-5874', 'sigmund.marks@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (78, 'Okuneva', 'Bergnaum', 'Prof. Jesse Schmitt Sr.', 'M', to_date('30/04/1957', 'DD/MM/YYYY'), to_date('12/07/2013', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '(903) 333-2127 x8160', 'alexanne.doyle@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (79, 'King', 'Hyatt', 'Terence Dare', 'M', to_date('18/12/1969', 'DD/MM/YYYY'), to_date('24/12/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '-3432', 'delta76@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (80, 'Parker', 'Rice', 'Jon Kiehn', 'F', to_date('06/09/1985', 'DD/MM/YYYY'), to_date('09/04/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '6.075.180.757', 'qnolan@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (81, 'Zemlak', 'Miller', 'Augusta Larson III', 'M', to_date('20/12/2004', 'DD/MM/YYYY'), to_date('21/02/2010', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '402.878.6772 x218', 'ryan.emmett@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (82, 'Stamm', 'Mayert', 'Prof. Tod McGlynn', 'F', to_date('10/10/1972', 'DD/MM/YYYY'), to_date('18/11/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '847-733-4676', 'kfarrell@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (83, 'Crooks', 'Labadie', 'Joseph Hintz', 'M', to_date('27/03/1951', 'DD/MM/YYYY'), to_date('20/09/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '225-284-1961 x978', 'hillary.emard@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (84, 'Sipes', 'Cruickshank', 'Zella Orn IV', 'F', to_date('29/09/1987', 'DD/MM/YYYY'), to_date('21/02/2011', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '(874) 406-2604', 'wilhelmine.predovic@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (85, 'Konopelski', 'Yundt', 'Dr. Randi Yost', 'M', to_date('11/02/1961', 'DD/MM/YYYY'), to_date('03/05/2017', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '783-310-0575', 'mconroy@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (86, 'Hyatt', 'Schimmel', 'Camylle Barrows', 'M', to_date('20/02/2009', 'DD/MM/YYYY'), to_date('17/01/2017', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '(514) 598-0491 x812', 'nanderson@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (87, 'Moen', 'Lindgren', 'Donato Hoppe', 'F', to_date('15/10/1987', 'DD/MM/YYYY'), to_date('03/02/2011', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '1-692-894-0696 x923', 'sawayn.dorris@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (88, 'Botsford', 'Metz', 'Ronny Maggio', 'M', to_date('22/04/1996', 'DD/MM/YYYY'), to_date('18/03/2015', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '439-384-8374', 'uosinski@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (89, 'Considine', 'Pacocha', 'Willard Lang', 'M', to_date('11/11/1975', 'DD/MM/YYYY'), to_date('22/01/2010', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '18.796.248.134', 'mante.rosalinda@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (90, 'Douglas', 'Frami', 'Allen Emard', 'F', to_date('26/04/1970', 'DD/MM/YYYY'), to_date('28/10/2012', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '+1 (968) 294-9487', 'miller.remington@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (91, 'Schmidt', 'Rau', 'Mrs. Layla Hauck', 'M', to_date('04/04/1969', 'DD/MM/YYYY'), to_date('01/09/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '593.338.6278 x565', 'hnitzsche@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (92, 'Thiel', 'Herzog', 'Josue Kris', 'F', to_date('13/04/1988', 'DD/MM/YYYY'), to_date('20/02/2013', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '7.106.309.948', 'hayley.rutherford@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (93, 'Brown', 'Stamm', 'Howard Effertz', 'M', to_date('25/01/1954', 'DD/MM/YYYY'), to_date('28/03/2010', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '(206) 926-0752 x4824', 'harris.joan@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (94, 'Farrell', 'Pollich', 'Pattie Aufderhar', 'M', to_date('24/12/1977', 'DD/MM/YYYY'), to_date('17/01/2015', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '8.535.011.473', 'quinten.mertz@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (95, 'Pacocha', 'Jacobson', 'Shanie Connelly PhD', 'F', to_date('29/06/1985', 'DD/MM/YYYY'), to_date('13/07/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '1-921-768-0247 x5246', 'qabernathy@example.com',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (96, 'Larson', 'Adams', 'Darren Schowalter Sr.', 'M', to_date('17/02/2005', 'DD/MM/YYYY'), to_date('30/01/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '-9526', 'ewilkinson@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (97, 'Gusikowski', 'Emmerich', 'Eleanora Lebsack', 'F', to_date('11/10/1969', 'DD/MM/YYYY'), to_date('07/05/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '946-893-4753 x478', 'dkemmer@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (98, 'Maggio', 'Terry', 'Elvie Balistreri', 'M', to_date('01/08/1998', 'DD/MM/YYYY'), to_date('06/07/2013', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '1-683-487-8568', 'kuhn.ocie@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (99, 'Corkery', 'Krajcik', 'Donny Goyette', 'F', to_date('14/08/1952', 'DD/MM/YYYY'), to_date('30/07/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '507-615-9038 x217', 'milton.morissette@example.com',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (100, 'Oberbrunner', 'Wilderman', 'Kale Crooks', 'M', to_date('25/10/1982', 'DD/MM/YYYY'), to_date('23/05/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '-3679', 'koch.vesta@example.com', 6);

commit;

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (101, 'Douglas', 'Kris', 'Karianne Bogan', 'M', to_date('30/03/1965', 'DD/MM/YYYY'), to_date('01/02/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '339-353-9010', 'brody27@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (102, 'Carter', 'Romaguera', 'Prof. Glen Mraz', 'F', to_date('22/11/1987', 'DD/MM/YYYY'), to_date('29/11/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '18.436.318.976', 'leannon.carmella@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (103, 'Romaguera', 'Buckridge', 'Adelle Lakin DDS', 'M', to_date('22/06/1979', 'DD/MM/YYYY'), to_date('26/06/2017', 'DD/MM/YYYY'), 'Esparza', 'Limon', '6.252.108.647', 'gweissnat@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (104, 'Corwin', 'Volkman', 'Hilton Kreiger', 'F', to_date('18/05/1942', 'DD/MM/YYYY'), to_date('13/05/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '19689427851', 'allie85@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (105, 'Heidenreich', 'Hodkiewicz', 'Owen Feil', 'M', to_date('21/06/1946', 'DD/MM/YYYY'), to_date('18/09/2014', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '1-782-694-0698 x052', 'zack.gleichner@example.org',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (106, 'Pagac', 'Ankunding', 'Mortimer Stokes', 'F', to_date('26/07/1999', 'DD/MM/YYYY'), to_date('26/07/2018', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '1-648-594-1939 x7259', 'stiedemann.rebeca@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (107, 'Braun', 'Schmitt', 'Isabella Lang DDS', 'M', to_date('02/04/1947', 'DD/MM/YYYY'), to_date('07/03/2018', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '(421) 247-3420', 'schneider.danny@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (108, 'Corwin', 'Oberbrunner', 'Dr. Claudie Gutkowski MD', 'M', to_date('28/01/1948', 'DD/MM/YYYY'), to_date('05/04/2014', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '774.540.3931 x03378', 'zhowell@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (109, 'Kiehn', 'Keeling', 'Ms. Catharine Gutkowski V', 'F', to_date('19/06/1992', 'DD/MM/YYYY'), to_date('21/08/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '17509615929', 'ruthie04@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (110, 'Gorczany', 'Aufderhar', 'Myra Rogahn', 'M', to_date('25/01/1980', 'DD/MM/YYYY'), to_date('02/04/2011', 'DD/MM/YYYY'), 'Liberia', 'Limon', '(517) 863-7174 x935', 'hschroeder@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (111, 'Gorczany', 'Hauck', 'Miss Magali Bednar', 'F', to_date('31/05/1998', 'DD/MM/YYYY'), to_date('14/04/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '861-896-1295 x1874', 'herbert21@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (112, 'Hoppe', 'Marks', 'Dr. Jody Raynor', 'M', to_date('14/10/1982', 'DD/MM/YYYY'), to_date('31/03/2013', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '+1 (949) 496-0397', 'carole.sauer@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (113, 'Pfeffer', 'Jenkins', 'Roman Larson', 'F', to_date('27/08/1995', 'DD/MM/YYYY'), to_date('17/08/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '18.062.706.755', 'sallie59@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (114, 'Koepp', 'Anderson', 'Asa Dickens', 'M', to_date('14/07/2004', 'DD/MM/YYYY'), to_date('15/10/2011', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '1-417-388-4961', 'mturner@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (115, 'Rempel', 'Koss', 'Josiane Kuhlman', 'M', to_date('01/06/1942', 'DD/MM/YYYY'), to_date('20/08/2011', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '1-476-292-1995 x649', 'anderson.vida@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (116, 'Yundt', 'Strosin', 'Estel Fritsch', 'F', to_date('16/01/1955', 'DD/MM/YYYY'), to_date('16/06/2012', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '1-225-470-6621 x312', 'ethel28@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (117, 'Cummings', 'Kirlin', 'Odie Streich', 'M', to_date('01/10/2007', 'DD/MM/YYYY'), to_date('21/10/2016', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '(786) 905-7771 x33227', 'vshields@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (118, 'Schmitt', 'Von', 'Miss Addison Rippin', 'M', to_date('07/07/1958', 'DD/MM/YYYY'), to_date('16/05/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '653.568.8752 x6435', 'filiberto.lowe@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (119, 'Collins', 'Daniel', 'Prof. Theodore Emard DDS', 'F', to_date('25/07/1963', 'DD/MM/YYYY'), to_date('21/05/2016', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '1-706-202-3618', 'edgar36@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (120, 'Gislason', 'Rippin', 'Dr. Jayson Powlowski V', 'M', to_date('18/06/1959', 'DD/MM/YYYY'), to_date('04/03/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '(420) 512-0722', 'nbednar@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (121, 'Walsh', 'Kulas', 'Chasity Collins', 'F', to_date('27/12/2005', 'DD/MM/YYYY'), to_date('07/08/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '1-463-940-5125 x289', 'octavia.gusikowski@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (122, 'Rutherford', 'Jakubowski', 'Barney Grimes', 'M', to_date('18/10/1987', 'DD/MM/YYYY'), to_date('20/10/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '925-769-9689 x3681', 'may.lind@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (123, 'Beer', 'Rutherford', 'Prof. Constance Sipes Jr.', 'F', to_date('04/03/1954', 'DD/MM/YYYY'), to_date('08/05/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '1-976-614-3007 x124', 'qvonrueden@example.org',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (124, 'Reinger', 'Parisian', 'Courtney Spinka', 'M', to_date('15/02/1972', 'DD/MM/YYYY'), to_date('30/06/2010', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '(323) 920-9165 x84936', 'robbie.shields@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (125, 'Robel', 'Wilderman', 'Mr. Javier Dare II', 'M', to_date('25/04/2008', 'DD/MM/YYYY'), to_date('22/08/2010', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '-2821', 'nicolas.aditya@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (126, 'Rowe', 'Hackett', 'Cruz Keeling III', 'F', to_date('13/01/1952', 'DD/MM/YYYY'), to_date('12/01/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '2.763.870.279', 'poberbrunner@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (127, 'Reichel', 'Tremblay', 'Miss Katheryn Larson', 'M', to_date('07/02/2004', 'DD/MM/YYYY'), to_date('01/03/2019', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '9.399.584.346', 'braden09@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (128, 'Ruecker', 'Funk', 'Lisette Maggio', 'F', to_date('07/11/1958', 'DD/MM/YYYY'), to_date('28/06/2010', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '281-874-1465 x243', 'davis.antonia@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (129, 'Hickle', 'Boyle', 'Prof. Gregoria Powlowski IV', 'M', to_date('29/03/1957', 'DD/MM/YYYY'), to_date('13/07/2010', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '5.912.077.937', 'allene82@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (130, 'Hayes', 'Cole', 'Ms. Verdie Legros', 'F', to_date('19/06/1940', 'DD/MM/YYYY'), to_date('19/09/2013', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '9.085.954.884', 'savannah.stiedemann@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (131, 'Ryan', 'Zulauf', 'Mr. Dante Olson', 'M', to_date('24/10/1986', 'DD/MM/YYYY'), to_date('22/09/2017', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '(459) 259-7312 x3852', 'nils45@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (132, 'Grant', 'Weissnat', 'Sophie Will', 'M', to_date('17/06/1947', 'DD/MM/YYYY'), to_date('28/06/2011', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '19569053377', 'huels.wayne@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (133, 'Gleason', 'Rempel', 'Mr. Marcelo Barton', 'F', to_date('25/01/1996', 'DD/MM/YYYY'), to_date('16/02/2019', 'DD/MM/YYYY'), 'San Jose', 'Limon', '(282) 368-9248', 'leuschke.cristina@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (134, 'Bashirian', 'Deckow', 'Marielle Emard', 'M', to_date('24/04/1989', 'DD/MM/YYYY'), to_date('16/07/2012', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '(663) 960-9089 x865', 'franecki.zena@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (135, 'Wolff', 'Ferry', 'Prof. Mazie Bechtelar IV', 'F', to_date('21/09/1961', 'DD/MM/YYYY'), to_date('31/12/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '285-621-2802 x4765', 'oral.christiansen@example.org',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (136, 'Rowe', 'Ferry', 'Missouri Pfannerstill', 'M', to_date('07/12/1987', 'DD/MM/YYYY'), to_date('16/05/2012', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '208-904-6233 x55773', 'ereichel@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (137, 'Gusikowski', 'Borer', 'Prof. Josue Medhurst PhD', 'F', to_date('23/11/1955', 'DD/MM/YYYY'), to_date('04/06/2018', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '1-413-964-6362 x409', 'cornelius80@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (138, 'Johns', 'Okuneva', 'Matilde Fisher', 'M', to_date('03/04/1967', 'DD/MM/YYYY'), to_date('04/11/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '950-276-6604 x239', 'luella87@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (139, 'Veum', 'Reichert', 'Lou Beer', 'M', to_date('24/03/1974', 'DD/MM/YYYY'), to_date('07/08/2015', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '940.622.9943 x1158', 'susan92@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (140, 'Sawayn', 'Haley', 'Elfrieda Willms', 'F', to_date('16/10/1987', 'DD/MM/YYYY'), to_date('14/11/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '863-950-1607', 'lindsay81@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (141, 'Pollich', 'Hettinger', 'Norene Purdy DVM', 'M', to_date('17/07/1963', 'DD/MM/YYYY'), to_date('28/09/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '846-968-9053', 'justyn.bergnaum@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (142, 'Kassulke', 'Gutmann', 'Elena Jacobs PhD', 'M', to_date('02/12/1964', 'DD/MM/YYYY'), to_date('21/03/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '994-410-8551 x3272', 'ohara.orin@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (143, 'Russel', 'Bahringer', 'Sandy Lebsack', 'F', to_date('19/01/1968', 'DD/MM/YYYY'), to_date('26/07/2011', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '14.243.692.742', 'jordi43@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (144, 'Johnson', 'Satterfield', 'Dock Hayes', 'M', to_date('29/05/1995', 'DD/MM/YYYY'), to_date('03/02/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '764-324-2901', 'ethel21@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (145, 'Simonis', 'Kilback', 'Mr. Harmon Wintheiser', 'F', to_date('13/05/1996', 'DD/MM/YYYY'), to_date('12/11/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '847-917-1605 x83164', 'april32@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (146, 'Hilpert', 'Turner', 'Ethan Marvin', 'M', to_date('07/10/1965', 'DD/MM/YYYY'), to_date('29/09/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '16.355.586.632', 'pheaney@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (147, 'Block', 'Connelly', 'Mekhi Stiedemann', 'F', to_date('18/07/1990', 'DD/MM/YYYY'), to_date('31/05/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '1-330-267-0501', 'nathen83@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (148, 'Smith', 'Sanford', 'Prof. Shakira Ruecker DVM', 'M', to_date('13/08/1974', 'DD/MM/YYYY'), to_date('02/06/2017', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '14.378.618.262', 'devon34@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (149, 'Lind', 'McLaughlin', 'Prof. Melvina Connelly I', 'M', to_date('11/01/2008', 'DD/MM/YYYY'), to_date('26/07/2013', 'DD/MM/YYYY'), 'San Jose', 'Limon', '+1 (863) 476-7335', 'treva.weimann@example.net',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (150, 'Jast', 'Leannon', 'Kaitlyn Corwin', 'F', to_date('12/10/1947', 'DD/MM/YYYY'), to_date('23/07/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '985-350-8478 x3886', 'wmedhurst@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (151, 'Huel', 'Blick', 'Dr. April Auer Sr.', 'M', to_date('02/05/1983', 'DD/MM/YYYY'), to_date('07/11/2012', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '(945) 399-0106 x0460', 'georgiana52@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (152, 'Batz', 'Kulas', 'Derek Gutmann III', 'F', to_date('17/05/1991', 'DD/MM/YYYY'), to_date('20/11/2009', 'DD/MM/YYYY'), 'Esparza', 'Limon', '1-319-584-6071', 'kane28@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (153, 'Berge', 'Crist', 'Prof. Hailey Walker', 'M', to_date('09/12/2001', 'DD/MM/YYYY'), to_date('09/09/2019', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '5.014.355.343', 'giuseppe68@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (154, 'Wintheiser', 'Stanton', 'Ila Wunsch', 'F', to_date('23/04/1993', 'DD/MM/YYYY'), to_date('17/01/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '2.969.045.379', 'bashirian.hollis@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (155, 'Halvorson', 'Effertz', 'Oma Hills', 'M', to_date('02/04/1965', 'DD/MM/YYYY'), to_date('09/04/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '309-602-2064 x158', 'bobbie.feest@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (156, 'Turcotte', 'Barton', 'Alexandrine Bayer Sr.', 'M', to_date('01/01/1952', 'DD/MM/YYYY'), to_date('04/10/2018', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '1-449-339-6367', 'herminio35@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (157, 'Terry', 'Schultz', 'Mr. Roberto Hodkiewicz IV', 'M', to_date('30/11/2006', 'DD/MM/YYYY'), to_date('03/03/2012', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '(804) 805-5137', 'torphy.torrance@example.net',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (158, 'Miller', 'Spinka', 'Libbie Hilpert MD', 'F', to_date('31/12/1992', 'DD/MM/YYYY'), to_date('18/11/2018', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '1-854-296-7977', 'dolores84@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (159, 'Toy', 'Wiegand', 'Joshua Beatty', 'M', to_date('22/11/1951', 'DD/MM/YYYY'), to_date('02/10/2019', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '1-513-383-3312 x184', 'hermann.clarabelle@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (160, 'Williamson', 'Nader', 'Ms. Jadyn Hagenes', 'F', to_date('29/06/1974', 'DD/MM/YYYY'), to_date('27/04/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '4.245.019.782', 'yparisian@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (161, 'Bernier', 'Steuber', 'Sabrina Wehner', 'M', to_date('03/11/1997', 'DD/MM/YYYY'), to_date('25/03/2014', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '7.708.359.450', 'samson.sauer@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (162, 'Ruecker', 'Bayer', 'Claudine Howe I', 'F', to_date('12/04/1975', 'DD/MM/YYYY'), to_date('09/10/2018', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '(547) 823-8436 x4378', 'rice.eudora@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (163, 'Hills', 'Lang', 'Merritt Bahringer', 'M', to_date('28/07/1944', 'DD/MM/YYYY'), to_date('11/12/2010', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '(812) 248-4319 x7395', 'samantha.lueilwitz@example.org',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (164, 'McClure', 'Marvin', 'Kristian Mueller', 'M', to_date('25/06/1976', 'DD/MM/YYYY'), to_date('15/10/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '9.654.473.201', 'baufderhar@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (165, 'Cummerata', 'Brakus', 'Miss Lyla Stracke I', 'F', to_date('19/05/1980', 'DD/MM/YYYY'), to_date('11/01/2015', 'DD/MM/YYYY'), 'Liberia', 'Limon', '(789) 933-2912 x9419', 'sanford.veum@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (166, 'Grimes', 'Weimann', 'Ms. Stephanie Jacobs DDS', 'M', to_date('09/10/2002', 'DD/MM/YYYY'), to_date('01/04/2014', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '12.404.675.470', 'rcassin@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (167, 'Zemlak', 'Von', 'Mr. Jayden Hill', 'F', to_date('09/11/1974', 'DD/MM/YYYY'), to_date('11/05/2019', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '686-533-7390', 'collier.ayla@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (168, 'Ortiz', 'Gulgowski', 'Anderson Hand', 'M', to_date('10/12/2001', 'DD/MM/YYYY'), to_date('03/09/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '+1 (226) 426-0334', 'hhartmann@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (169, 'Ebert', 'Torp', 'Bud Kris', 'F', to_date('24/03/1986', 'DD/MM/YYYY'), to_date('30/04/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '18079861745', 'aditya.flatley@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (170, 'Bernier', 'Hyatt', 'April Stoltenberg IV', 'M', to_date('01/02/1952', 'DD/MM/YYYY'), to_date('27/03/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '741-529-5758', 'ardith36@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (171, 'Rutherford', 'Smitham', 'Ms. Petra Runolfsson V', 'M', to_date('28/04/1994', 'DD/MM/YYYY'), to_date('06/02/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '(247) 264-8240 x34906', 'dawn40@example.net',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (172, 'Hettinger', 'Reinger', 'Miss Vincenza Cartwright PhD', 'F', to_date('23/07/1983', 'DD/MM/YYYY'), to_date('20/11/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '(716) 809-3107 x17102', 'fjacobs@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (173, 'Ankunding', 'Fritsch', 'Lucienne Flatley DVM', 'M', to_date('28/02/1973', 'DD/MM/YYYY'), to_date('02/05/2013', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '623.651.6155 x372', 'lfranecki@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (174, 'Reinger', 'Stamm', 'Lolita Dickinson', 'F', to_date('29/05/1974', 'DD/MM/YYYY'), to_date('04/07/2015', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '358-948-4054 x944', 'mcdermott.violet@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (175, 'Sporer', 'Gleichner', 'Travis Macejkovic', 'M', to_date('15/01/1943', 'DD/MM/YYYY'), to_date('18/12/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '+1 (921) 718-0311', 'salvador.hirthe@example.org',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (176, 'Paucek', 'Gerhold', 'Prof. Joy Olson PhD', 'F', to_date('24/06/1957', 'DD/MM/YYYY'), to_date('02/08/2010', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '12.684.668.666', 'dickens.stephan@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (177, 'Moen', 'Kilback', 'Immanuel Johnston Sr.', 'M', to_date('28/01/1998', 'DD/MM/YYYY'), to_date('01/05/2018', 'DD/MM/YYYY'), 'San Jose', 'Limon', '656.903.2767 x9533', 'pgreen@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (178, 'Ryan', 'Jast', 'Henry Deckow', 'M', to_date('21/05/1993', 'DD/MM/YYYY'), to_date('13/09/2018', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '(323) 689-0219', 'ali.torphy@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (179, 'Hickle', 'Sipes', 'Prof. Sasha Armstrong', 'F', to_date('14/09/1976', 'DD/MM/YYYY'), to_date('02/06/2018', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '18798593230', 'obie.kunze@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (180, 'Prosacco', 'Kautzer', 'Nick Stroman', 'M', to_date('25/04/1974', 'DD/MM/YYYY'), to_date('02/11/2010', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '(426) 669-0266', 'hyman.glover@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (181, 'Schmidt', 'Dickens', 'Rod Gutkowski', 'M', to_date('13/09/1959', 'DD/MM/YYYY'), to_date('27/07/2016', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '898.823.3431 x28011', 'tanner40@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (182, 'Kuhn', 'Witting', 'Prof. Sister Runte MD', 'F', to_date('25/04/1975', 'DD/MM/YYYY'), to_date('11/07/2011', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '13.783.411.020', 'turcotte.enos@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (183, 'Wyman', 'Blanda', 'Herminia Schmeler', 'M', to_date('16/12/1952', 'DD/MM/YYYY'), to_date('23/07/2010', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '854-265-3866 x015', 'gislason.lurline@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (184, 'Wisoky', 'Weissnat', 'Abagail Berge', 'F', to_date('07/09/1989', 'DD/MM/YYYY'), to_date('23/02/2015', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '889.516.0256 x498', 'jakob34@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (185, 'Denesik', 'Baumbach', 'Otto Batz', 'M', to_date('08/05/1968', 'DD/MM/YYYY'), to_date('16/06/2013', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '690.552.3453 x65839', 'jaylan.rice@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (186, 'Witting', 'Grady', 'Prof. Gay Reinger DDS', 'M', to_date('15/06/1983', 'DD/MM/YYYY'), to_date('10/10/2015', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '(573) 273-0617 x2720', 'terrill.hane@example.com',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (187, 'Kerluke', 'Brakus', 'Jennyfer Stokes', 'F', to_date('12/10/1998', 'DD/MM/YYYY'), to_date('11/10/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '873-328-8451 x5586', 'rhowe@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (188, 'Klein', 'Ziemann', 'Shanie Hegmann', 'M', to_date('16/02/1949', 'DD/MM/YYYY'), to_date('17/08/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '(595) 505-6509 x0063', 'ubauch@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (189, 'Koss', 'Rosenbaum', 'Georgianna Schoen', 'F', to_date('17/11/1997', 'DD/MM/YYYY'), to_date('09/02/2013', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '+1 (487) 710-5134', 'isaiah.hill@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (190, 'Eichmann', 'Lemke', 'Dr. Regan Pagac', 'M', to_date('17/01/1984', 'DD/MM/YYYY'), to_date('02/03/2010', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '(817) 674-3739 x7649', 'otilia07@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (191, 'Hartmann', 'Krajcik', 'Mrs. Kailyn Toy PhD', 'F', to_date('20/05/1969', 'DD/MM/YYYY'), to_date('01/01/2011', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '+1 (956) 474-1406', 'halvorson.nettie@example.org',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (192, 'Crist', 'Bruen', 'Vladimir Little', 'M', to_date('23/06/1981', 'DD/MM/YYYY'), to_date('25/01/2012', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '(574) 535-0849', 'kunde.theresa@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (193, 'Hagenes', 'Torp', 'Esta Cummerata', 'M', to_date('20/06/2003', 'DD/MM/YYYY'), to_date('23/02/2011', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '634.570.2099 x1704', 'xdavis@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (194, 'Cartwright', 'Bartoletti', 'Alanis Luettgen V', 'F', to_date('03/01/2004', 'DD/MM/YYYY'), to_date('25/02/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '6.496.553.570', 'angeline.price@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (195, 'Gaylord', 'Morar', 'Miss Orpha Schuppe', 'M', to_date('16/03/1972', 'DD/MM/YYYY'), to_date('09/01/2013', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '-10801', 'quigley.carole@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (196, 'Douglas', 'Turcotte', 'Maximilian O''Connell', 'F', to_date('16/08/1952', 'DD/MM/YYYY'), to_date('19/05/2011', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '260.455.4547 x831', 'ronaldo.bartoletti@example.net',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (197, 'Homenick', 'Will', 'Guadalupe Johnson', 'M', to_date('18/06/1986', 'DD/MM/YYYY'), to_date('28/12/2011', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '323-610-4932 x7082', 'bbogisich@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (198, 'Doyle', 'Effertz', 'Jesus Labadie IV', 'F', to_date('09/05/1950', 'DD/MM/YYYY'), to_date('09/08/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '907-739-9697 x01947', 'skautzer@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (199, 'Ondricka', 'West', 'Pauline Bins', 'M', to_date('13/11/1944', 'DD/MM/YYYY'), to_date('10/02/2017', 'DD/MM/YYYY'), 'San Jose', 'Limon', '(395) 881-5790 x746', 'bud.runolfsson@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (200, 'Luettgen', 'Hamill', 'Neoma Zieme', 'M', to_date('01/09/1979', 'DD/MM/YYYY'), to_date('11/12/2012', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '+1 (786) 943-6156', 'jarret.gusikowski@example.com', 1);

commit;

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (201, 'Hudson', 'Powlowski', 'Cameron Daniel', 'F', to_date('12/03/1968', 'DD/MM/YYYY'), to_date('15/03/2019', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '7.735.208.872', 'mitchell.summer@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (202, 'Stamm', 'Green', 'Shanie Kulas II', 'M', to_date('22/12/1971', 'DD/MM/YYYY'), to_date('17/05/2017', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '1-989-309-9008', 'jaleel99@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (203, 'Purdy', 'Gulgowski', 'Prof. Kayleigh Hudson', 'F', to_date('01/12/1984', 'DD/MM/YYYY'), to_date('12/04/2019', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '524-384-2721 x0342', 'mlebsack@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (204, 'Miller', 'Hessel', 'Rebecca Weissnat', 'M', to_date('03/10/1996', 'DD/MM/YYYY'), to_date('14/06/2019', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '(763) 859-4703 x47486', 'ardella16@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (205, 'Lakin', 'Jenkins', 'Donato Kuhic', 'F', to_date('11/04/2003', 'DD/MM/YYYY'), to_date('11/10/2010', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '975-396-1336 x8826', 'kayley79@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (206, 'Ziemann', 'Strosin', 'Ms. Shayna Herman', 'M', to_date('06/06/1956', 'DD/MM/YYYY'), to_date('24/06/2017', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '1-801-753-1217', 'sluettgen@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (207, 'Barton', 'Zemlak', 'Henderson Hartmann', 'M', to_date('03/04/1940', 'DD/MM/YYYY'), to_date('15/06/2012', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '-6025', 'rick75@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (208, 'Waelchi', 'Boyle', 'Rozella Dooley IV', 'F', to_date('04/06/1975', 'DD/MM/YYYY'), to_date('07/06/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '-6249', 'maggio.fidel@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (209, 'Feest', 'Gleason', 'Mikel Kihn PhD', 'M', to_date('12/01/1996', 'DD/MM/YYYY'), to_date('21/10/2013', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '1-272-243-2863 x134', 'merlin.schaefer@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (210, 'Bogisich', 'Boehm', 'Domenick Rosenbaum', 'M', to_date('22/12/1986', 'DD/MM/YYYY'), to_date('18/05/2012', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '229-598-5563', 'melody.kuhn@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (211, 'Rippin', 'Sipes', 'Taryn Casper', 'F', to_date('21/04/2003', 'DD/MM/YYYY'), to_date('14/07/2014', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '773-395-7371', 'sebastian.gulgowski@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (212, 'Quigley', 'Hermann', 'Cristal Funk III', 'M', to_date('04/02/1945', 'DD/MM/YYYY'), to_date('29/12/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '1-209-766-2221', 'nprice@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (213, 'Feil', 'Schinner', 'Ms. Eda Jakubowski', 'F', to_date('29/07/1996', 'DD/MM/YYYY'), to_date('02/01/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '(601) 698-3527', 'bbradtke@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (214, 'Schmidt', 'Grant', 'Mr. Bertrand Boyle', 'M', to_date('21/01/1999', 'DD/MM/YYYY'), to_date('07/02/2015', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '393-207-3244 x60511', 'schamberger.jeanette@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (215, 'Erdman', 'Schumm', 'Kaya Blanda', 'F', to_date('15/12/1985', 'DD/MM/YYYY'), to_date('23/04/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '+1 (646) 668-3788', 'murray.green@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (216, 'Schaden', 'Funk', 'Vince Wiegand DDS', 'M', to_date('13/03/1978', 'DD/MM/YYYY'), to_date('30/10/2018', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '1-856-629-6563 x93478', 'kris.whitney@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (217, 'Watsica', 'Gerlach', 'Fatima Denesik', 'M', to_date('19/05/1986', 'DD/MM/YYYY'), to_date('14/12/2009', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '(414) 701-9701 x6127', 'wilderman.hortense@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (218, 'Spencer', 'Kling', 'Miss Bernice Ondricka III', 'F', to_date('29/05/1991', 'DD/MM/YYYY'), to_date('11/04/2017', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '6.705.835.833', 'ludie.jaskolski@example.net',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (219, 'Christiansen', 'O''Hara', 'Ruby Gusikowski', 'M', to_date('03/09/1986', 'DD/MM/YYYY'), to_date('30/01/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '19.349.720.546', 'murphy.sonny@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (220, 'Hill', 'Pfannerstill', 'Prof. Kyla Willms MD', 'F', to_date('21/10/2008', 'DD/MM/YYYY'), to_date('23/08/2015', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '467.255.6116 x28454', 'elaina25@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (221, 'Koepp', 'Hegmann', 'Mr. Nasir Berge Sr.', 'M', to_date('05/04/1983', 'DD/MM/YYYY'), to_date('11/05/2017', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '579-457-5602', 'qjast@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (222, 'Carter', 'Hudson', 'Mrs. Eulalia Beer', 'F', to_date('17/12/2007', 'DD/MM/YYYY'), to_date('30/03/2019', 'DD/MM/YYYY'), 'San Jose', 'Limon', '(963) 569-6671 x5343', 'king.keely@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (223, 'Towne', 'Gibson', 'Noemie Steuber', 'M', to_date('22/05/2008', 'DD/MM/YYYY'), to_date('15/07/2012', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '(252) 652-0463 x570', 'schamberger.caitlyn@example.org',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (224, 'Blick', 'Gleason', 'Lurline Kuhic', 'M', to_date('11/02/1980', 'DD/MM/YYYY'), to_date('03/02/2019', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '996-420-4335', 'jackie.morar@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (225, 'Crist', 'Crona', 'Jammie Green', 'F', to_date('26/11/1959', 'DD/MM/YYYY'), to_date('29/01/2010', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '(641) 673-0132 x623', 'lonny.jacobi@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (226, 'Mitchell', 'Howe', 'Ms. Abagail Champlin', 'M', to_date('03/11/1978', 'DD/MM/YYYY'), to_date('25/09/2017', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '458-876-6964', 'jblanda@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (227, 'Senger', 'Jast', 'Freeman Nikolaus', 'F', to_date('10/12/1983', 'DD/MM/YYYY'), to_date('13/07/2019', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '731.369.6877 x681', 'eldridge.padberg@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (228, 'Hermiston', 'Lynch', 'Miss Mittie Klein', 'M', to_date('03/09/1941', 'DD/MM/YYYY'), to_date('19/08/2019', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '(914) 513-7520 x420', 'lavina.becker@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (229, 'Waters', 'Beahan', 'Cornell Schneider', 'F', to_date('05/02/1963', 'DD/MM/YYYY'), to_date('24/04/2014', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '1-491-253-0387 x8136', 'isaac07@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (230, 'Beatty', 'Schinner', 'Ferne White', 'M', to_date('23/08/1973', 'DD/MM/YYYY'), to_date('05/04/2017', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '19.157.026.512', 'purdy.fabiola@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (231, 'Hegmann', 'Skiles', 'Cordelia Hintz', 'M', to_date('09/03/1958', 'DD/MM/YYYY'), to_date('21/10/2010', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '16.933.386.863', 'isabella82@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (232, 'Tromp', 'Carter', 'Verlie Adams', 'F', to_date('21/09/1984', 'DD/MM/YYYY'), to_date('20/06/2015', 'DD/MM/YYYY'), 'San Jose', 'Limon', '(534) 790-3054', 'mkshlerin@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (233, 'Lang', 'Cummings', 'Mr. Raleigh Mohr PhD', 'M', to_date('25/09/2009', 'DD/MM/YYYY'), to_date('28/03/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '810-816-2937', 'arjun.hoppe@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (234, 'Gislason', 'Torp', 'Mr. Ruben Pagac', 'M', to_date('03/06/1978', 'DD/MM/YYYY'), to_date('21/01/2019', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '696-483-1508 x334', 'hiram.quitzon@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (235, 'Russel', 'Muller', 'Keith Brown', 'F', to_date('19/05/1949', 'DD/MM/YYYY'), to_date('02/05/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '(246) 522-2115 x14331', 'langworth.ivory@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (236, 'Schmitt', 'Nicolas', 'Prof. Lionel Hudson', 'M', to_date('26/11/1990', 'DD/MM/YYYY'), to_date('15/09/2012', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '664-592-2784 x59279', 'hallie.hoeger@example.org',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (237, 'Padberg', 'Davis', 'Haylee Langworth Jr.', 'F', to_date('26/07/1971', 'DD/MM/YYYY'), to_date('09/06/2010', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '14315692234', 'isaias.murphy@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (238, 'Swift', 'Shields', 'Cielo Lang', 'M', to_date('28/07/1975', 'DD/MM/YYYY'), to_date('26/07/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '562-735-8581 x248', 'fabian.morar@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (239, 'Rodriguez', 'Bradtke', 'Dr. Jaiden D''Amore', 'F', to_date('18/07/2002', 'DD/MM/YYYY'), to_date('12/10/2016', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '16.829.148.789', 'rey.feeney@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (240, 'Strosin', 'Jaskolski', 'Crystal Prosacco', 'M', to_date('01/03/1964', 'DD/MM/YYYY'), to_date('19/08/2019', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '(582) 648-4682 x761', 'uschuster@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (241, 'Kiehn', 'Shields', 'Maegan Harvey', 'M', to_date('04/02/1990', 'DD/MM/YYYY'), to_date('12/03/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '718-997-6834 x8065', 'doyle.petra@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (242, 'Cummerata', 'Parker', 'Mrs. Jeanie Strosin', 'F', to_date('19/11/2002', 'DD/MM/YYYY'), to_date('26/04/2011', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '(672) 607-8243 x26006', 'johan.mann@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (243, 'Welch', 'Dach', 'Rolando Gutmann', 'M', to_date('10/12/1964', 'DD/MM/YYYY'), to_date('28/01/2011', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '(830) 643-3450 x6304', 'della.koch@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (244, 'Beier', 'Erdman', 'Shaina Hoppe', 'F', to_date('08/09/1989', 'DD/MM/YYYY'), to_date('05/10/2014', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '378-621-3255', 'romaine40@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (245, 'Botsford', 'Fadel', 'Armani Rosenbaum', 'M', to_date('13/06/1980', 'DD/MM/YYYY'), to_date('02/11/2019', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '-2739', 'nborer@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (246, 'Konopelski', 'Russel', 'Adela Walker MD', 'F', to_date('31/01/2001', 'DD/MM/YYYY'), to_date('12/07/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '13.697.690.886', 'mossie.runolfsdottir@example.com',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (247, 'Schiller', 'Luettgen', 'Jeanne McLaughlin', 'M', to_date('21/05/1986', 'DD/MM/YYYY'), to_date('17/10/2015', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '1-449-682-3353', 'beier.jazmyne@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (248, 'Russel', 'Gorczany', 'Oda Metz', 'M', to_date('20/10/2008', 'DD/MM/YYYY'), to_date('27/07/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '386-703-0708', 'knikolaus@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (249, 'Pouros', 'Herman', 'Beverly Glover', 'M', to_date('23/05/1969', 'DD/MM/YYYY'), to_date('19/03/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '242-481-2814 x688', 'williamson.bianka@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (250, 'Streich', 'Lueilwitz', 'Kathryne Upton', 'F', to_date('09/09/1965', 'DD/MM/YYYY'), to_date('18/10/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '1-921-915-4567 x185', 'eusebio.ruecker@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (251, 'Torp', 'Lueilwitz', 'Johnson Bauch', 'M', to_date('26/12/1955', 'DD/MM/YYYY'), to_date('18/10/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '(931) 512-2637', 'madelyn.fritsch@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (252, 'Bashirian', 'Cassin', 'Dr. Eliezer Feeney', 'F', to_date('28/10/1962', 'DD/MM/YYYY'), to_date('09/11/2013', 'DD/MM/YYYY'), 'Esparza', 'Limon', '397.847.9828 x4457', 'demarcus.abernathy@example.org',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (253, 'Hessel', 'Waelchi', 'Tony Erdman MD', 'M', to_date('03/01/1945', 'DD/MM/YYYY'), to_date('01/01/2017', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '(231) 465-9311 x7124', 'bonita56@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (254, 'Quitzon', 'Klein', 'Ms. Marjorie Collier', 'F', to_date('24/06/1990', 'DD/MM/YYYY'), to_date('13/01/2016', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '(915) 772-2055', 'leuschke.maida@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (255, 'Fahey', 'Boehm', 'Shaylee Deckow', 'M', to_date('19/09/1976', 'DD/MM/YYYY'), to_date('13/11/2009', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '929-874-5352', 'pattie13@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (256, 'Schiller', 'Stehr', 'Johnny Littel', 'M', to_date('06/08/2008', 'DD/MM/YYYY'), to_date('29/11/2013', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '767.493.7795 x02227', 'rashawn32@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (257, 'Larkin', 'Bins', 'Vicenta Bernier', 'F', to_date('31/12/1995', 'DD/MM/YYYY'), to_date('07/10/2012', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '-3022', 'louie.lindgren@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (258, 'Johns', 'Bednar', 'Natasha Toy', 'M', to_date('01/02/1974', 'DD/MM/YYYY'), to_date('02/10/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '(694) 356-8831', 'johnson.winona@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (259, 'Gerhold', 'Goldner', 'Marina Kshlerin', 'F', to_date('14/11/1944', 'DD/MM/YYYY'), to_date('27/02/2011', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '+1 (295) 768-6368', 'abbott.heidi@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (260, 'Spinka', 'Durgan', 'Lysanne Prohaska I', 'M', to_date('27/06/1956', 'DD/MM/YYYY'), to_date('16/06/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '(742) 577-3581 x34372', 'watsica.janis@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (261, 'VonRueden', 'Braun', 'Callie Mueller', 'F', to_date('22/03/1983', 'DD/MM/YYYY'), to_date('07/11/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '640-941-6335 x2902', 'emard.margaret@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (262, 'Leuschke', 'Lebsack', 'Oliver Bosco', 'M', to_date('13/12/1953', 'DD/MM/YYYY'), to_date('10/09/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '(685) 387-0070 x687', 'zlueilwitz@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (263, 'Mraz', 'Nolan', 'Fernando Rempel', 'M', to_date('08/05/1978', 'DD/MM/YYYY'), to_date('21/07/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '818.762.0989 x82622', 'west.shawna@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (264, 'Schoen', 'Koelpin', 'Russell Mueller', 'F', to_date('07/03/1942', 'DD/MM/YYYY'), to_date('14/12/2010', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '1-752-655-6854 x371', 'adelbert96@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (265, 'Kautzer', 'Cremin', 'Tyree Simonis', 'M', to_date('03/11/1975', 'DD/MM/YYYY'), to_date('06/03/2016', 'DD/MM/YYYY'), 'Esparza', 'Limon', '13699347312', 'zita.ritchie@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (266, 'Daugherty', 'Heaney', 'Avery Kub', 'F', to_date('27/07/1974', 'DD/MM/YYYY'), to_date('14/06/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '1-495-309-4381', 'greenfelder.landen@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (267, 'Morissette', 'Lind', 'Velma Volkman', 'M', to_date('12/09/1982', 'DD/MM/YYYY'), to_date('24/05/2017', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '657-441-8686', 'alexandria.murray@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (268, 'Jenkins', 'Ward', 'Abigail Reilly', 'F', to_date('03/03/1988', 'DD/MM/YYYY'), to_date('16/05/2012', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '-6247', 'tkuvalis@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (269, 'Reilly', 'Blick', 'Carson Lynch', 'M', to_date('27/11/1975', 'DD/MM/YYYY'), to_date('08/05/2016', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '591-319-1373', 'kpurdy@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (270, 'Boehm', 'Stokes', 'Hillary Gleason', 'M', to_date('08/08/1952', 'DD/MM/YYYY'), to_date('06/11/2011', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '+1 (724) 783-2924', 'vicente.pacocha@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (271, 'Lind', 'Cole', 'Prof. Keith Keebler', 'F', to_date('03/09/1974', 'DD/MM/YYYY'), to_date('07/03/2016', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '1-545-535-9665', 'crona.lillie@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (272, 'Harvey', 'Crooks', 'Mr. Armani Rau', 'M', to_date('08/09/1985', 'DD/MM/YYYY'), to_date('27/06/2017', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '14.393.797.710', 'sam12@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (273, 'Kuhlman', 'Hoppe', 'Lowell Brakus Jr.', 'M', to_date('17/12/1968', 'DD/MM/YYYY'), to_date('06/08/2013', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '-9267', 'hermann.cyril@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (274, 'Blick', 'Stoltenberg', 'Quinten Nader', 'F', to_date('06/05/1982', 'DD/MM/YYYY'), to_date('13/02/2017', 'DD/MM/YYYY'), 'Esparza', 'Limon', '685.243.5579 x1533', 'deborah63@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (275, 'Crist', 'Schmitt', 'Prof. Aaron Shanahan', 'M', to_date('09/01/1963', 'DD/MM/YYYY'), to_date('16/08/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '1-582-853-2818 x350', 'yundt.quincy@example.net',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (276, 'Brekke', 'Nicolas', 'Johann Koepp', 'F', to_date('18/09/2004', 'DD/MM/YYYY'), to_date('19/10/2013', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '15528044037', 'evon@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (277, 'Cruickshank', 'Flatley', 'Makenzie Crona', 'M', to_date('09/06/1952', 'DD/MM/YYYY'), to_date('01/10/2012', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '17.715.089.445', 'lelia50@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (278, 'Wiza', 'Stanton', 'Genevieve Robel', 'M', to_date('08/04/1959', 'DD/MM/YYYY'), to_date('28/05/2019', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '+1 (907) 752-3996', 'hayes.malcolm@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (279, 'Kertzmann', 'Schoen', 'Wilfrid Koss', 'F', to_date('26/10/1988', 'DD/MM/YYYY'), to_date('17/07/2010', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '(916) 567-4957', 'christiansen.kitty@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (280, 'Zulauf', 'Labadie', 'Brandyn Russel', 'M', to_date('22/04/1993', 'DD/MM/YYYY'), to_date('13/01/2017', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '1-205-997-8410', 'shermiston@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (281, 'Romaguera', 'Bergnaum', 'Jorge Bosco III', 'F', to_date('20/04/2008', 'DD/MM/YYYY'), to_date('15/07/2013', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '(528) 215-2451 x16869', 'svolkman@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (282, 'Armstrong', 'Keeling', 'Giovanna Simonis', 'M', to_date('06/07/1949', 'DD/MM/YYYY'), to_date('27/07/2010', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '+1 (391) 453-1619', 'durgan.jeromy@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (283, 'Langworth', 'Gerhold', 'Simeon Cronin', 'F', to_date('12/12/1943', 'DD/MM/YYYY'), to_date('18/01/2015', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '1-435-547-7988', 'twillms@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (284, 'Kirlin', 'Hansen', 'Ophelia Rowe', 'M', to_date('14/03/1969', 'DD/MM/YYYY'), to_date('04/08/2017', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '1-729-699-9368', 'dschneider@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (285, 'Kiehn', 'Mann', 'Prof. Wilson Fritsch', 'M', to_date('27/10/1995', 'DD/MM/YYYY'), to_date('30/03/2015', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '+1 (831) 626-2046', 'witting.ottilie@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (286, 'Nicolas', 'Klocko', 'Alisa Willms', 'F', to_date('08/01/2009', 'DD/MM/YYYY'), to_date('18/02/2012', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '-5106', 'lcrooks@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (287, 'Conroy', 'Sauer', 'Jannie Fisher', 'M', to_date('21/03/1968', 'DD/MM/YYYY'), to_date('04/05/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '1-246-661-2340', 'little.selena@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (288, 'Thompson', 'Bergstrom', 'Mia Beer DVM', 'F', to_date('12/06/1957', 'DD/MM/YYYY'), to_date('20/11/2015', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '1-434-370-2007 x85990', 'deanna97@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (289, 'Robel', 'Bruen', 'Keenan Brekke', 'M', to_date('05/01/1961', 'DD/MM/YYYY'), to_date('21/11/2015', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '469-984-7846 x7107', 'harris.jennifer@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (290, 'McGlynn', 'Reinger', 'Mrs. Modesta Dickens PhD', 'F', to_date('10/05/1948', 'DD/MM/YYYY'), to_date('04/05/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '375.275.6805 x309', 'pparisian@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (291, 'Lubowitz', 'Gulgowski', 'Korey Cremin', 'M', to_date('05/10/2009', 'DD/MM/YYYY'), to_date('11/09/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '(350) 366-2539 x595', 'treva.zboncak@example.org',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (292, 'Kuhic', 'Beatty', 'Tressie Walker', 'M', to_date('21/01/1973', 'DD/MM/YYYY'), to_date('26/08/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '+1 (726) 488-3631', 'dhuel@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (293, 'Ryan', 'Doyle', 'Andres Goldner', 'F', to_date('02/03/1949', 'DD/MM/YYYY'), to_date('29/11/2015', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '+1 (541) 660-0279', 'irving.mohr@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (294, 'Herman', 'Rau', 'Casandra Stracke', 'M', to_date('08/03/1959', 'DD/MM/YYYY'), to_date('26/12/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '1-279-550-3974 x2117', 'rowena.wiza@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (295, 'Gleichner', 'Bode', 'Lavern Smitham', 'F', to_date('07/08/1967', 'DD/MM/YYYY'), to_date('01/09/2012', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '587-785-0925 x022', 'xromaguera@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (296, 'Halvorson', 'Macejkovic', 'Kathryn Dare', 'M', to_date('24/11/1989', 'DD/MM/YYYY'), to_date('11/05/2013', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '+1 (226) 990-5539', 'gideon.oberbrunner@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (297, 'Runolfsdottir', 'Upton', 'Hillary Rath', 'F', to_date('08/10/2001', 'DD/MM/YYYY'), to_date('13/04/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '1-846-782-4862', 'fritsch.noel@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (298, 'Lind', 'Price', 'Jamaal Harvey', 'M', to_date('22/08/1981', 'DD/MM/YYYY'), to_date('12/03/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '16437905811', 'tillman.harvey@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (299, 'Walter', 'Stiedemann', 'Dr. Hulda Hegmann', 'M', to_date('20/03/1968', 'DD/MM/YYYY'), to_date('11/05/2012', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '1-879-562-2337 x376', 'tfay@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (300, 'Ortiz', 'Leuschke', 'Salvador Schinner', 'F', to_date('03/04/1993', 'DD/MM/YYYY'), to_date('22/07/2013', 'DD/MM/YYYY'), 'San Jose', 'Limon', '18562180453', 'bwhite@example.net', 6);

commit;

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (301, 'Breitenberg', 'Blick', 'Dr. Mallie Hills', 'M', to_date('14/09/1949', 'DD/MM/YYYY'), to_date('19/11/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '992.429.4760 x70159', 'katlynn31@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (302, 'Paucek', 'Corkery', 'Orie Wehner', 'M', to_date('17/04/1991', 'DD/MM/YYYY'), to_date('07/12/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '345-486-0159 x244', 'shannon65@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (303, 'Gottlieb', 'Brown', 'Mr. Stuart Batz MD', 'F', to_date('16/08/1988', 'DD/MM/YYYY'), to_date('15/02/2014', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '1-971-259-8286', 'efeeney@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (304, 'Adams', 'Crooks', 'Levi Bode', 'M', to_date('24/04/1970', 'DD/MM/YYYY'), to_date('14/06/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '767.614.3786 x8901', 'sim66@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (305, 'Blick', 'Grimes', 'Mr. Gerald Waelchi', 'F', to_date('04/01/1958', 'DD/MM/YYYY'), to_date('01/07/2011', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '1-674-490-2072', 'rosalind50@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (306, 'Ankunding', 'Kling', 'Wilhelmine Gibson', 'M', to_date('21/04/1953', 'DD/MM/YYYY'), to_date('22/02/2019', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '-4868', 'bednar.kiana@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (307, 'Wehner', 'Fadel', 'Bria Muller', 'F', to_date('12/11/1957', 'DD/MM/YYYY'), to_date('05/12/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '(491) 272-1945', 'emraz@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (308, 'Nolan', 'Howe', 'Prof. Glen Kirlin PhD', 'M', to_date('01/02/1949', 'DD/MM/YYYY'), to_date('20/11/2011', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '-8726', 'kadin.feest@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (309, 'Cronin', 'Harber', 'Tatyana Littel', 'M', to_date('13/12/2005', 'DD/MM/YYYY'), to_date('02/11/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '1-278-920-3059 x6505', 'fred.towne@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (310, 'Beer', 'Bode', 'Ms. Raquel Weber Jr.', 'F', to_date('09/06/1982', 'DD/MM/YYYY'), to_date('22/07/2019', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '9.078.738.161', 'bvon@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (311, 'Roberts', 'VonRueden', 'Bernhard Dietrich', 'M', to_date('30/04/1999', 'DD/MM/YYYY'), to_date('02/11/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '-8144', 'shaina.lueilwitz@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (312, 'Grant', 'Bailey', 'Josianne Schowalter', 'F', to_date('20/03/1972', 'DD/MM/YYYY'), to_date('17/11/2013', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '(216) 813-5688 x396', 'samantha.bechtelar@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (313, 'Lang', 'Schowalter', 'Mr. Alan Paucek IV', 'M', to_date('20/02/2002', 'DD/MM/YYYY'), to_date('27/04/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '(659) 601-7680', 'kamren.klocko@example.net',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (314, 'Bartoletti', 'Marvin', 'Marcelino Okuneva', 'F', to_date('18/05/1959', 'DD/MM/YYYY'), to_date('21/09/2017', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '728-932-8517', 'bhoppe@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (315, 'Auer', 'Tremblay', 'Ulises Paucek', 'M', to_date('12/10/2008', 'DD/MM/YYYY'), to_date('04/04/2018', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '258-792-0284', 'gaylord.hadley@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (316, 'Stokes', 'Hessel', 'Kyle Champlin IV', 'M', to_date('14/08/1956', 'DD/MM/YYYY'), to_date('19/02/2019', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '(747) 314-0722 x530', 'jabari.roob@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (317, 'Rogahn', 'Rowe', 'Bonita Grant MD', 'F', to_date('23/07/1963', 'DD/MM/YYYY'), to_date('03/01/2013', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '1-556-216-5128', 'eschowalter@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (318, 'Leuschke', 'Emard', 'Shea Heller', 'M', to_date('08/06/1976', 'DD/MM/YYYY'), to_date('28/10/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '(806) 699-0535', 'xlittle@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (319, 'Bosco', 'Cummings', 'Elisa Bruen', 'F', to_date('07/01/1957', 'DD/MM/YYYY'), to_date('27/10/2015', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '(920) 870-1962 x80011', 'dameon34@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (320, 'O''Reilly', 'Bartell', 'Marta Wolff', 'M', to_date('24/12/1992', 'DD/MM/YYYY'), to_date('09/08/2015', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '(508) 541-0118 x275', 'yjast@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (321, 'Ullrich', 'Rodriguez', 'Gunner Tromp PhD', 'F', to_date('26/12/1953', 'DD/MM/YYYY'), to_date('01/10/2010', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '1-528-601-9380', 'gcasper@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (322, 'Roob', 'Harvey', 'Orlando Kuphal PhD', 'M', to_date('28/10/1950', 'DD/MM/YYYY'), to_date('07/03/2012', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '341-783-8328', 'waelchi.kim@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (323, 'Gerhold', 'Schuster', 'Prof. Joaquin Prosacco Sr.', 'M', to_date('08/06/1971', 'DD/MM/YYYY'), to_date('22/04/2010', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '810.348.9060 x2306', 'osbaldo.white@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (324, 'Haag', 'Raynor', 'Abagail Boehm', 'F', to_date('11/07/1948', 'DD/MM/YYYY'), to_date('16/04/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '1-720-401-7755', 'chelsea.quitzon@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (325, 'Jacobs', 'Hills', 'Sabryna Balistreri DVM', 'M', to_date('30/07/2007', 'DD/MM/YYYY'), to_date('18/05/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '702-695-1030', 'brunolfsdottir@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (326, 'Hettinger', 'Bernier', 'Taryn Hansen', 'M', to_date('17/09/2003', 'DD/MM/YYYY'), to_date('11/06/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '427-817-8525 x12748', 'gaylord47@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (327, 'Treutel', 'Hoeger', 'Esther Nikolaus I', 'F', to_date('20/05/1997', 'DD/MM/YYYY'), to_date('03/12/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '19045758166', 'felicity.prohaska@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (328, 'Koepp', 'Zulauf', 'Muriel Thiel', 'M', to_date('15/10/1985', 'DD/MM/YYYY'), to_date('25/07/2012', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '-4528', 'lwilkinson@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (329, 'Glover', 'Altenwerth', 'Moshe Ledner', 'F', to_date('31/08/1950', 'DD/MM/YYYY'), to_date('16/02/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '959.265.0377 x113', 'yadira.leuschke@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (330, 'Wunsch', 'Cole', 'Chasity Feil', 'M', to_date('09/04/1961', 'DD/MM/YYYY'), to_date('03/06/2013', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '1-780-565-9318 x109', 'jhodkiewicz@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (331, 'Kiehn', 'Hudson', 'Prof. Leonel Breitenberg', 'F', to_date('21/05/1976', 'DD/MM/YYYY'), to_date('02/11/2010', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '1-683-469-2424', 'welch.orie@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (332, 'Kuhlman', 'Mueller', 'Jacynthe Kerluke', 'M', to_date('13/07/1963', 'DD/MM/YYYY'), to_date('29/12/2017', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '9.083.964.111', 'dgutkowski@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (333, 'Rowe', 'Hirthe', 'Miss Vivian Donnelly', 'M', to_date('17/08/1940', 'DD/MM/YYYY'), to_date('16/07/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '14365466413', 'ccronin@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (334, 'Veum', 'Toy', 'Prof. Kamryn Lubowitz', 'F', to_date('24/01/1981', 'DD/MM/YYYY'), to_date('05/02/2012', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '+1 (274) 680-4582', 'zita47@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (335, 'Schaden', 'Effertz', 'Fernando Torphy', 'M', to_date('18/06/1986', 'DD/MM/YYYY'), to_date('09/03/2016', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '(841) 719-3523 x1625', 'efeest@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (336, 'Torphy', 'Hane', 'Miles O''Conner', 'F', to_date('21/11/2005', 'DD/MM/YYYY'), to_date('23/03/2013', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '-1014', 'brakus.rhianna@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (337, 'Reynolds', 'Ortiz', 'Rico Muller Sr.', 'M', to_date('15/08/1959', 'DD/MM/YYYY'), to_date('19/03/2014', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '1-639-705-9000', 'johnpaul.raynor@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (338, 'Hintz', 'Boyer', 'Dr. Cordia Sipes MD', 'F', to_date('23/05/1991', 'DD/MM/YYYY'), to_date('27/05/2010', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '4.377.232.826', 'jarret.torp@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (339, 'Davis', 'Stamm', 'Lewis Dare', 'M', to_date('16/02/1963', 'DD/MM/YYYY'), to_date('09/01/2013', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '-10126', 'parisian.annamarie@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (340, 'Bahringer', 'Thompson', 'Marc Veum', 'M', to_date('22/12/1997', 'DD/MM/YYYY'), to_date('01/11/2017', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '+1 (875) 778-0418', 'gislason.avery@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (341, 'Wuckert', 'Witting', 'Ryann Rempel', 'M', to_date('21/04/2008', 'DD/MM/YYYY'), to_date('28/12/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '282-837-3651 x3171', 'pbartoletti@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (342, 'Kilback', 'Lesch', 'Aniyah Koelpin', 'F', to_date('11/11/1948', 'DD/MM/YYYY'), to_date('07/11/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '-2044', 'emmerich.althea@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (343, 'Toy', 'Schuppe', 'Santiago Abshire DDS', 'M', to_date('12/07/1988', 'DD/MM/YYYY'), to_date('25/11/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '979-723-6609', 'dayna.nitzsche@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (344, 'Morar', 'Schaefer', 'Kory Torp DDS', 'F', to_date('15/05/1981', 'DD/MM/YYYY'), to_date('25/04/2014', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '1-529-929-5098', 'jcremin@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (345, 'Schaefer', 'Jacobs', 'Zachery Schamberger Jr.', 'M', to_date('19/05/2002', 'DD/MM/YYYY'), to_date('29/11/2015', 'DD/MM/YYYY'), 'Liberia', 'Limon', '919-214-0243', 'kameron.ortiz@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (346, 'Schaden', 'Mosciski', 'Myron Legros I', 'F', to_date('08/08/1980', 'DD/MM/YYYY'), to_date('07/09/2016', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '-1769', 'conn.tessie@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (347, 'Hessel', 'O''Keefe', 'Zion Gislason', 'M', to_date('09/10/1986', 'DD/MM/YYYY'), to_date('31/12/2015', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '(414) 571-4247 x0859', 'devin29@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (348, 'Jaskolski', 'Lockman', 'Queen Anderson', 'M', to_date('17/09/1955', 'DD/MM/YYYY'), to_date('24/11/2017', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '9.483.381.048', 'fausto.cummings@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (349, 'Ryan', 'Hackett', 'Tressa Schmeler', 'F', to_date('02/08/1970', 'DD/MM/YYYY'), to_date('19/11/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '345.920.7034 x6258', 'hwintheiser@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (350, 'Ledner', 'Brakus', 'Jermaine Kling', 'M', to_date('06/08/1989', 'DD/MM/YYYY'), to_date('01/04/2017', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '528.793.1914 x2042', 'lyric.becker@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (351, 'O''Hara', 'Torphy', 'Clarabelle Wehner', 'F', to_date('14/08/1983', 'DD/MM/YYYY'), to_date('25/11/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '1-513-342-1189 x192', 'ferry.kelsi@example.org',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (352, 'Ledner', 'Hand', 'Gardner Mayert', 'M', to_date('07/04/1942', 'DD/MM/YYYY'), to_date('28/07/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '-7195', 'kari.harvey@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (353, 'Hansen', 'Greenfelder', 'Kasandra Jacobi', 'F', to_date('22/08/1992', 'DD/MM/YYYY'), to_date('07/01/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '+1 (853) 516-1784', 'tyra95@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (354, 'Bruen', 'Eichmann', 'Terry Boyer', 'M', to_date('24/11/1968', 'DD/MM/YYYY'), to_date('12/11/2010', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '508.925.2416 x41234', 'lura75@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (355, 'Heaney', 'Prohaska', 'Dr. Wilton Waelchi Sr.', 'M', to_date('04/07/1995', 'DD/MM/YYYY'), to_date('07/12/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '7.044.629.659', 'alexie03@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (356, 'Haley', 'Dickinson', 'Catharine Heidenreich', 'F', to_date('21/12/1971', 'DD/MM/YYYY'), to_date('16/02/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '-9200', 'zrippin@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (357, 'Stanton', 'Batz', 'Dr. Bertha Hickle IV', 'M', to_date('18/02/1947', 'DD/MM/YYYY'), to_date('10/06/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '729-372-3349 x5049', 'dolores.torp@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (358, 'Schimmel', 'Heller', 'Franz Blick', 'F', to_date('28/03/1996', 'DD/MM/YYYY'), to_date('17/07/2014', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '885-263-9536 x11256', 'ebony.bartoletti@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (359, 'Nikolaus', 'Lockman', 'Mr. Dion Sanford', 'M', to_date('06/07/1986', 'DD/MM/YYYY'), to_date('22/07/2019', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '13.689.508.400', 'kennedi61@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (360, 'Lesch', 'Emard', 'Prof. Stephanie Jones Jr.', 'F', to_date('28/01/1955', 'DD/MM/YYYY'), to_date('17/04/2017', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '338.589.8441 x675', 'prunte@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (361, 'Lindgren', 'Rempel', 'Mr. Helmer Stracke', 'M', to_date('14/02/1954', 'DD/MM/YYYY'), to_date('30/12/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '16.538.034.429', 'darian.herzog@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (362, 'Hayes', 'Bernhard', 'Nellie Rutherford', 'M', to_date('01/01/1989', 'DD/MM/YYYY'), to_date('08/08/2019', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '15894229195', 'pearline81@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (363, 'Homenick', 'Schumm', 'Breana Greenholt DDS', 'F', to_date('11/08/1989', 'DD/MM/YYYY'), to_date('01/05/2017', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '+1 (591) 442-4637', 'leonel.bernier@example.org',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (364, 'Murphy', 'Haley', 'Grover Runolfsdottir V', 'M', to_date('05/03/1952', 'DD/MM/YYYY'), to_date('06/02/2017', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '261-268-5802 x24404', 'belle.abbott@example.net',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (365, 'Trantow', 'Becker', 'Mrs. Natalie Cronin PhD', 'M', to_date('23/10/1952', 'DD/MM/YYYY'), to_date('22/04/2014', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '+1 (423) 232-6301', 'deborah59@example.org',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (366, 'Johnston', 'O''Kon', 'Coby Lemke', 'F', to_date('15/12/1941', 'DD/MM/YYYY'), to_date('24/11/2018', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '7.093.959.328', 'sydnie36@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (367, 'Wilderman', 'Beatty', 'Andre Klein', 'M', to_date('18/03/1943', 'DD/MM/YYYY'), to_date('01/02/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '658-616-5991', 'terence.collins@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (368, 'Romaguera', 'Walker', 'Carter Wehner', 'F', to_date('01/10/1964', 'DD/MM/YYYY'), to_date('22/01/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '709-738-5516 x562', 'bpacocha@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (369, 'Bogan', 'Erdman', 'Dr. Koby Brakus', 'M', to_date('11/05/1985', 'DD/MM/YYYY'), to_date('06/02/2011', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '12.413.688.927', 'demario.beier@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (370, 'Smitham', 'Botsford', 'Gordon Block', 'M', to_date('18/06/1981', 'DD/MM/YYYY'), to_date('20/03/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '1-958-526-7003 x590', 'mosciski.laron@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (371, 'Hauck', 'Buckridge', 'Trenton Flatley', 'F', to_date('23/08/1967', 'DD/MM/YYYY'), to_date('29/04/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '1-529-633-0584', 'sterling93@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (372, 'Kulas', 'Pfeffer', 'Cade Rosenbaum', 'M', to_date('02/11/2003', 'DD/MM/YYYY'), to_date('30/11/2009', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '740.271.4375 x75902', 'thurman80@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (373, 'Steuber', 'Lind', 'Emely Mueller', 'F', to_date('24/06/2007', 'DD/MM/YYYY'), to_date('28/01/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '-7822', 'lisette.robel@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (374, 'Schamberger', 'Wunsch', 'Prof. Guillermo Gusikowski I', 'M', to_date('23/04/1947', 'DD/MM/YYYY'), to_date('17/12/2012', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '+1 (607) 440-3839', 'ybartell@example.com',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (375, 'Senger', 'Price', 'Celestino Wunsch', 'F', to_date('06/02/1990', 'DD/MM/YYYY'), to_date('22/05/2014', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '-1992', 'jruecker@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (376, 'Rodriguez', 'Veum', 'Prof. Kane Kertzmann', 'M', to_date('23/10/1951', 'DD/MM/YYYY'), to_date('18/08/2015', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '+1 (958) 957-5573', 'koelpin.rosemarie@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (377, 'Gibson', 'Collins', 'Shane Becker', 'M', to_date('29/10/1967', 'DD/MM/YYYY'), to_date('12/03/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '407.365.3121 x16550', 'daryl.shanahan@example.org',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (378, 'Considine', 'Fritsch', 'Juston Goldner III', 'F', to_date('23/03/1959', 'DD/MM/YYYY'), to_date('28/09/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '1-930-731-3631', 'ethan.schinner@example.com',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (379, 'Abshire', 'Franecki', 'Mariane Jerde', 'M', to_date('22/01/1991', 'DD/MM/YYYY'), to_date('14/06/2012', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '353-255-8316', 'price.maritza@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (380, 'Herman', 'Stokes', 'Kenya Pacocha DDS', 'F', to_date('14/03/1984', 'DD/MM/YYYY'), to_date('10/06/2019', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '-10006', 'bridie.hagenes@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (381, 'Wehner', 'Reichel', 'Alfonzo Wilderman MD', 'M', to_date('20/04/2007', 'DD/MM/YYYY'), to_date('03/07/2010', 'DD/MM/YYYY'), 'San Jose', 'Limon', '(620) 986-1193', 'winifred.jenkins@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (382, 'Mayert', 'Kutch', 'Brady Morar', 'F', to_date('01/09/1944', 'DD/MM/YYYY'), to_date('02/10/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '2.763.471.072', 'destin.franecki@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (383, 'Luettgen', 'Predovic', 'Tiffany Romaguera IV', 'M', to_date('04/05/1951', 'DD/MM/YYYY'), to_date('03/06/2014', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '(878) 615-6481 x57215', 'katharina21@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (384, 'Quitzon', 'Johns', 'Eda Marquardt', 'M', to_date('31/05/1999', 'DD/MM/YYYY'), to_date('28/06/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '(528) 309-5913', 'strosin.chester@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (385, 'Skiles', 'Roberts', 'Antonietta Schuppe', 'F', to_date('07/05/2008', 'DD/MM/YYYY'), to_date('10/01/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '-4494', 'fkuphal@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (386, 'Reinger', 'Bernier', 'Ross Kling III', 'M', to_date('20/05/1952', 'DD/MM/YYYY'), to_date('31/03/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '-6750', 'hugh27@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (387, 'Kunde', 'Connelly', 'Theodora O''Conner', 'F', to_date('21/01/1940', 'DD/MM/YYYY'), to_date('23/11/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '778.480.6563 x733', 'scummings@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (388, 'Hagenes', 'Gibson', 'Mariam Wehner MD', 'M', to_date('04/01/1942', 'DD/MM/YYYY'), to_date('05/10/2015', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '12655786144', 'aurelie99@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (389, 'Bednar', 'Bechtelar', 'Prof. Adalberto Boyer Sr.', 'F', to_date('23/06/1948', 'DD/MM/YYYY'), to_date('03/03/2013', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '(457) 223-1054', 'smitham.eloy@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (390, 'Smitham', 'Pagac', 'Dr. Earnestine Gerhold DVM', 'M', to_date('11/08/1964', 'DD/MM/YYYY'), to_date('01/08/2016', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '580-510-9513 x241', 'buster41@example.net',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (391, 'Huel', 'Rempel', 'Porter Quitzon', 'M', to_date('22/12/1947', 'DD/MM/YYYY'), to_date('01/12/2016', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '-7728', 'hartmann.dorris@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (392, 'Murazik', 'Block', 'Adrianna Christiansen', 'F', to_date('03/03/1988', 'DD/MM/YYYY'), to_date('17/09/2015', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '1-716-937-5760', 'tokuneva@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (393, 'Kozey', 'Leannon', 'Timmothy Doyle', 'M', to_date('20/09/1968', 'DD/MM/YYYY'), to_date('05/05/2010', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '(906) 327-8020 x419', 'mblock@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (394, 'Bauch', 'Nicolas', 'Ms. Shirley Upton', 'M', to_date('02/08/1967', 'DD/MM/YYYY'), to_date('04/12/2017', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '320-236-5672', 'ernestina.bins@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (395, 'Satterfield', 'Tillman', 'Mrs. Abigail Smith PhD', 'F', to_date('23/03/1964', 'DD/MM/YYYY'), to_date('19/06/2011', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '417-267-3765 x4277', 'kmccullough@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (396, 'Considine', 'Gibson', 'Prof. Dora Dooley', 'M', to_date('05/04/1988', 'DD/MM/YYYY'), to_date('10/09/2010', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '13.188.768.250', 'jenkins.abdiel@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (397, 'Tromp', 'Jacobs', 'Dr. Steve Aufderhar', 'F', to_date('07/06/1955', 'DD/MM/YYYY'), to_date('22/05/2012', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '-4751', 'strosin.cecil@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (398, 'Gleichner', 'Sauer', 'Vergie Koch', 'M', to_date('03/01/1941', 'DD/MM/YYYY'), to_date('06/06/2011', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '1-207-481-9900', 'rodolfo64@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (399, 'Willms', 'Ondricka', 'Prof. Wilson Daugherty V', 'F', to_date('02/12/1970', 'DD/MM/YYYY'), to_date('11/03/2016', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '927-370-4115', 'alana.tillman@example.org',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (400, 'Dach', 'Paucek', 'Joany Fahey', 'M', to_date('18/07/1966', 'DD/MM/YYYY'), to_date('24/03/2018', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '260-699-6215', 'nona43@example.org', 6);

commit;

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (401, 'Fisher', 'McKenzie', 'Monte Lebsack', 'M', to_date('12/04/1999', 'DD/MM/YYYY'), to_date('24/12/2017', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '-7946', 'hlockman@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (402, 'Fadel', 'Nolan', 'Dr. Anjali Hickle', 'F', to_date('27/12/1999', 'DD/MM/YYYY'), to_date('07/01/2012', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '(214) 419-6380', 'kenyon67@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (403, 'Schiller', 'Hagenes', 'Pietro Bradtke', 'M', to_date('20/05/2006', 'DD/MM/YYYY'), to_date('20/04/2018', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '1-398-312-1875', 'eve.ernser@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (404, 'Yundt', 'Okuneva', 'Dr. Jamil Pfeffer IV', 'F', to_date('11/03/2008', 'DD/MM/YYYY'), to_date('01/02/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '352-667-2295 x929', 'kimberly.murazik@example.com',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (405, 'Nicolas', 'Kovacek', 'Emiliano Yost', 'M', to_date('07/08/1991', 'DD/MM/YYYY'), to_date('24/03/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '737-556-9110', 'quinton.jast@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (406, 'Rosenbaum', 'Donnelly', 'Bryana Johnson', 'F', to_date('04/06/1999', 'DD/MM/YYYY'), to_date('05/01/2011', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '1-427-251-5318 x905', 'ofeil@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (407, 'Dare', 'Fay', 'Caitlyn Eichmann', 'M', to_date('26/07/1978', 'DD/MM/YYYY'), to_date('02/12/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '984-875-1272', 'margarette.mcclure@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (408, 'Bartell', 'Zieme', 'Sandrine Klein', 'M', to_date('01/11/1946', 'DD/MM/YYYY'), to_date('03/08/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '203-955-6528 x621', 'preston.champlin@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (409, 'Miller', 'Zulauf', 'Ari Wuckert II', 'F', to_date('18/09/1969', 'DD/MM/YYYY'), to_date('28/01/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '17875503155', 'madge61@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (410, 'Bogan', 'Swift', 'Gardner Paucek', 'M', to_date('09/06/2002', 'DD/MM/YYYY'), to_date('27/07/2010', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '694-757-7734', 'lucio.romaguera@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (411, 'Herman', 'Runolfsson', 'Mr. Heber Jacobi', 'F', to_date('05/08/1971', 'DD/MM/YYYY'), to_date('14/03/2010', 'DD/MM/YYYY'), 'San Jose', 'Limon', '1-921-774-6614 x5548', 'goldner.jake@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (412, 'Gottlieb', 'Schimmel', 'Mr. Sigmund Paucek', 'M', to_date('26/09/1998', 'DD/MM/YYYY'), to_date('04/07/2017', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '(547) 899-8979', 'croberts@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (413, 'Hammes', 'Champlin', 'Mrs. Nadia Berge', 'F', to_date('21/09/1945', 'DD/MM/YYYY'), to_date('21/02/2010', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '1-415-683-5113 x1245', 'viva57@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (414, 'Murazik', 'Hamill', 'Burley Frami PhD', 'M', to_date('19/08/1957', 'DD/MM/YYYY'), to_date('27/03/2010', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '15.605.593.412', 'ysatterfield@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (415, 'McDermott', 'Bergnaum', 'Prof. Camden Shields II', 'M', to_date('05/10/1959', 'DD/MM/YYYY'), to_date('18/04/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '979-725-1733', 'miracle07@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (416, 'Collier', 'Heller', 'Mathias Collier', 'F', to_date('18/01/1971', 'DD/MM/YYYY'), to_date('04/12/2018', 'DD/MM/YYYY'), 'Esparza', 'Limon', '240-745-1430', 'edd49@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (417, 'Abernathy', 'Dickens', 'Van Koss', 'M', to_date('09/01/1944', 'DD/MM/YYYY'), to_date('04/02/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '541-984-2808', 'welch.augusta@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (418, 'Trantow', 'Koss', 'Kaya Hane', 'M', to_date('07/12/1958', 'DD/MM/YYYY'), to_date('26/06/2019', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '-2325', 'emmalee.olson@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (419, 'Kessler', 'Nitzsche', 'Izabella Schuster III', 'F', to_date('28/06/1950', 'DD/MM/YYYY'), to_date('20/05/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '8.082.250.464', 'paula.halvorson@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (420, 'Upton', 'O''Kon', 'Greyson Macejkovic', 'M', to_date('11/04/1955', 'DD/MM/YYYY'), to_date('26/03/2015', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '17.195.897.287', 'xrice@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (421, 'Deckow', 'Kling', 'Bryana Macejkovic', 'F', to_date('23/05/1976', 'DD/MM/YYYY'), to_date('15/12/2009', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '-3449', 'leannon.willa@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (422, 'Cassin', 'Schinner', 'Karen Trantow', 'M', to_date('04/05/1953', 'DD/MM/YYYY'), to_date('06/08/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '(292) 954-8364 x4167', 'cborer@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (423, 'Braun', 'Stamm', 'Prof. Clyde Gleason', 'F', to_date('27/10/1947', 'DD/MM/YYYY'), to_date('31/12/2017', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '995-350-8016', 'willis.mohr@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (424, 'Swaniawski', 'Schultz', 'Coy Hauck', 'M', to_date('09/07/1960', 'DD/MM/YYYY'), to_date('16/07/2014', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '(323) 635-0915 x167', 'panderson@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (425, 'Sauer', 'Deckow', 'Dr. Rocky Will II', 'M', to_date('02/04/1965', 'DD/MM/YYYY'), to_date('31/01/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '6.532.529.602', 'bryce22@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (426, 'Waters', 'Pollich', 'Dr. Alyson McLaughlin Jr.', 'F', to_date('03/01/2001', 'DD/MM/YYYY'), to_date('12/10/2016', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '834-891-2918 x9217', 'fay.davion@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (427, 'Runolfsdottir', 'Okuneva', 'Ms. Chanel Stokes', 'M', to_date('14/07/1975', 'DD/MM/YYYY'), to_date('08/01/2013', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '953.249.2059 x831', 'franco.maggio@example.org',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (428, 'Dibbert', 'Baumbach', 'Ettie Pfannerstill', 'F', to_date('01/04/1940', 'DD/MM/YYYY'), to_date('10/12/2013', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '-8609', 'rhansen@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (429, 'Bogisich', 'Nicolas', 'Caroline Hudson', 'M', to_date('28/08/1997', 'DD/MM/YYYY'), to_date('09/01/2012', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '(427) 366-5810', 'ibrahim.miller@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (430, 'Bednar', 'Watsica', 'Shad Fay', 'F', to_date('05/03/2006', 'DD/MM/YYYY'), to_date('03/03/2015', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '961-394-4494', 'harvey.pierre@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (431, 'Walter', 'Dach', 'Logan Smith', 'M', to_date('22/04/2004', 'DD/MM/YYYY'), to_date('31/08/2012', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '1-710-852-0238 x97148', 'kendrick.connelly@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (432, 'McCullough', 'Baumbach', 'Isabell Schmeler III', 'M', to_date('10/09/1971', 'DD/MM/YYYY'), to_date('12/07/2013', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '251-279-2915', 'gorczany.alisha@example.org',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (433, 'Rolfson', 'Littel', 'Loy Feeney', 'M', to_date('26/07/1940', 'DD/MM/YYYY'), to_date('16/05/2013', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '1-869-446-5688 x6667', 'shayne64@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (434, 'Bernier', 'Turcotte', 'Dr. Ludwig Ankunding', 'F', to_date('06/06/2000', 'DD/MM/YYYY'), to_date('22/06/2017', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '5.536.262.905', 'xmetz@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (435, 'Dibbert', 'Hane', 'Green Glover', 'M', to_date('12/09/2002', 'DD/MM/YYYY'), to_date('28/09/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '738.866.4240 x42288', 'kevon.stiedemann@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (436, 'Zulauf', 'Reinger', 'Asia Harber Jr.', 'F', to_date('05/05/1982', 'DD/MM/YYYY'), to_date('22/07/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '(240) 424-8085 x917', 'gunner32@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (437, 'Murray', 'Gerhold', 'Abdul Crist', 'M', to_date('06/12/1983', 'DD/MM/YYYY'), to_date('04/12/2018', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '1-413-243-1678', 'chanel.corwin@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (438, 'Tillman', 'Pfeffer', 'Kevin Quitzon', 'F', to_date('07/05/1991', 'DD/MM/YYYY'), to_date('24/09/2019', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '(935) 295-5103 x29503', 'whitney.heller@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (439, 'Bayer', 'O''Keefe', 'Heather Cormier', 'M', to_date('24/05/2008', 'DD/MM/YYYY'), to_date('23/10/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '-3664', 'gsteuber@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (440, 'Littel', 'Lemke', 'Madeline Marquardt', 'M', to_date('26/09/1954', 'DD/MM/YYYY'), to_date('04/04/2019', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '17944549532', 'mohr.demarcus@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (441, 'Schamberger', 'Bartoletti', 'Prof. Antone Ledner MD', 'F', to_date('08/03/2001', 'DD/MM/YYYY'), to_date('11/05/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '436-517-2059 x694', 'hellen.ortiz@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (442, 'Leannon', 'Cruickshank', 'Mrs. Breana Parisian', 'M', to_date('25/02/1977', 'DD/MM/YYYY'), to_date('04/10/2018', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '-9046', 'christiana31@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (443, 'Dicki', 'Stroman', 'Cynthia Spencer', 'F', to_date('15/09/1944', 'DD/MM/YYYY'), to_date('24/04/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '+1 (290) 271-6718', 'deshaun90@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (444, 'Parisian', 'Heller', 'Judy Fadel', 'M', to_date('31/12/1968', 'DD/MM/YYYY'), to_date('29/01/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '1-725-347-4576', 'perry10@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (445, 'Veum', 'Deckow', 'Alessia Mann PhD', 'F', to_date('22/11/1986', 'DD/MM/YYYY'), to_date('10/12/2011', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '+1 (967) 270-7873', 'mason51@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (446, 'Batz', 'Marvin', 'Wellington Gerhold', 'M', to_date('02/02/1942', 'DD/MM/YYYY'), to_date('08/01/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '-10125', 'ydenesik@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (447, 'Grimes', 'Leannon', 'Richmond Watsica', 'M', to_date('19/05/1981', 'DD/MM/YYYY'), to_date('28/03/2010', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '(627) 916-6187 x9847', 'brook.jenkins@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (448, 'Hegmann', 'Gusikowski', 'Greta Marks', 'F', to_date('05/07/2003', 'DD/MM/YYYY'), to_date('27/01/2011', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '(554) 420-3649 x27530', 'spinka.jaylon@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (449, 'Schaefer', 'Crooks', 'Darryl Gutmann', 'M', to_date('13/05/1997', 'DD/MM/YYYY'), to_date('29/01/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '381.934.3750 x430', 'salvador.schowalter@example.org',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (450, 'Kihn', 'Roob', 'Mr. Tyrel Bauch MD', 'F', to_date('10/12/1994', 'DD/MM/YYYY'), to_date('16/05/2014', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '(307) 865-7407', 'rtowne@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (451, 'Boehm', 'Swaniawski', 'Sonia Koelpin', 'M', to_date('27/04/1985', 'DD/MM/YYYY'), to_date('02/10/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '906-454-1987 x3695', 'hansen.jordan@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (452, 'Breitenberg', 'Ferry', 'Maddison Dickens', 'F', to_date('05/03/1968', 'DD/MM/YYYY'), to_date('01/11/2016', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '1-271-396-0145', 'coby.macejkovic@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (453, 'Nolan', 'Kohler', 'Velva Brekke', 'M', to_date('12/04/1998', 'DD/MM/YYYY'), to_date('14/08/2011', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '7.625.078.734', 'jaida64@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (454, 'Gleichner', 'Schiller', 'Kaela Farrell', 'M', to_date('19/07/1986', 'DD/MM/YYYY'), to_date('25/10/2017', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '7.406.813.067', 'jparker@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (455, 'Mills', 'Littel', 'Margarett Welch', 'F', to_date('12/10/1948', 'DD/MM/YYYY'), to_date('04/09/2011', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '895-315-3358', 'uwilliamson@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (456, 'Koss', 'Prohaska', 'Sam Thompson Sr.', 'M', to_date('10/10/1944', 'DD/MM/YYYY'), to_date('15/08/2010', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '1-887-838-2579 x700', 'freda75@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (457, 'Dickens', 'Dibbert', 'Jessyca Jakubowski DDS', 'M', to_date('02/05/1983', 'DD/MM/YYYY'), to_date('12/12/2012', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '316.876.6917 x3397', 'prosacco.maegan@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (458, 'Koch', 'Pfannerstill', 'Deontae Dare I', 'F', to_date('18/01/1946', 'DD/MM/YYYY'), to_date('20/09/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '569-720-4041', 'tara.torp@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (459, 'Strosin', 'Baumbach', 'Dr. Miles Mills MD', 'M', to_date('29/06/2007', 'DD/MM/YYYY'), to_date('14/04/2013', 'DD/MM/YYYY'), 'San Jose', 'Limon', '16803325671', 'will.keven@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (460, 'Lang', 'Dickens', 'Prof. Tyree Hauck', 'F', to_date('19/12/1999', 'DD/MM/YYYY'), to_date('16/04/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '958-969-7336 x02514', 'nitzsche.cameron@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (461, 'Rolfson', 'Thompson', 'Dr. Emerson Tillman', 'M', to_date('18/02/2003', 'DD/MM/YYYY'), to_date('28/09/2016', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '-4786', 'ymohr@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (462, 'Witting', 'Corwin', 'Dr. Kay Dickinson III', 'M', to_date('19/10/2000', 'DD/MM/YYYY'), to_date('25/03/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '(459) 639-9322', 'borer.gillian@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (463, 'Morissette', 'Kulas', 'Mr. Candelario Bode', 'F', to_date('04/09/1987', 'DD/MM/YYYY'), to_date('24/02/2011', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '5.306.225.670', 'alf78@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (464, 'Wiegand', 'Kertzmann', 'Prof. Velda Walter III', 'M', to_date('10/07/1970', 'DD/MM/YYYY'), to_date('14/07/2016', 'DD/MM/YYYY'), 'Esparza', 'Limon', '(693) 523-7639', 'celia75@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (465, 'Leffler', 'Swift', 'Prof. Jacky Cummerata V', 'F', to_date('15/03/1973', 'DD/MM/YYYY'), to_date('15/08/2019', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '(356) 802-9422 x7941', 'loyce39@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (466, 'Haley', 'Eichmann', 'Jordi Bauch', 'M', to_date('09/04/1996', 'DD/MM/YYYY'), to_date('29/01/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '(491) 637-6308', 'trenton.maggio@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (467, 'Nolan', 'Gutmann', 'Lionel Dicki', 'F', to_date('22/07/1965', 'DD/MM/YYYY'), to_date('27/02/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '242-731-1345', 'gibson.delia@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (468, 'Koss', 'Waelchi', 'Miss Carlee Marks I', 'M', to_date('07/03/1961', 'DD/MM/YYYY'), to_date('18/08/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '728-250-1941 x51571', 'wfritsch@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (469, 'Lynch', 'Krajcik', 'Glen Jenkins', 'M', to_date('08/07/1940', 'DD/MM/YYYY'), to_date('31/05/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '(280) 498-7117', 'hazel.block@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (470, 'Lebsack', 'Kris', 'Helena Grant', 'F', to_date('12/06/1975', 'DD/MM/YYYY'), to_date('15/08/2010', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '-10587', 'mauer@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (471, 'Ritchie', 'Bahringer', 'Miss Frieda Davis I', 'M', to_date('28/04/1940', 'DD/MM/YYYY'), to_date('31/08/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '891.752.3339 x80133', 'frederic.hane@example.org',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (472, 'Russel', 'Glover', 'Prof. Jacquelyn Walsh II', 'F', to_date('13/05/1982', 'DD/MM/YYYY'), to_date('20/04/2011', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '(956) 854-1720 x5065', 'kaley95@example.org',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (473, 'Spinka', 'Kunde', 'Darlene Rodriguez', 'M', to_date('22/03/1962', 'DD/MM/YYYY'), to_date('09/09/2012', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '(918) 229-3143 x6122', 'onie.renner@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (474, 'Shields', 'White', 'Buster Paucek V', 'F', to_date('27/01/1968', 'DD/MM/YYYY'), to_date('20/01/2013', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '(934) 339-5115 x782', 'stroman.jaren@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (475, 'Russel', 'Collier', 'Ova Hermann', 'M', to_date('18/12/2002', 'DD/MM/YYYY'), to_date('22/11/2010', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '1-748-585-6619', 'rhiannon73@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (476, 'Thiel', 'Quigley', 'Yasmin Kuhn', 'M', to_date('08/12/1964', 'DD/MM/YYYY'), to_date('03/12/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '(997) 957-4183 x53654', 'rowena.ledner@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (477, 'Dietrich', 'Hamill', 'Keagan Huel III', 'F', to_date('22/10/1944', 'DD/MM/YYYY'), to_date('31/12/2018', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '+1 (345) 617-1842', 'raven55@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (478, 'Reichert', 'Stark', 'Mr. Alphonso Ratke', 'M', to_date('11/04/1979', 'DD/MM/YYYY'), to_date('04/07/2019', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '1-481-773-2784 x78316', 'fannie57@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (479, 'Dicki', 'Greenfelder', 'Mayra Schaden', 'F', to_date('29/02/2008', 'DD/MM/YYYY'), to_date('28/07/2010', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '297-802-1331 x6391', 'hudson.balistreri@example.net',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (480, 'Denesik', 'Donnelly', 'Gisselle Prosacco', 'M', to_date('13/06/2003', 'DD/MM/YYYY'), to_date('17/04/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '354-509-1812 x639', 'oconnell.frederique@example.org',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (481, 'Connelly', 'Anderson', 'Laurine Macejkovic', 'F', to_date('16/02/2007', 'DD/MM/YYYY'), to_date('04/04/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '(653) 750-4025 x60336', 'aterry@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (482, 'Kohler', 'Daniel', 'Estevan Olson', 'M', to_date('25/10/1980', 'DD/MM/YYYY'), to_date('08/12/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '-4217', 'rowe.janick@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (483, 'Berge', 'Bechtelar', 'Libbie Wisoky IV', 'M', to_date('09/10/1957', 'DD/MM/YYYY'), to_date('11/12/2010', 'DD/MM/YYYY'), 'San Jose', 'Limon', '406-644-1862 x6024', 'monica10@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (484, 'Zulauf', 'Klein', 'Estell Bauch', 'F', to_date('21/09/1954', 'DD/MM/YYYY'), to_date('10/06/2019', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '(730) 697-9684 x62809', 'crist.fern@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (485, 'Nader', 'Spencer', 'Danika Hartmann', 'M', to_date('27/06/2001', 'DD/MM/YYYY'), to_date('10/08/2015', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '(745) 726-3971 x6698', 'olin.zboncak@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (486, 'Lockman', 'Ziemann', 'Prof. Sidney Hoeger I', 'M', to_date('15/02/1995', 'DD/MM/YYYY'), to_date('26/04/2013', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '(789) 214-8953 x7903', 'craig.lang@example.org',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (487, 'Osinski', 'Fadel', 'Hazel Rolfson', 'F', to_date('08/10/1959', 'DD/MM/YYYY'), to_date('23/12/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '+1 (208) 515-9532', 'auer.lily@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (488, 'Windler', 'Metz', 'Dr. Amiya Ryan', 'M', to_date('19/08/1965', 'DD/MM/YYYY'), to_date('19/10/2016', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '(259) 971-7560 x5628', 'berry.hettinger@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (489, 'Rohan', 'Mueller', 'Camilla Nienow', 'F', to_date('03/12/1986', 'DD/MM/YYYY'), to_date('07/01/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '7.073.971.855', 'adela44@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (490, 'Stehr', 'Reynolds', 'Jerad Abbott', 'M', to_date('29/08/1964', 'DD/MM/YYYY'), to_date('03/08/2010', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '+1 (469) 339-2771', 'ashtyn79@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (491, 'Nader', 'Wiza', 'Ariel Frami', 'F', to_date('07/07/1976', 'DD/MM/YYYY'), to_date('28/02/2019', 'DD/MM/YYYY'), 'Liberia', 'Limon', '6.947.413.491', 'stephon53@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (492, 'Prosacco', 'Runolfsson', 'Hector Ebert Sr.', 'M', to_date('19/02/1999', 'DD/MM/YYYY'), to_date('23/07/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '-3092', 'jimmie.hirthe@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (493, 'Oberbrunner', 'Romaguera', 'Ms. Laurianne Schiller', 'M', to_date('25/05/1981', 'DD/MM/YYYY'), to_date('02/06/2014', 'DD/MM/YYYY'), 'Esparza', 'Limon', '1-616-207-7577', 'antonio.spinka@example.org',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (494, 'Little', 'Renner', 'Gerald Wisoky', 'F', to_date('18/09/1981', 'DD/MM/YYYY'), to_date('29/08/2014', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '478-873-7368', 'ratke.ursula@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (495, 'Pagac', 'Maggio', 'Spencer Kautzer Sr.', 'M', to_date('27/09/1945', 'DD/MM/YYYY'), to_date('16/07/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '-10262', 'ieffertz@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (496, 'Brakus', 'Hayes', 'Gwendolyn Hartmann DVM', 'F', to_date('29/01/1992', 'DD/MM/YYYY'), to_date('14/10/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '283.900.7774 x2658', 'harrison77@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (497, 'Lemke', 'Herzog', 'Prof. Riley Morar DVM', 'M', to_date('07/09/1948', 'DD/MM/YYYY'), to_date('18/11/2015', 'DD/MM/YYYY'), 'Esparza', 'Limon', '(682) 604-7515', 'isabell06@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (498, 'Smith', 'Daniel', 'Estel Lueilwitz', 'F', to_date('22/07/1971', 'DD/MM/YYYY'), to_date('08/09/2016', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '575.676.5715 x441', 'jaqueline.nolan@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (499, 'Rippin', 'Stokes', 'Shaun Lueilwitz', 'M', to_date('09/08/1993', 'DD/MM/YYYY'), to_date('10/05/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '826.279.3661 x4317', 'winston.carter@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (500, 'Welch', 'McGlynn', 'Buck Rice', 'M', to_date('03/06/1984', 'DD/MM/YYYY'), to_date('21/02/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '1-946-422-8843 x8048', 'neoma.boehm@example.org', 2);

commit;

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (501, 'Rath', 'Considine', 'Erick Zemlak', 'F', to_date('26/02/1994', 'DD/MM/YYYY'), to_date('21/09/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '14.549.235.332', 'zrenner@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (502, 'Grimes', 'Jones', 'Eladio Mante', 'M', to_date('01/02/1944', 'DD/MM/YYYY'), to_date('29/11/2018', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '1-262-498-8390', 'awolf@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (503, 'Braun', 'Abbott', 'Westley Predovic DDS', 'F', to_date('22/11/1953', 'DD/MM/YYYY'), to_date('19/01/2014', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '(552) 233-5026', 'tremblay.gretchen@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (504, 'Osinski', 'Rosenbaum', 'Garland Dach', 'M', to_date('16/10/2006', 'DD/MM/YYYY'), to_date('26/06/2017', 'DD/MM/YYYY'), 'San Jose', 'Limon', '19.323.764.716', 'tkshlerin@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (505, 'Sauer', 'Lesch', 'Dr. Ronny Bergstrom', 'F', to_date('09/06/1954', 'DD/MM/YYYY'), to_date('23/02/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '19.422.758.250', 'ewelch@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (506, 'Shields', 'Lockman', 'Maureen Glover', 'M', to_date('22/07/1969', 'DD/MM/YYYY'), to_date('30/08/2016', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '1-427-861-4599 x1644', 'mweber@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (507, 'Hagenes', 'Champlin', 'Prof. Dakota Abernathy', 'M', to_date('19/04/1957', 'DD/MM/YYYY'), to_date('29/03/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '(683) 253-3901', 'fjerde@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (508, 'Rice', 'Prosacco', 'Gabriella Koch', 'F', to_date('03/05/1966', 'DD/MM/YYYY'), to_date('09/12/2009', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '971-366-9447', 'danial31@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (509, 'Renner', 'Schmeler', 'Queenie Kuhlman', 'M', to_date('24/10/1984', 'DD/MM/YYYY'), to_date('22/02/2012', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '(542) 434-2787 x8222', 'xweber@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (510, 'Skiles', 'Bartoletti', 'Miss Pauline Lehner', 'M', to_date('14/02/1988', 'DD/MM/YYYY'), to_date('21/10/2013', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '5.973.992.790', 'ernestine10@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (511, 'Rogahn', 'Smitham', 'Friedrich Buckridge', 'F', to_date('26/01/1978', 'DD/MM/YYYY'), to_date('04/08/2017', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '8.649.856.269', 'rkuhic@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (512, 'Ankunding', 'Upton', 'Annamae Streich', 'M', to_date('21/12/1968', 'DD/MM/YYYY'), to_date('25/08/2010', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '1-238-274-8045 x26763', 'adrien.kris@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (513, 'Emmerich', 'Sipes', 'Kendall Kessler', 'F', to_date('28/06/1956', 'DD/MM/YYYY'), to_date('18/12/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '714.810.7607 x845', 'htremblay@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (514, 'Lindgren', 'Considine', 'Jordon Medhurst', 'M', to_date('29/11/2008', 'DD/MM/YYYY'), to_date('13/12/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '686.454.9106 x1488', 'cheyanne13@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (515, 'Kiehn', 'Ruecker', 'Erica Wintheiser', 'F', to_date('14/05/2006', 'DD/MM/YYYY'), to_date('09/01/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '(212) 348-0950 x636', 'krajcik.garrick@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (516, 'Stracke', 'Conn', 'Andreane Dickens', 'M', to_date('03/06/1975', 'DD/MM/YYYY'), to_date('27/01/2014', 'DD/MM/YYYY'), 'Esparza', 'Limon', '-7737', 'caleb.rogahn@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (517, 'Sporer', 'Schimmel', 'Jovany Grimes', 'M', to_date('03/02/2001', 'DD/MM/YYYY'), to_date('05/07/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '216.971.4587 x815', 'mbayer@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (518, 'Mertz', 'Shields', 'Magali Barton', 'F', to_date('13/01/1945', 'DD/MM/YYYY'), to_date('09/06/2010', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '(225) 481-4150', 'simonis.elouise@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (519, 'Kulas', 'Beatty', 'Neoma Marks', 'M', to_date('13/02/1989', 'DD/MM/YYYY'), to_date('23/07/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '-6862', 'padberg.anahi@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (520, 'Gislason', 'Labadie', 'Sim Bergnaum', 'F', to_date('27/05/2008', 'DD/MM/YYYY'), to_date('15/06/2011', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '4.719.870.053', 'sarai.roberts@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (521, 'Carter', 'Mertz', 'Domenic Hoppe', 'M', to_date('10/11/1998', 'DD/MM/YYYY'), to_date('19/08/2018', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '1-594-282-9797 x7927', 'jakubowski.shanel@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (522, 'Murphy', 'Hammes', 'Ms. Lorna Ryan I', 'F', to_date('04/07/1982', 'DD/MM/YYYY'), to_date('08/05/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '+1 (667) 521-9390', 'parker.velda@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (523, 'Goyette', 'Hamill', 'Mauricio Bins II', 'M', to_date('26/04/1967', 'DD/MM/YYYY'), to_date('29/12/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '19.279.483.079', 'yundt.guido@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (524, 'Jast', 'Metz', 'Tyshawn Stiedemann', 'M', to_date('08/09/1995', 'DD/MM/YYYY'), to_date('30/04/2011', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '665.875.6284 x23752', 'ihomenick@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (525, 'Gorczany', 'Bradtke', 'Maryjane Wehner', 'M', to_date('10/09/1955', 'DD/MM/YYYY'), to_date('07/06/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '(923) 450-4633 x5671', 'watsica.bernardo@example.com',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (526, 'Luettgen', 'Veum', 'Ms. Samanta Rutherford V', 'F', to_date('30/05/1942', 'DD/MM/YYYY'), to_date('25/11/2011', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '13782646056', 'denesik.ismael@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (527, 'Block', 'Gorczany', 'Dr. Evelyn Lemke', 'M', to_date('21/08/1998', 'DD/MM/YYYY'), to_date('16/09/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '6.253.260.380', 'yschmidt@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (528, 'Ritchie', 'Rodriguez', 'Mr. Kevin Brekke Jr.', 'F', to_date('19/11/1964', 'DD/MM/YYYY'), to_date('25/03/2012', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '9.562.599.571', 'kemmer.katarina@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (529, 'Lemke', 'Hahn', 'Miss Lillie Durgan', 'M', to_date('03/04/1965', 'DD/MM/YYYY'), to_date('22/07/2018', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '678-628-6609', 'sjones@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (530, 'O''Reilly', 'Carroll', 'Carli Mayer', 'F', to_date('30/07/1961', 'DD/MM/YYYY'), to_date('23/09/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '-10747', 'lwilderman@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (531, 'Swift', 'McKenzie', 'Miss Lauryn Muller PhD', 'M', to_date('10/10/1950', 'DD/MM/YYYY'), to_date('23/02/2019', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '16053420788', 'nledner@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (532, 'Jacobs', 'Hayes', 'Adelia Reichert', 'M', to_date('16/06/1988', 'DD/MM/YYYY'), to_date('30/04/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '18.917.403.432', 'ankunding.maritza@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (533, 'Kreiger', 'Yost', 'Daren Wehner', 'F', to_date('11/08/2004', 'DD/MM/YYYY'), to_date('13/02/2018', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '15.434.180.339', 'sgottlieb@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (534, 'Bayer', 'Hirthe', 'Prof. Sister Hand III', 'M', to_date('01/04/1966', 'DD/MM/YYYY'), to_date('05/09/2014', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '838-216-2385 x4441', 'abdul26@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (535, 'Swift', 'Kozey', 'Darren Jaskolski', 'F', to_date('22/04/1983', 'DD/MM/YYYY'), to_date('20/03/2018', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '1-532-574-9119 x510', 'onikolaus@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (536, 'Christiansen', 'Wiza', 'Aaliyah White II', 'M', to_date('14/11/1956', 'DD/MM/YYYY'), to_date('28/07/2012', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '1-273-879-5564', 'elliot93@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (537, 'Renner', 'Hermann', 'Clotilde Streich', 'F', to_date('21/07/1988', 'DD/MM/YYYY'), to_date('12/04/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '754-878-8117 x71414', 'kerluke.alanna@example.com',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (538, 'Braun', 'Kautzer', 'Clay Christiansen', 'M', to_date('18/06/2003', 'DD/MM/YYYY'), to_date('08/04/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '490-541-1498', 'shyanne.murazik@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (539, 'Kuhn', 'Ortiz', 'Dr. Paul Ritchie', 'M', to_date('11/01/1971', 'DD/MM/YYYY'), to_date('01/08/2010', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '-2061', 'tyra.hintz@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (540, 'Adams', 'Murazik', 'Demond Gottlieb III', 'F', to_date('05/08/1960', 'DD/MM/YYYY'), to_date('31/05/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '15.234.461.193', 'kirstin92@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (541, 'Rohan', 'Jerde', 'Mr. Warren Zulauf', 'M', to_date('17/02/2006', 'DD/MM/YYYY'), to_date('29/11/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '4.363.080.473', 'trystan49@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (542, 'Bailey', 'Torp', 'Ms. Carrie Klein', 'F', to_date('04/02/1988', 'DD/MM/YYYY'), to_date('05/01/2010', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '(910) 431-4749', 'cristal70@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (543, 'Prosacco', 'Abbott', 'Mr. Loyal Gaylord', 'M', to_date('24/12/1997', 'DD/MM/YYYY'), to_date('22/05/2016', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '1-986-370-6331 x606', 'tcasper@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (544, 'Marks', 'Stamm', 'Sharon Hamill IV', 'F', to_date('27/08/1988', 'DD/MM/YYYY'), to_date('12/10/2015', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '897-883-7404 x269', 'okeefe.javonte@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (545, 'Daniel', 'Donnelly', 'Jalon Lang', 'M', to_date('23/01/1964', 'DD/MM/YYYY'), to_date('02/01/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '806-743-4626 x03664', 'nzulauf@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (546, 'Rutherford', 'Schoen', 'Zoie Kozey', 'M', to_date('03/05/1947', 'DD/MM/YYYY'), to_date('02/04/2014', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '(965) 737-6915 x253', 'arne.kuvalis@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (547, 'Miller', 'Kub', 'Paul Ruecker', 'F', to_date('04/12/1955', 'DD/MM/YYYY'), to_date('21/09/2010', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '773-354-1900 x08730', 'zritchie@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (548, 'Hudson', 'Gulgowski', 'Miss Lura Nienow Sr.', 'M', to_date('26/12/1967', 'DD/MM/YYYY'), to_date('04/11/2018', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '(940) 897-2924', 'xwilliamson@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (549, 'Price', 'Block', 'Bradly Kshlerin', 'M', to_date('07/12/1959', 'DD/MM/YYYY'), to_date('01/09/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '948.930.8561 x56761', 'ray.harris@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (550, 'Barton', 'Frami', 'Evangeline Gutmann', 'F', to_date('17/01/2005', 'DD/MM/YYYY'), to_date('24/10/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '(351) 300-4868', 'dach.nigel@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (551, 'Friesen', 'Marks', 'Lillie Feil', 'M', to_date('23/09/1955', 'DD/MM/YYYY'), to_date('19/10/2018', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '1-869-478-8812', 'alena.harber@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (552, 'Anderson', 'Barton', 'Mr. Jamil Kulas', 'F', to_date('16/01/1958', 'DD/MM/YYYY'), to_date('20/05/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '+1 (337) 789-5208', 'fjacobi@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (553, 'Cartwright', 'Kerluke', 'Lonzo Howe', 'M', to_date('31/01/1940', 'DD/MM/YYYY'), to_date('08/07/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '(756) 915-2404 x614', 'harold.stanton@example.org',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (554, 'Wyman', 'Koch', 'Angel Rohan', 'M', to_date('18/06/1993', 'DD/MM/YYYY'), to_date('18/09/2019', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '19808222504', 'darrell31@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (555, 'Fritsch', 'Jenkins', 'Geovanni Ebert MD', 'F', to_date('29/07/1945', 'DD/MM/YYYY'), to_date('11/02/2013', 'DD/MM/YYYY'), 'Esparza', 'Limon', '1-987-736-8056', 'cgrady@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (556, 'Rowe', 'Haley', 'Monique Greenholt', 'M', to_date('08/01/1981', 'DD/MM/YYYY'), to_date('14/05/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '643.483.3655 x244', 'michaela92@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (557, 'Hermiston', 'Becker', 'Ms. Kaitlin Hackett V', 'F', to_date('08/08/1982', 'DD/MM/YYYY'), to_date('11/12/2015', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '(212) 529-7796 x37671', 'lexi11@example.net',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (558, 'Cremin', 'Schroeder', 'Liam McGlynn', 'M', to_date('15/08/1944', 'DD/MM/YYYY'), to_date('04/05/2010', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '1-203-295-2920', 'sarah34@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (559, 'Sawayn', 'Koss', 'Dr. Damien Jenkins', 'F', to_date('02/05/1973', 'DD/MM/YYYY'), to_date('03/09/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '717.517.2119 x892', 'toby77@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (560, 'Larkin', 'Deckow', 'Dameon Feest', 'M', to_date('07/06/1971', 'DD/MM/YYYY'), to_date('16/05/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '-8697', 'egoodwin@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (561, 'McLaughlin', 'Krajcik', 'Reggie Mayert', 'M', to_date('11/09/2002', 'DD/MM/YYYY'), to_date('12/08/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '(585) 758-3426 x21367', 'broob@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (562, 'Gerlach', 'Spencer', 'Cleo Hintz', 'F', to_date('25/01/1955', 'DD/MM/YYYY'), to_date('16/02/2019', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '1-335-519-5171', 'bklocko@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (563, 'Schiller', 'Dare', 'Miss Berneice Schamberger Sr.', 'M', to_date('07/11/1945', 'DD/MM/YYYY'), to_date('11/08/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '1-331-577-6922', 'matilde.hackett@example.com',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (564, 'Ziemann', 'Casper', 'Toni Collins', 'F', to_date('05/07/1986', 'DD/MM/YYYY'), to_date('16/02/2013', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '1-257-776-7927 x6656', 'sabrina26@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (565, 'Cormier', 'Bechtelar', 'Alvera Hackett I', 'M', to_date('02/08/1965', 'DD/MM/YYYY'), to_date('10/10/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '(747) 586-1065 x4899', 'esenger@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (566, 'Kirlin', 'Ferry', 'Joana Bradtke', 'F', to_date('21/06/2008', 'DD/MM/YYYY'), to_date('12/04/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '3.398.030.807', 'kay.runolfsdottir@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (567, 'Dibbert', 'Kunze', 'Shakira Harvey', 'M', to_date('09/08/1974', 'DD/MM/YYYY'), to_date('07/10/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '(597) 797-3604 x07932', 'alta18@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (568, 'Nienow', 'Roberts', 'Hope Metz', 'M', to_date('24/07/2002', 'DD/MM/YYYY'), to_date('29/08/2012', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '-2108', 'zquigley@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (569, 'Moen', 'Huels', 'Prof. Rose Hansen', 'F', to_date('30/06/1974', 'DD/MM/YYYY'), to_date('29/03/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '(248) 761-3241 x751', 'rylee.schinner@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (570, 'Medhurst', 'Gusikowski', 'Ms. Itzel Wisoky PhD', 'M', to_date('22/12/2007', 'DD/MM/YYYY'), to_date('24/10/2019', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '+1 (996) 550-0849', 'ckozey@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (571, 'Ward', 'Goldner', 'Aurelia Hyatt', 'F', to_date('26/11/1960', 'DD/MM/YYYY'), to_date('23/02/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '1-827-348-3250', 'quigley.maximus@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (572, 'Marquardt', 'Berge', 'Prof. Herbert Bins IV', 'M', to_date('30/09/2009', 'DD/MM/YYYY'), to_date('30/05/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '(547) 381-9598', 'wehner.octavia@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (573, 'Boyle', 'Abbott', 'Kirk Little II', 'F', to_date('27/09/1989', 'DD/MM/YYYY'), to_date('17/07/2017', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '3.629.932.895', 'pouros.johnnie@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (574, 'Nienow', 'Williamson', 'Marquis Hartmann', 'M', to_date('29/10/1995', 'DD/MM/YYYY'), to_date('20/01/2018', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '934-968-6466 x7123', 'eleonore.graham@example.org',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (575, 'Denesik', 'Bechtelar', 'Candice Hickle', 'M', to_date('23/02/1960', 'DD/MM/YYYY'), to_date('24/01/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '(757) 431-0940', 'yost.izaiah@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (576, 'Mosciski', 'Raynor', 'Mr. Tomas Mraz IV', 'F', to_date('02/06/1944', 'DD/MM/YYYY'), to_date('21/11/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '1-794-493-1548', 'lmurphy@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (577, 'Kreiger', 'Turcotte', 'Kaia Schowalter', 'M', to_date('30/10/1990', 'DD/MM/YYYY'), to_date('30/01/2011', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '18527991262', 'zulauf.lois@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (578, 'Reinger', 'Gleichner', 'Scot Gusikowski Sr.', 'M', to_date('14/10/2008', 'DD/MM/YYYY'), to_date('29/08/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '6.295.746.012', 'jarrell.yost@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (579, 'Botsford', 'Renner', 'Dr. Summer Padberg', 'F', to_date('08/01/1955', 'DD/MM/YYYY'), to_date('13/04/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '623.559.1254 x438', 'ledner.annabel@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (580, 'Raynor', 'Mante', 'Mr. Gianni Deckow IV', 'M', to_date('01/09/1957', 'DD/MM/YYYY'), to_date('24/04/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '6.138.861.727', 'amaya.boehm@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (581, 'Zulauf', 'Streich', 'Martina Rath', 'F', to_date('12/03/1964', 'DD/MM/YYYY'), to_date('17/04/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '940-624-1293 x338', 'ymarquardt@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (582, 'Mante', 'Block', 'Jeff Cartwright', 'M', to_date('05/03/1975', 'DD/MM/YYYY'), to_date('29/06/2011', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '(914) 829-8271 x5058', 'dsenger@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (583, 'Boyer', 'Hill', 'Lorenza Rosenbaum', 'F', to_date('15/07/1982', 'DD/MM/YYYY'), to_date('15/09/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '746.533.0436 x56220', 'zstroman@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (584, 'Altenwerth', 'Rutherford', 'Bonnie Bode', 'M', to_date('18/06/2005', 'DD/MM/YYYY'), to_date('17/12/2015', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '1-228-462-4473', 'vivienne04@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (585, 'Corkery', 'Price', 'Dagmar Reilly', 'M', to_date('09/02/1973', 'DD/MM/YYYY'), to_date('07/12/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '1-465-338-8074', 'fspencer@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (586, 'Kemmer', 'Baumbach', 'Graham Kuhlman', 'F', to_date('16/03/1976', 'DD/MM/YYYY'), to_date('07/07/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '(910) 502-8966 x09640', 'zheaney@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (587, 'Huel', 'Heller', 'Aubree Hegmann III', 'M', to_date('13/08/1986', 'DD/MM/YYYY'), to_date('16/06/2011', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '-4134', 'cmaggio@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (588, 'Sporer', 'Tremblay', 'Mr. Miles Abernathy Jr.', 'F', to_date('05/02/1988', 'DD/MM/YYYY'), to_date('11/01/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '462.932.6098 x8871', 'nmcclure@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (589, 'Bins', 'Yost', 'Lucious Streich', 'M', to_date('08/11/1982', 'DD/MM/YYYY'), to_date('13/04/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '(379) 566-8226', 'hilton05@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (590, 'Davis', 'Yost', 'Karolann Morissette', 'F', to_date('20/08/1961', 'DD/MM/YYYY'), to_date('15/09/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '1-850-222-8838 x3124', 'jacobs.buddy@example.net',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (591, 'Hickle', 'Roob', 'Hope Sanford Sr.', 'M', to_date('26/01/1977', 'DD/MM/YYYY'), to_date('10/03/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '423-713-4716 x63466', 'vhuels@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (592, 'Zieme', 'Lesch', 'Rogers Kirlin', 'M', to_date('19/07/1948', 'DD/MM/YYYY'), to_date('11/11/2010', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '1-457-873-1342', 'yhamill@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (593, 'Greenholt', 'Carroll', 'Lue Kirlin', 'F', to_date('27/02/1990', 'DD/MM/YYYY'), to_date('19/07/2016', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '828-542-8397 x622', 'walter.tromp@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (594, 'Schuster', 'Lang', 'Gennaro Marvin', 'M', to_date('26/05/1957', 'DD/MM/YYYY'), to_date('20/10/2017', 'DD/MM/YYYY'), 'Esparza', 'Limon', '673-389-8071 x328', 'langosh.emmalee@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (595, 'Abbott', 'Considine', 'Terence Farrell III', 'F', to_date('11/05/1964', 'DD/MM/YYYY'), to_date('06/11/2011', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '983-646-9306', 'mckayla.wilderman@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (596, 'Powlowski', 'Dibbert', 'Nedra Brakus', 'M', to_date('21/04/2003', 'DD/MM/YYYY'), to_date('06/12/2017', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '(316) 816-7222 x03122', 'larissa90@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (597, 'Little', 'Walsh', 'Jamie Schneider', 'F', to_date('09/10/1977', 'DD/MM/YYYY'), to_date('19/03/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '9.148.386.608', 'qschaden@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (598, 'Hermiston', 'Kub', 'May Gutkowski', 'M', to_date('24/09/1986', 'DD/MM/YYYY'), to_date('28/07/2013', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '684-830-1283 x361', 'rath.vincent@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (599, 'Harber', 'Emmerich', 'Cora Reichert', 'M', to_date('18/04/1964', 'DD/MM/YYYY'), to_date('25/11/2015', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '-1695', 'gillian.stamm@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (600, 'Dicki', 'Reynolds', 'Dr. Cristopher Klocko', 'F', to_date('21/02/1971', 'DD/MM/YYYY'), to_date('18/04/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '497-844-7538 x70132', 'hagenes.roxane@example.com',
   1);

commit;

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (601, 'Corkery', 'Dickinson', 'Arlene Fay', 'M', to_date('18/05/1957', 'DD/MM/YYYY'), to_date('10/06/2019', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '15.357.371.896', 'ifriesen@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (602, 'Medhurst', 'Swift', 'Isaiah Torp IV', 'M', to_date('04/03/1972', 'DD/MM/YYYY'), to_date('16/02/2011', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '17309387250', 'ileffler@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (603, 'Connelly', 'Kunze', 'Ms. Antonia Lubowitz II', 'F', to_date('03/06/1998', 'DD/MM/YYYY'), to_date('29/09/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '(348) 357-9024', 'mariela62@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (604, 'McDermott', 'Kunze', 'Miss Mary Feeney IV', 'M', to_date('16/03/2002', 'DD/MM/YYYY'), to_date('23/06/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '(583) 457-4581', 'sipes.salvador@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (605, 'Dicki', 'Fisher', 'Romaine Berge', 'F', to_date('04/10/1942', 'DD/MM/YYYY'), to_date('30/11/2009', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '(559) 664-6037 x86793', 'schaefer.birdie@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (606, 'Yundt', 'Kihn', 'Maureen Koepp', 'M', to_date('01/04/2000', 'DD/MM/YYYY'), to_date('23/05/2018', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '(909) 680-9329', 'avis.welch@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (607, 'Lehner', 'Wolff', 'Edna Nicolas DVM', 'F', to_date('25/05/1978', 'DD/MM/YYYY'), to_date('15/04/2018', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '914-307-8446 x4632', 'dell.turner@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (608, 'Moen', 'Wyman', 'Abigail Hand', 'M', to_date('07/09/1941', 'DD/MM/YYYY'), to_date('23/04/2014', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '18.642.922.924', 'cummerata.carlee@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (609, 'Koepp', 'Hartmann', 'Philip Leuschke V', 'M', to_date('26/06/2000', 'DD/MM/YYYY'), to_date('31/08/2018', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '(392) 433-8607 x74608', 'ebert.peggie@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (610, 'Stoltenberg', 'Schmitt', 'Mr. Kobe Stamm DVM', 'F', to_date('31/05/1990', 'DD/MM/YYYY'), to_date('29/05/2013', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '594.801.0158 x712', 'kherman@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (611, 'Langosh', 'Reinger', 'Lilyan Ruecker Jr.', 'M', to_date('24/10/1989', 'DD/MM/YYYY'), to_date('05/02/2013', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '258-529-1275 x8765', 'hayden.skiles@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (612, 'Thompson', 'Davis', 'Dr. Malcolm Leuschke', 'F', to_date('21/06/1963', 'DD/MM/YYYY'), to_date('14/12/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '(569) 434-3256', 'gfritsch@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (613, 'Heaney', 'Tremblay', 'Prof. Seamus Toy', 'M', to_date('30/04/2008', 'DD/MM/YYYY'), to_date('04/08/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '915.582.8132 x8496', 'ardella.bartoletti@example.org',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (614, 'Ritchie', 'Kovacek', 'Kristin Hettinger', 'F', to_date('25/12/1957', 'DD/MM/YYYY'), to_date('23/12/2018', 'DD/MM/YYYY'), 'Liberia', 'Limon', '1-752-435-3883 x488', 'paula24@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (615, 'Erdman', 'Runolfsdottir', 'Zechariah Schulist', 'M', to_date('18/03/2001', 'DD/MM/YYYY'), to_date('12/01/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '(849) 302-7918 x660', 'itoy@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (616, 'Vandervort', 'Hamill', 'Dr. Camren Bernier MD', 'M', to_date('15/12/1941', 'DD/MM/YYYY'), to_date('18/07/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '497-836-0292 x69836', 'kuhn.neoma@example.com',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (617, 'Kuphal', 'Flatley', 'Virginia Cummerata', 'M', to_date('26/06/1985', 'DD/MM/YYYY'), to_date('10/11/2018', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '1-614-247-0787 x303', 'fahey.johnny@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (618, 'Pacocha', 'Sipes', 'Brennon Homenick', 'F', to_date('04/12/1954', 'DD/MM/YYYY'), to_date('24/02/2012', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '6.289.491.377', 'cblick@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (619, 'Streich', 'Corkery', 'Miss Caitlyn Orn II', 'M', to_date('25/06/1998', 'DD/MM/YYYY'), to_date('07/11/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '-10912', 'evan81@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (620, 'Kuhic', 'Legros', 'Benjamin McLaughlin', 'F', to_date('19/09/1950', 'DD/MM/YYYY'), to_date('17/09/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '325-974-0716', 'shayne46@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (621, 'Ullrich', 'Haag', 'Kirstin Schaden', 'M', to_date('18/08/1986', 'DD/MM/YYYY'), to_date('28/10/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '871-560-3102', 'talia.balistreri@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (622, 'Schneider', 'Heaney', 'Evie Lubowitz', 'F', to_date('19/05/1951', 'DD/MM/YYYY'), to_date('22/03/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '(275) 960-4234', 'rfay@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (623, 'Lakin', 'Swaniawski', 'Kathryn Runolfsson', 'M', to_date('03/02/1989', 'DD/MM/YYYY'), to_date('23/01/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '1-572-530-0258', 'ondricka.reynold@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (624, 'Rolfson', 'Littel', 'Meaghan Daniel', 'M', to_date('31/05/1962', 'DD/MM/YYYY'), to_date('14/09/2019', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '+1 (454) 579-3936', 'ngreenfelder@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (625, 'Lehner', 'Rowe', 'Chase Heaney', 'F', to_date('22/09/1986', 'DD/MM/YYYY'), to_date('06/07/2016', 'DD/MM/YYYY'), 'Liberia', 'Limon', '1-719-392-0198 x2963', 'zbuckridge@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (626, 'Bernhard', 'Collier', 'Kade Armstrong', 'M', to_date('08/03/1943', 'DD/MM/YYYY'), to_date('22/03/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '16.602.547.436', 'ethelyn.torp@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (627, 'Fritsch', 'Heidenreich', 'Davin Kulas', 'F', to_date('01/02/1978', 'DD/MM/YYYY'), to_date('28/07/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '4.364.876.502', 'hildegard.pfeffer@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (628, 'Keeling', 'Huels', 'Ryann Cummings Sr.', 'M', to_date('27/07/1951', 'DD/MM/YYYY'), to_date('20/07/2016', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '(876) 203-9269', 'fbogisich@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (629, 'Hagenes', 'Jacobson', 'Robyn Haley', 'F', to_date('25/07/1991', 'DD/MM/YYYY'), to_date('23/03/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '847-898-9488 x2950', 'cedrick09@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (630, 'Windler', 'Herman', 'Mrs. Holly Ryan', 'M', to_date('03/03/1947', 'DD/MM/YYYY'), to_date('30/06/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '1-310-216-8078', 'josefa.bradtke@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (631, 'Heller', 'Wintheiser', 'Mr. Stevie Ondricka', 'M', to_date('25/08/2004', 'DD/MM/YYYY'), to_date('09/10/2011', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '13.615.736.626', 'miller57@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (632, 'Doyle', 'Boehm', 'Dr. Chadd Kreiger', 'F', to_date('10/03/1959', 'DD/MM/YYYY'), to_date('08/11/2014', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '1-890-797-6682 x17748', 'medhurst.adaline@example.com',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (633, 'Schaden', 'Okuneva', 'David Ankunding', 'M', to_date('05/08/1994', 'DD/MM/YYYY'), to_date('22/10/2013', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '(939) 705-7265 x691', 'cole.ivory@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (634, 'Swift', 'Kautzer', 'Greg Satterfield I', 'F', to_date('13/02/1952', 'DD/MM/YYYY'), to_date('08/07/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '(227) 374-9501 x11836', 'otho.kris@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (635, 'Cole', 'Cronin', 'Teagan Dickinson', 'M', to_date('28/03/1996', 'DD/MM/YYYY'), to_date('24/08/2010', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '(960) 577-0464 x26732', 'kertzmann.ned@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (636, 'McClure', 'Schamberger', 'Nicklaus Leffler', 'F', to_date('02/05/1964', 'DD/MM/YYYY'), to_date('16/11/2012', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '(440) 346-2104 x63236', 'wilmer77@example.net',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (637, 'Abshire', 'Quigley', 'Rosamond Jacobson', 'M', to_date('21/04/1944', 'DD/MM/YYYY'), to_date('11/11/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '18097712212', 'west.evans@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (638, 'Reynolds', 'Larkin', 'Mrs. Shanny Durgan II', 'M', to_date('26/08/1997', 'DD/MM/YYYY'), to_date('29/02/2012', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '292.956.6010 x1623', 'egrady@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (639, 'O''Hara', 'Daniel', 'Loyal Oberbrunner', 'F', to_date('21/08/1969', 'DD/MM/YYYY'), to_date('29/05/2017', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '1-617-282-5880', 'ghagenes@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (640, 'Crist', 'Hammes', 'Dr. Armani Terry I', 'M', to_date('31/05/1980', 'DD/MM/YYYY'), to_date('03/06/2012', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '(205) 630-4672 x0911', 'jacobson.ferne@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (641, 'Pfeffer', 'Hansen', 'Mr. Rowland Jacobi', 'M', to_date('28/08/1949', 'DD/MM/YYYY'), to_date('10/07/2017', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '519.560.5843 x14139', 'hilpert.esther@example.org',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (642, 'Bednar', 'O''Conner', 'Alysson Mayert', 'F', to_date('11/04/1944', 'DD/MM/YYYY'), to_date('22/03/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '+1 (675) 946-2733', 'igrady@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (643, 'Treutel', 'Denesik', 'Wilbert Rogahn', 'M', to_date('10/07/2006', 'DD/MM/YYYY'), to_date('16/11/2010', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '1-431-342-5309 x1754', 'roob.laverne@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (644, 'Waelchi', 'Emmerich', 'Arnold Homenick Sr.', 'F', to_date('18/04/1982', 'DD/MM/YYYY'), to_date('19/04/2012', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '989.878.4576 x05803', 'hlowe@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (645, 'Thompson', 'Gerhold', 'Norberto Stroman', 'M', to_date('17/09/1972', 'DD/MM/YYYY'), to_date('17/03/2016', 'DD/MM/YYYY'), 'Liberia', 'Limon', '529-234-2872 x837', 'lmcglynn@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (646, 'Miller', 'Brekke', 'Ignacio Durgan', 'M', to_date('24/11/1983', 'DD/MM/YYYY'), to_date('12/12/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '414.576.5471 x152', 'monahan.gideon@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (647, 'Kiehn', 'Konopelski', 'Trever Willms', 'F', to_date('23/10/1960', 'DD/MM/YYYY'), to_date('28/09/2017', 'DD/MM/YYYY'), 'San Jose', 'Limon', '513-814-4486 x631', 'green.angeline@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (648, 'Metz', 'Brown', 'Ms. Yasmeen Ryan', 'M', to_date('16/02/1943', 'DD/MM/YYYY'), to_date('10/05/2015', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '18106059800', 'wyatt.hudson@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (649, 'Ernser', 'McDermott', 'Mrs. Amelie O''Reilly', 'F', to_date('03/09/1973', 'DD/MM/YYYY'), to_date('30/08/2012', 'DD/MM/YYYY'), 'Esparza', 'Limon', '1-907-636-2227 x0406', 'emmerich.zita@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (650, 'Okuneva', 'Brakus', 'Nils Gorczany', 'M', to_date('15/01/2001', 'DD/MM/YYYY'), to_date('05/07/2013', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '995-818-5444', 'torphy.adolphus@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (651, 'Romaguera', 'Bailey', 'Megane Stracke', 'F', to_date('14/09/1962', 'DD/MM/YYYY'), to_date('27/08/2012', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '(646) 953-7353 x6998', 'norbert08@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (652, 'Crooks', 'Bruen', 'Garrett Hintz', 'M', to_date('09/03/1975', 'DD/MM/YYYY'), to_date('31/01/2018', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '5.363.789.350', 'uleannon@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (653, 'Hayes', 'Moen', 'Euna Stanton', 'M', to_date('25/11/1954', 'DD/MM/YYYY'), to_date('12/08/2019', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '1-261-483-6849 x3111', 'zoe06@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (654, 'Turner', 'Hahn', 'Jewell Lesch', 'F', to_date('25/06/1990', 'DD/MM/YYYY'), to_date('19/07/2019', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '638.382.4655 x0999', 'skerluke@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (655, 'Friesen', 'Hansen', 'Dr. Casey Smith DVM', 'M', to_date('07/02/1982', 'DD/MM/YYYY'), to_date('10/01/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '489.436.5684 x9593', 'destini.leannon@example.com',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (656, 'Beer', 'Kerluke', 'Miss Blanca Rempel', 'F', to_date('14/10/2007', 'DD/MM/YYYY'), to_date('14/08/2014', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '(669) 789-7105', 'libbie93@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (657, 'Brekke', 'Haley', 'Ransom Morissette', 'M', to_date('13/05/1954', 'DD/MM/YYYY'), to_date('11/12/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '753.569.5272 x471', 'lbins@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (658, 'Bergnaum', 'Moore', 'Deondre Jast II', 'F', to_date('19/08/1990', 'DD/MM/YYYY'), to_date('23/04/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '(910) 431-8556', 'iwill@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (659, 'Towne', 'Cremin', 'Dax Gutmann III', 'M', to_date('03/07/1984', 'DD/MM/YYYY'), to_date('28/10/2015', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '(348) 262-8970', 'jena.mueller@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (660, 'Treutel', 'Auer', 'Maida Bednar', 'M', to_date('20/01/1953', 'DD/MM/YYYY'), to_date('23/02/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '(327) 506-5699', 'green.alexie@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (661, 'Torp', 'Dibbert', 'Celia Tromp', 'F', to_date('24/09/2000', 'DD/MM/YYYY'), to_date('23/03/2017', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '(248) 945-7277', 'littel.alisha@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (662, 'Funk', 'Swaniawski', 'Dr. Travis Kozey II', 'M', to_date('04/05/1940', 'DD/MM/YYYY'), to_date('08/02/2014', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '846-275-1531 x999', 'ernestina.corkery@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (663, 'Bernhard', 'Lockman', 'Enrico Sporer', 'F', to_date('27/07/1994', 'DD/MM/YYYY'), to_date('24/08/2014', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '710-625-5121 x43834', 'afeeney@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (664, 'Rippin', 'Tromp', 'Miss Stephania Becker', 'M', to_date('19/06/1981', 'DD/MM/YYYY'), to_date('04/09/2017', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '3.178.935.405', 'duncan94@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (665, 'Cronin', 'Toy', 'Denis Cummerata III', 'F', to_date('16/08/2006', 'DD/MM/YYYY'), to_date('24/09/2017', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '-2793', 'salvador07@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (666, 'Shields', 'Rosenbaum', 'Dashawn Lueilwitz', 'M', to_date('17/01/1981', 'DD/MM/YYYY'), to_date('17/12/2014', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '1-654-347-7351', 'mohr.chandler@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (667, 'Stanton', 'Connelly', 'Jeremie Kreiger', 'M', to_date('06/03/1984', 'DD/MM/YYYY'), to_date('26/08/2016', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '(351) 225-6478 x490', 'lueilwitz.isai@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (668, 'Ward', 'Effertz', 'Ms. Vicky Mraz V', 'F', to_date('08/12/1983', 'DD/MM/YYYY'), to_date('28/10/2012', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '4.255.229.794', 'domenick.kiehn@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (669, 'Kautzer', 'Collier', 'Mrs. Twila Schumm Sr.', 'M', to_date('15/05/1969', 'DD/MM/YYYY'), to_date('26/03/2018', 'DD/MM/YYYY'), 'Liberia', 'Limon', '395.732.0266 x79381', 'oral16@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (670, 'Medhurst', 'Leuschke', 'Christina Weissnat', 'M', to_date('05/12/1991', 'DD/MM/YYYY'), to_date('28/05/2014', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '(851) 485-9631 x074', 'riley55@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (671, 'Klocko', 'Hoppe', 'Cedrick Cronin', 'F', to_date('11/01/1989', 'DD/MM/YYYY'), to_date('21/03/2010', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '(660) 871-7190', 'raheem21@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (672, 'Daniel', 'Ledner', 'Lauriane Hettinger', 'M', to_date('07/03/1984', 'DD/MM/YYYY'), to_date('27/03/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '-4697', 'krajcik.antonietta@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (673, 'Jast', 'Huel', 'Prof. Cristina Hauck III', 'F', to_date('29/10/1969', 'DD/MM/YYYY'), to_date('23/02/2019', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '6.239.434.578', 'fritsch.alejandrin@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (674, 'Labadie', 'Keebler', 'Mr. Cleveland Beatty', 'M', to_date('23/12/1984', 'DD/MM/YYYY'), to_date('13/04/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '14075097908', 'darwin37@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (675, 'Torp', 'Bernier', 'Miss Lavonne Emard', 'F', to_date('07/06/1992', 'DD/MM/YYYY'), to_date('04/10/2014', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '664.439.4896 x4786', 'everette52@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (676, 'Rodriguez', 'McCullough', 'Berneice Wisoky', 'M', to_date('21/11/1956', 'DD/MM/YYYY'), to_date('28/10/2010', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '(360) 987-9158 x0231', 'toney.nolan@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (677, 'Zemlak', 'Roberts', 'Rollin O''Reilly', 'M', to_date('03/05/1967', 'DD/MM/YYYY'), to_date('13/03/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '15.328.728.997', 'elroy13@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (678, 'Dooley', 'Weissnat', 'Isabelle Cronin DDS', 'F', to_date('04/06/1959', 'DD/MM/YYYY'), to_date('28/08/2017', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '4.608.910.158', 'jakubowski.gay@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (679, 'Gibson', 'Gleichner', 'Miss Leatha Heller', 'M', to_date('25/01/2003', 'DD/MM/YYYY'), to_date('05/10/2011', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '661.487.9051 x12551', 'jesse24@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (680, 'Friesen', 'Schulist', 'Miss Vernie Cummerata', 'F', to_date('18/07/1986', 'DD/MM/YYYY'), to_date('13/03/2016', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '1-461-434-3616', 'augustus.schaefer@example.com',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (681, 'Stokes', 'Zieme', 'Mr. Eliezer Schimmel', 'M', to_date('30/01/1985', 'DD/MM/YYYY'), to_date('16/06/2017', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '4.933.764.138', 'emmy.ullrich@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (682, 'Corkery', 'Hoeger', 'Carmen Mohr', 'F', to_date('02/02/1948', 'DD/MM/YYYY'), to_date('27/06/2014', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '1-781-418-5366', 'mellie34@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (683, 'Fadel', 'Bode', 'Oscar Rath', 'M', to_date('04/01/2001', 'DD/MM/YYYY'), to_date('16/02/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '661.301.7746 x216', 'breanne48@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (684, 'Emmerich', 'Bartoletti', 'Harley Kessler', 'M', to_date('27/11/1996', 'DD/MM/YYYY'), to_date('17/05/2015', 'DD/MM/YYYY'), 'Liberia', 'Limon', '413-817-4608', 'alisa88@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (685, 'Boyer', 'Goyette', 'Bella McCullough', 'F', to_date('14/02/1967', 'DD/MM/YYYY'), to_date('26/06/2019', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '1-804-974-3663', 'klein.darrick@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (686, 'White', 'Koss', 'Dee Douglas', 'M', to_date('23/06/1987', 'DD/MM/YYYY'), to_date('05/03/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '1-296-241-7844 x10486', 'jerry.brown@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (687, 'Miller', 'Hauck', 'Noe Herzog', 'F', to_date('10/07/1945', 'DD/MM/YYYY'), to_date('06/09/2013', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '-2986', 'trevion.treutel@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (688, 'Steuber', 'Marvin', 'Ida Prosacco', 'M', to_date('06/04/1968', 'DD/MM/YYYY'), to_date('22/04/2018', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '1-614-791-1144', 'fahey.aisha@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (689, 'Bogan', 'Goldner', 'Anabelle Morar', 'F', to_date('09/01/1992', 'DD/MM/YYYY'), to_date('15/01/2010', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '18.138.551.281', 'rroberts@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (690, 'McClure', 'Keeling', 'Thad Bins DVM', 'M', to_date('19/07/1948', 'DD/MM/YYYY'), to_date('10/04/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '310.836.4375 x44751', 'briana98@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (691, 'Hauck', 'Towne', 'Ms. Alene Dach V', 'M', to_date('04/12/1990', 'DD/MM/YYYY'), to_date('18/05/2018', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '881-616-1900', 'irma.streich@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (692, 'Waters', 'Hirthe', 'Alexander Cummerata', 'F', to_date('01/10/1943', 'DD/MM/YYYY'), to_date('04/02/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '590.913.2093 x39429', 'cmaggio@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (693, 'Terry', 'Gerlach', 'Imani Dickinson', 'M', to_date('03/09/2000', 'DD/MM/YYYY'), to_date('09/01/2015', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '(384) 385-4506 x9612', 'moore.freda@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (694, 'Monahan', 'Jast', 'Aliyah Schimmel Sr.', 'M', to_date('09/01/1990', 'DD/MM/YYYY'), to_date('24/11/2011', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '507.296.6181 x0459', 'rosario87@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (695, 'Yundt', 'Carroll', 'Prof. Golden Kihn MD', 'F', to_date('03/12/1996', 'DD/MM/YYYY'), to_date('13/12/2009', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '18.894.775.141', 'doug66@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (696, 'Goodwin', 'O''Hara', 'Prof. Kevon Hickle', 'M', to_date('28/08/2001', 'DD/MM/YYYY'), to_date('14/07/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '-4430', 'epacocha@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (697, 'McKenzie', 'Skiles', 'Erick Eichmann PhD', 'F', to_date('01/02/1966', 'DD/MM/YYYY'), to_date('01/05/2010', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '(681) 594-1990', 'agnes57@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (698, 'King', 'Fahey', 'Prof. Brayan Durgan II', 'M', to_date('10/09/1960', 'DD/MM/YYYY'), to_date('09/05/2010', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '12426917940', 'pablo71@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (699, 'Oberbrunner', 'Labadie', 'Miss Marlene Klocko MD', 'F', to_date('17/11/1986', 'DD/MM/YYYY'), to_date('25/03/2010', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '(419) 404-1152 x4394', 'mcclure.demetris@example.com',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (700, 'Terry', 'Kozey', 'Reina Nienow DDS', 'M', to_date('09/05/1969', 'DD/MM/YYYY'), to_date('25/11/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '725-555-3042 x854', 'sigmund86@example.com', 6);

commit;

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (701, 'Stehr', 'Champlin', 'Mallory Huel', 'M', to_date('15/06/1991', 'DD/MM/YYYY'), to_date('15/12/2013', 'DD/MM/YYYY'), 'Liberia', 'Limon', '624-772-7997 x7224', 'kovacek.scarlett@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (702, 'Hackett', 'Weissnat', 'Lucious Schuppe', 'F', to_date('10/12/1962', 'DD/MM/YYYY'), to_date('17/03/2016', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '1-883-915-7670', 'gaylord59@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (703, 'Ryan', 'Lueilwitz', 'Chadd Hilpert', 'M', to_date('03/03/1978', 'DD/MM/YYYY'), to_date('07/07/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '5.083.749.951', 'lgorczany@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (704, 'Kutch', 'Howe', 'Timothy Cronin', 'F', to_date('18/08/1963', 'DD/MM/YYYY'), to_date('28/01/2016', 'DD/MM/YYYY'), 'Esparza', 'Limon', '1-442-359-6528 x50754', 'mosciski.rhoda@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (705, 'Kirlin', 'Conroy', 'Natalie Tremblay', 'M', to_date('13/01/1961', 'DD/MM/YYYY'), to_date('25/02/2012', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '-4488', 'jstracke@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (706, 'Legros', 'Bergnaum', 'Keven Welch', 'F', to_date('11/04/1989', 'DD/MM/YYYY'), to_date('13/05/2011', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '+1 (548) 500-8093', 'lavada76@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (707, 'Reichel', 'Sawayn', 'Sydney McGlynn III', 'M', to_date('22/04/1992', 'DD/MM/YYYY'), to_date('27/02/2011', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '1-212-943-0858 x7908', 'april73@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (708, 'Mraz', 'Schuppe', 'Demetris Dibbert Sr.', 'M', to_date('08/11/1998', 'DD/MM/YYYY'), to_date('13/01/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '(314) 913-4284', 'larkin.davion@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (709, 'Hill', 'Konopelski', 'Donavon Dibbert', 'M', to_date('18/11/1971', 'DD/MM/YYYY'), to_date('02/05/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '(281) 275-2055 x61790', 'bartholome30@example.com',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (710, 'Fay', 'Goyette', 'Daija Krajcik', 'F', to_date('30/07/1965', 'DD/MM/YYYY'), to_date('08/02/2019', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '(756) 768-3148 x2995', 'leuschke.ardith@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (711, 'Gulgowski', 'Frami', 'Ada Medhurst III', 'M', to_date('19/12/2005', 'DD/MM/YYYY'), to_date('08/08/2018', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '(836) 328-6314 x2634', 'ignacio98@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (712, 'Conroy', 'Abernathy', 'Ole Barrows', 'F', to_date('20/08/1963', 'DD/MM/YYYY'), to_date('22/01/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '16025457254', 'justus.powlowski@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (713, 'Klein', 'Rau', 'Ms. Lelah Kemmer DDS', 'M', to_date('03/08/1952', 'DD/MM/YYYY'), to_date('23/03/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '217.818.9991 x27979', 'libby32@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (714, 'Strosin', 'Hermann', 'Coleman Larkin', 'F', to_date('19/05/1985', 'DD/MM/YYYY'), to_date('23/11/2010', 'DD/MM/YYYY'), 'Liberia', 'Limon', '(636) 298-9645 x11066', 'pacocha.devin@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (715, 'Larson', 'Kirlin', 'Prof. Rachel Aufderhar I', 'M', to_date('21/07/1949', 'DD/MM/YYYY'), to_date('06/03/2019', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '(487) 999-9823 x14589', 'franecki.wilmer@example.com',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (716, 'Mosciski', 'Glover', 'Dr. Jessie Abernathy', 'M', to_date('03/10/2009', 'DD/MM/YYYY'), to_date('06/07/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '(817) 817-2649 x820', 'will21@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (717, 'Rippin', 'Spencer', 'Ludie Kunde', 'F', to_date('18/11/1970', 'DD/MM/YYYY'), to_date('29/06/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '(740) 869-0011 x857', 'jacobi.meagan@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (718, 'DuBuque', 'Wiza', 'Prof. Palma Nikolaus', 'M', to_date('15/01/1973', 'DD/MM/YYYY'), to_date('17/02/2012', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '1-585-336-7914', 'mmonahan@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (719, 'Hintz', 'Wisoky', 'Christop Turcotte V', 'F', to_date('12/07/1986', 'DD/MM/YYYY'), to_date('09/11/2011', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '6.243.550.262', 'kvolkman@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (720, 'Schaden', 'Blick', 'Michel Nicolas MD', 'M', to_date('09/11/1966', 'DD/MM/YYYY'), to_date('19/07/2015', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '1-638-408-4408', 'omari.barton@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (721, 'Price', 'Moen', 'Dr. Furman Strosin', 'F', to_date('11/12/1982', 'DD/MM/YYYY'), to_date('11/01/2017', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '1-863-618-6782 x2223', 'hirthe.hobart@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (722, 'Wilkinson', 'Schneider', 'Miss Hailie Lynch I', 'M', to_date('04/06/1983', 'DD/MM/YYYY'), to_date('09/06/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '1-908-960-4462 x74269', 'bpouros@example.net',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (723, 'Kassulke', 'Hermann', 'Prof. Mya Jacobi', 'M', to_date('23/07/2001', 'DD/MM/YYYY'), to_date('09/09/2017', 'DD/MM/YYYY'), 'San Jose', 'Limon', '701-971-5006', 'wgoyette@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (724, 'Batz', 'Waters', 'Emilie Ledner II', 'F', to_date('27/11/1995', 'DD/MM/YYYY'), to_date('05/07/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '746-989-0442', 'ramona44@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (725, 'Hudson', 'Johnson', 'Mrs. Magali Bauch', 'M', to_date('22/10/1965', 'DD/MM/YYYY'), to_date('11/09/2011', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '294.378.2666 x918', 'vwill@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (726, 'Reinger', 'Jacobi', 'Dereck Langworth', 'F', to_date('13/12/1955', 'DD/MM/YYYY'), to_date('17/09/2017', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '1-612-837-9587 x357', 'alexzander53@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (727, 'Gerhold', 'Klocko', 'Zoila Stoltenberg III', 'M', to_date('25/02/1979', 'DD/MM/YYYY'), to_date('28/09/2016', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '+1 (829) 668-2459', 'kunze.drew@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (728, 'Volkman', 'Ledner', 'Arjun Pagac DDS', 'F', to_date('21/09/1983', 'DD/MM/YYYY'), to_date('15/07/2011', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '779-442-1474 x79260', 'ziemann.daisha@example.com',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (729, 'Schoen', 'Moen', 'Ada Moore', 'M', to_date('30/08/1992', 'DD/MM/YYYY'), to_date('12/11/2010', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '583-237-1676 x43200', 'rolando.botsford@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (730, 'Will', 'Funk', 'Miss Alysson Mraz Sr.', 'M', to_date('18/07/2008', 'DD/MM/YYYY'), to_date('26/11/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '481.365.5754 x7486', 'judge.jones@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (731, 'Walter', 'Mann', 'Prof. Stacey Bernier DDS', 'F', to_date('31/10/1956', 'DD/MM/YYYY'), to_date('08/01/2014', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '1-787-394-3940 x157', 'mitchel20@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (732, 'Kertzmann', 'Dibbert', 'Myra Stanton III', 'M', to_date('05/02/2004', 'DD/MM/YYYY'), to_date('23/09/2017', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '(958) 535-9524', 'khalid.shields@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (733, 'Kutch', 'Ratke', 'Myrtle Stiedemann', 'M', to_date('19/10/1951', 'DD/MM/YYYY'), to_date('10/08/2014', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '(386) 878-6447 x466', 'lillian33@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (734, 'Walsh', 'Bernier', 'Rupert Hamill', 'F', to_date('07/09/2001', 'DD/MM/YYYY'), to_date('28/10/2018', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '1-852-320-6269 x7504', 'kiel.schamberger@example.org',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (735, 'Schimmel', 'Wisoky', 'Itzel Herman', 'M', to_date('31/05/1951', 'DD/MM/YYYY'), to_date('23/07/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '1-870-563-4682', 'winifred.toy@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (736, 'Dickinson', 'Beier', 'Keira Pollich', 'F', to_date('17/04/1986', 'DD/MM/YYYY'), to_date('01/04/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '1-550-490-0853', 'wilfred23@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (737, 'Denesik', 'Gutkowski', 'Quinn Hettinger', 'M', to_date('04/04/2003', 'DD/MM/YYYY'), to_date('07/07/2015', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '8.535.706.124', 'andreanne40@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (738, 'Rodriguez', 'Daniel', 'Miss Marilie McGlynn DVM', 'M', to_date('08/08/1973', 'DD/MM/YYYY'), to_date('30/03/2015', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '+1 (343) 524-0495', 'lionel.predovic@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (739, 'Beier', 'Murphy', 'Summer Wuckert', 'F', to_date('07/01/1940', 'DD/MM/YYYY'), to_date('16/11/2015', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '(294) 338-8757 x8941', 'reichel.sister@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (740, 'Hodkiewicz', 'Wiza', 'Reed Stanton', 'M', to_date('06/12/1959', 'DD/MM/YYYY'), to_date('24/04/2011', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '791.628.9767 x8195', 'ndickinson@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (741, 'Flatley', 'Gislason', 'Prof. Bernie Bogisich', 'F', to_date('27/10/1957', 'DD/MM/YYYY'), to_date('31/08/2012', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '(865) 581-7083 x5563', 'webster.hudson@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (742, 'Berge', 'Grady', 'Curt Harvey MD', 'M', to_date('11/11/1993', 'DD/MM/YYYY'), to_date('26/06/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '303.376.9465 x7161', 'levi.baumbach@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (743, 'Rodriguez', 'Kuvalis', 'Russ Jacobs', 'F', to_date('24/05/1987', 'DD/MM/YYYY'), to_date('03/11/2018', 'DD/MM/YYYY'), 'Liberia', 'Limon', '1-957-505-3103 x892', 'ybeer@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (744, 'Walsh', 'O''Reilly', 'Carlotta Dooley', 'M', to_date('23/06/2004', 'DD/MM/YYYY'), to_date('24/07/2018', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '+1 (685) 720-2955', 'lenny.walter@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (745, 'Wilderman', 'Johnson', 'Garry Stamm', 'M', to_date('11/01/1970', 'DD/MM/YYYY'), to_date('19/09/2014', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '3.902.478.989', 'jhoppe@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (746, 'Rosenbaum', 'Frami', 'Miss Augusta Larkin DDS', 'F', to_date('15/06/2008', 'DD/MM/YYYY'), to_date('19/03/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '12.419.191.714', 'gfay@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (747, 'Feest', 'Price', 'Ashleigh Terry', 'M', to_date('24/06/1982', 'DD/MM/YYYY'), to_date('06/01/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '(906) 950-3346', 'kristian16@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (748, 'Dooley', 'Ebert', 'Bryana Kshlerin PhD', 'F', to_date('09/04/1979', 'DD/MM/YYYY'), to_date('02/05/2015', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '8.842.584.586', 'bailey.moore@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (749, 'Harvey', 'Bode', 'Dr. Alek Marks V', 'M', to_date('26/09/1976', 'DD/MM/YYYY'), to_date('24/06/2014', 'DD/MM/YYYY'), 'Liberia', 'Limon', '293-323-4057 x3942', 'ashlynn.pfannerstill@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (750, 'Zemlak', 'Lesch', 'Callie Grady', 'F', to_date('16/07/2002', 'DD/MM/YYYY'), to_date('11/06/2016', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '1-765-450-4885 x68794', 'bogan.marjolaine@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (751, 'Zemlak', 'Jenkins', 'Kevin Torp I', 'M', to_date('20/10/1987', 'DD/MM/YYYY'), to_date('20/09/2011', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '+1 (354) 680-1265', 'rpaucek@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (752, 'Little', 'Altenwerth', 'Mr. Myles Spinka', 'M', to_date('03/10/1940', 'DD/MM/YYYY'), to_date('27/08/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '1-546-387-7238', 'qyundt@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (753, 'Champlin', 'Cassin', 'Eliane Cole', 'F', to_date('06/05/2009', 'DD/MM/YYYY'), to_date('01/09/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '-6557', 'sawayn.maverick@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (754, 'Prosacco', 'Waelchi', 'Ferne Luettgen', 'M', to_date('01/10/1963', 'DD/MM/YYYY'), to_date('15/10/2014', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '1-513-891-9452 x66093', 'ohara.enid@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (755, 'Doyle', 'Rodriguez', 'Mr. Shawn Gleason', 'F', to_date('17/03/2000', 'DD/MM/YYYY'), to_date('14/08/2012', 'DD/MM/YYYY'), 'Liberia', 'Limon', '+1 (358) 347-4494', 'istreich@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (756, 'O''Reilly', 'Stracke', 'Kristoffer Lueilwitz', 'M', to_date('01/12/1990', 'DD/MM/YYYY'), to_date('05/07/2013', 'DD/MM/YYYY'), 'San Jose', 'Limon', '436-439-3095 x89590', 'ihowell@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (757, 'Conroy', 'Yost', 'Jazmyn Heaney', 'F', to_date('08/02/1996', 'DD/MM/YYYY'), to_date('21/05/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '532-430-6049 x641', 'blick.vaughn@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (758, 'Labadie', 'Pagac', 'Christy Fritsch Jr.', 'M', to_date('06/05/2008', 'DD/MM/YYYY'), to_date('25/07/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '5.402.729.655', 'delilah75@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (759, 'Lang', 'Torp', 'Darrell Swift', 'M', to_date('10/12/1958', 'DD/MM/YYYY'), to_date('01/01/2017', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '(473) 451-8889', 'rice.carter@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (760, 'Lynch', 'Hoeger', 'Rosella Kulas', 'F', to_date('10/03/2004', 'DD/MM/YYYY'), to_date('29/10/2011', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '(582) 330-9527', 'van.boehm@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (761, 'Rohan', 'Harber', 'Dr. Buck Frami DVM', 'M', to_date('08/07/1954', 'DD/MM/YYYY'), to_date('28/11/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '1-871-316-9792', 'pondricka@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (762, 'Ullrich', 'Hansen', 'Shaniya Harris', 'M', to_date('18/03/1950', 'DD/MM/YYYY'), to_date('25/12/2010', 'DD/MM/YYYY'), 'Esparza', 'Limon', '930-698-4967 x12707', 'orrin87@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (763, 'Ratke', 'Langosh', 'Hollie Sauer', 'F', to_date('12/06/1951', 'DD/MM/YYYY'), to_date('29/02/2012', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '(587) 655-9009 x9564', 'bbarrows@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (764, 'Langosh', 'Buckridge', 'Johnpaul Considine', 'M', to_date('31/05/1982', 'DD/MM/YYYY'), to_date('10/11/2013', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '14.277.324.750', 'stoltenberg.tamara@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (765, 'O''Reilly', 'Boehm', 'June Jaskolski', 'F', to_date('29/01/1967', 'DD/MM/YYYY'), to_date('10/01/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '517-609-8889 x7082', 'awintheiser@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (766, 'Goyette', 'Reinger', 'Emmalee Sanford', 'M', to_date('18/02/1966', 'DD/MM/YYYY'), to_date('30/01/2010', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '603.392.4409 x33922', 'derek17@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (767, 'Ankunding', 'Heller', 'Freeman O''Kon', 'F', to_date('07/08/1979', 'DD/MM/YYYY'), to_date('18/01/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '1-531-999-9191', 'sonya.swaniawski@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (768, 'Kautzer', 'Harvey', 'Verna O''Connell', 'M', to_date('21/10/1974', 'DD/MM/YYYY'), to_date('28/01/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '12656077587', 'milan78@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (769, 'Kemmer', 'Bruen', 'Jaquelin Cruickshank MD', 'M', to_date('10/07/1984', 'DD/MM/YYYY'), to_date('29/01/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '272-702-4044 x20161', 'elna.fadel@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (770, 'Hane', 'Hand', 'Dena Runolfsson PhD', 'F', to_date('03/08/1980', 'DD/MM/YYYY'), to_date('10/04/2019', 'DD/MM/YYYY'), 'Esparza', 'Limon', '403.794.4813 x570', 'ghudson@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (771, 'Herman', 'Lebsack', 'Ms. Helga Franecki Jr.', 'M', to_date('25/12/1974', 'DD/MM/YYYY'), to_date('13/08/2018', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '270-767-0452 x098', 'niko.johnston@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (772, 'Jones', 'Rath', 'Mitchel Wolf', 'F', to_date('13/07/2006', 'DD/MM/YYYY'), to_date('29/04/2014', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '2.738.742.852', 'gklocko@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (773, 'Yost', 'Daugherty', 'Prof. Anibal Pfeffer III', 'M', to_date('26/12/1950', 'DD/MM/YYYY'), to_date('09/04/2013', 'DD/MM/YYYY'), 'Liberia', 'Limon', '5.299.711.298', 'rocio99@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (774, 'Green', 'Hoeger', 'Kenyon Waelchi III', 'F', to_date('03/07/2001', 'DD/MM/YYYY'), to_date('15/07/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '(953) 913-5083 x010', 'istanton@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (775, 'Lynch', 'Kunze', 'Joyce Marks', 'M', to_date('20/10/1967', 'DD/MM/YYYY'), to_date('08/11/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '5.755.015.439', 'xschinner@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (776, 'Fahey', 'Steuber', 'Alison Yost', 'M', to_date('11/09/1966', 'DD/MM/YYYY'), to_date('08/05/2014', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '1-428-236-3242', 'maria.hayes@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (777, 'Metz', 'Ward', 'Marcos Heaney', 'F', to_date('14/10/1999', 'DD/MM/YYYY'), to_date('17/11/2013', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '(798) 537-7657', 'hansen.lexus@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (778, 'Cummings', 'Turcotte', 'Prof. Aron Cremin', 'M', to_date('22/04/1955', 'DD/MM/YYYY'), to_date('06/12/2011', 'DD/MM/YYYY'), 'Alajuela', 'Puntarenas', '+1 (694) 294-5744', 'roberts.haylee@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (779, 'Marks', 'Yost', 'Dr. Randal Lubowitz DVM', 'F', to_date('01/06/1957', 'DD/MM/YYYY'), to_date('23/04/2011', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '332.872.7276 x5512', 'ben98@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (780, 'Lang', 'Lemke', 'Delaney Waelchi', 'M', to_date('27/05/1978', 'DD/MM/YYYY'), to_date('25/10/2016', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '(783) 675-4611 x044', 'dsenger@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (781, 'Smitham', 'Schamberger', 'Prof. Hailie D''Amore DVM', 'F', to_date('09/03/1987', 'DD/MM/YYYY'), to_date('02/09/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '504-662-8662', 'kuvalis.mikel@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (782, 'Cole', 'Macejkovic', 'Oda Ondricka', 'M', to_date('19/04/1955', 'DD/MM/YYYY'), to_date('24/05/2019', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '-7502', 'aidan.stark@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (783, 'Boehm', 'Wintheiser', 'Dr. Teresa Stokes IV', 'M', to_date('12/09/1973', 'DD/MM/YYYY'), to_date('05/06/2011', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '760-873-6152 x28069', 'hank.hessel@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (784, 'Hansen', 'Koch', 'Adriana Casper IV', 'F', to_date('05/05/1996', 'DD/MM/YYYY'), to_date('09/10/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '+1 (973) 362-9269', 'corine68@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (785, 'Carter', 'Crona', 'Moshe Hammes', 'M', to_date('15/10/1992', 'DD/MM/YYYY'), to_date('06/11/2017', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '(738) 609-5979 x35739', 'pblanda@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (786, 'Orn', 'Oberbrunner', 'Eliza Schinner III', 'M', to_date('08/11/1953', 'DD/MM/YYYY'), to_date('03/12/2012', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '4.908.078.610', 'hildegard36@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (787, 'Ullrich', 'Frami', 'Thelma Carroll Jr.', 'F', to_date('12/02/1959', 'DD/MM/YYYY'), to_date('03/04/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '-3608', 'lreinger@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (788, 'Orn', 'Gerlach', 'Miss Dessie Bartoletti', 'M', to_date('02/12/1974', 'DD/MM/YYYY'), to_date('06/03/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '-3489', 'sally64@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (789, 'Kuhic', 'Ferry', 'Miss Noemy Carroll MD', 'F', to_date('21/10/2003', 'DD/MM/YYYY'), to_date('18/02/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '1-490-219-7963 x573', 'charity.von@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (790, 'Schulist', 'Batz', 'Fredrick Kerluke', 'M', to_date('12/04/1983', 'DD/MM/YYYY'), to_date('30/10/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '624-671-6155', 'funk.dorothea@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (791, 'Littel', 'Gleason', 'Evert Wuckert', 'F', to_date('23/10/1970', 'DD/MM/YYYY'), to_date('16/01/2013', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '439-646-4918', 'klittle@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (792, 'Walter', 'Wehner', 'Melissa Kilback', 'M', to_date('07/11/1979', 'DD/MM/YYYY'), to_date('16/08/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '1-351-921-3661 x328', 'lind.rex@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (793, 'Koch', 'Kris', 'Billie Konopelski', 'M', to_date('20/05/1999', 'DD/MM/YYYY'), to_date('09/10/2017', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '3.372.637.210', 'torphy.rubye@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (794, 'Bogisich', 'Schaefer', 'Lonzo Schimmel', 'F', to_date('16/05/1973', 'DD/MM/YYYY'), to_date('01/08/2011', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '1-337-692-0126', 'laron.quitzon@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (795, 'Weimann', 'Bechtelar', 'Kasey Cartwright', 'M', to_date('08/06/1968', 'DD/MM/YYYY'), to_date('30/01/2015', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '986.909.9742 x129', 'tiara82@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (796, 'Zemlak', 'Gottlieb', 'Roberto Green I', 'F', to_date('19/05/1985', 'DD/MM/YYYY'), to_date('29/01/2015', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '1-709-234-4932', 'rreinger@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (797, 'Wehner', 'Buckridge', 'Mrs. Kaitlyn Kunde II', 'M', to_date('22/12/1963', 'DD/MM/YYYY'), to_date('13/09/2012', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '2.525.511.876', 'pschuster@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (798, 'Simonis', 'Von', 'Lonny Eichmann', 'F', to_date('01/12/1952', 'DD/MM/YYYY'), to_date('18/01/2010', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '16604530414', 'dach.okey@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (799, 'Kilback', 'Walter', 'Mr. Efrain McLaughlin PhD', 'M', to_date('15/09/1942', 'DD/MM/YYYY'), to_date('06/04/2017', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '1-891-409-4250', 'schroeder.kaci@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (800, 'Hansen', 'Simonis', 'Mr. Skylar Casper', 'M', to_date('09/11/1968', 'DD/MM/YYYY'), to_date('17/08/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '724-513-6851 x72498', 'okuneva.alford@example.com',
   5);

commit;

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (801, 'Morissette', 'Shields', 'Dr. Cloyd Prohaska', 'M', to_date('26/08/2007', 'DD/MM/YYYY'), to_date('23/12/2018', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '+1 (707) 575-2379', 'darion.stokes@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (802, 'Purdy', 'Daniel', 'Else Orn', 'F', to_date('23/11/1947', 'DD/MM/YYYY'), to_date('02/03/2015', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '951.651.5249 x4319', 'laverna.jones@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (803, 'Ward', 'Donnelly', 'Elwin Carter', 'M', to_date('13/02/1951', 'DD/MM/YYYY'), to_date('06/11/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '1-712-216-6283 x37329', 'alexis21@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (804, 'Bode', 'O''Connell', 'Dr. Rickie Jacobs', 'F', to_date('11/02/1975', 'DD/MM/YYYY'), to_date('22/01/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '15726672230', 'wilkinson.joanne@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (805, 'Torphy', 'Maggio', 'Geoffrey Hudson', 'M', to_date('26/01/1953', 'DD/MM/YYYY'), to_date('21/05/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '1-838-665-3727 x057', 'elnora08@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (806, 'D''Amore', 'Predovic', 'Bonnie Oberbrunner', 'F', to_date('09/01/1965', 'DD/MM/YYYY'), to_date('12/11/2014', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '(993) 728-1818 x534', 'verdie.effertz@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (807, 'Nitzsche', 'Kuvalis', 'Ms. Naomi Hill Sr.', 'M', to_date('19/05/2002', 'DD/MM/YYYY'), to_date('17/11/2010', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '-10674', 'meaghan.christiansen@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (808, 'VonRueden', 'Lockman', 'Norma West', 'M', to_date('17/05/1979', 'DD/MM/YYYY'), to_date('20/05/2014', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '282-640-4544', 'jammie.waters@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (809, 'Jenkins', 'Hickle', 'Dr. Bernard Stehr', 'F', to_date('19/09/1960', 'DD/MM/YYYY'), to_date('13/11/2013', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '879-438-0589 x397', 'bauch.courtney@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (810, 'DuBuque', 'Bergnaum', 'Prof. Bradford Dach', 'M', to_date('26/10/1959', 'DD/MM/YYYY'), to_date('27/03/2015', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '(819) 369-3463 x302', 'charlene.bode@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (811, 'Bosco', 'Mohr', 'Antonetta Bode', 'F', to_date('29/10/1958', 'DD/MM/YYYY'), to_date('05/09/2016', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '979.313.3546 x77031', 'jenkins.dudley@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (812, 'Senger', 'Davis', 'Emerson Farrell', 'M', to_date('08/07/1960', 'DD/MM/YYYY'), to_date('03/01/2017', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '+1 (893) 879-2585', 'creola.prosacco@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (813, 'Shanahan', 'Steuber', 'Kiara Goodwin', 'F', to_date('14/08/1944', 'DD/MM/YYYY'), to_date('05/06/2010', 'DD/MM/YYYY'), 'San Jose', 'Limon', '(412) 469-8192 x03785', 'klocko.israel@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (814, 'Schiller', 'Lang', 'Casimer Ortiz V', 'M', to_date('19/01/1956', 'DD/MM/YYYY'), to_date('06/12/2011', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '6.978.056.518', 'xolson@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (815, 'Crona', 'Durgan', 'Chasity Stiedemann', 'M', to_date('11/04/1952', 'DD/MM/YYYY'), to_date('09/08/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '1-639-919-0693', 'windler.henri@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (816, 'Lebsack', 'Weissnat', 'Mr. Rey Kunde', 'F', to_date('04/06/1963', 'DD/MM/YYYY'), to_date('31/01/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '1-274-971-8836 x791', 'bins.vanessa@example.org',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (817, 'Larkin', 'Conn', 'Prof. Ella Pfannerstill PhD', 'M', to_date('30/06/1973', 'DD/MM/YYYY'), to_date('09/11/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '1-740-622-1745 x047', 'fprohaska@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (818, 'Oberbrunner', 'Ward', 'Guillermo Kuphal', 'F', to_date('24/05/1987', 'DD/MM/YYYY'), to_date('15/12/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '-8516', 'clangworth@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (819, 'Hahn', 'Hessel', 'Dr. Stan Wisozk III', 'M', to_date('28/08/1943', 'DD/MM/YYYY'), to_date('21/12/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '19857829042', 'brenden72@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (820, 'Shanahan', 'Orn', 'Emilia Schuster', 'F', to_date('19/07/1982', 'DD/MM/YYYY'), to_date('29/03/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '308-980-6207', 'winnifred31@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (821, 'Harris', 'Schaefer', 'Michel Balistreri', 'M', to_date('12/09/1982', 'DD/MM/YYYY'), to_date('12/07/2017', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '807-770-2726', 'clarson@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (822, 'McGlynn', 'Larson', 'Elza Collier', 'M', to_date('08/01/1984', 'DD/MM/YYYY'), to_date('13/06/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '1-614-498-6457 x1664', 'greenfelder.lucile@example.com',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (823, 'Wolf', 'D''Amore', 'Mr. Sydney Fahey IV', 'F', to_date('17/01/1961', 'DD/MM/YYYY'), to_date('09/10/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '767-702-1975 x896', 'anthony.smith@example.com',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (824, 'Hill', 'Larson', 'Roy Glover', 'M', to_date('18/10/1947', 'DD/MM/YYYY'), to_date('06/09/2016', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '+1 (310) 310-8308', 'amalia31@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (825, 'Sauer', 'Carter', 'Camren Hane', 'M', to_date('30/12/1971', 'DD/MM/YYYY'), to_date('28/01/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '1-729-547-1651 x23733', 'mgraham@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (826, 'Schulist', 'Barrows', 'Kaley Rolfson', 'F', to_date('02/04/1973', 'DD/MM/YYYY'), to_date('03/02/2017', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '-7804', 'pfeffer.ross@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (827, 'Bechtelar', 'Barton', 'Marisol Morissette V', 'M', to_date('29/11/2000', 'DD/MM/YYYY'), to_date('28/01/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '-3151', 'gutmann.vivien@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (828, 'Larson', 'Rowe', 'Lawrence Krajcik V', 'F', to_date('30/03/1959', 'DD/MM/YYYY'), to_date('30/10/2019', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '512.283.8284 x647', 'cjacobs@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (829, 'Champlin', 'D''Amore', 'Mr. Sherman Torp Jr.', 'M', to_date('10/10/1989', 'DD/MM/YYYY'), to_date('11/01/2010', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '679-947-0897 x77829', 'vena.cartwright@example.com',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (830, 'Hand', 'Walker', 'Boris Langosh', 'M', to_date('12/10/1983', 'DD/MM/YYYY'), to_date('14/03/2017', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '8.424.600.974', 'jacinthe85@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (831, 'Metz', 'Cummings', 'Lizeth Hirthe', 'F', to_date('19/12/1950', 'DD/MM/YYYY'), to_date('04/03/2018', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '(853) 539-4356 x32817', 'fwiza@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (832, 'Pfannerstill', 'Legros', 'Molly Morar', 'M', to_date('15/01/1952', 'DD/MM/YYYY'), to_date('30/05/2016', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '631-433-7801 x806', 'hubert.wunsch@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (833, 'Willms', 'Thiel', 'Keenan Walsh', 'F', to_date('27/06/1967', 'DD/MM/YYYY'), to_date('04/09/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '1-480-624-2053 x492', 'reynolds.greg@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (834, 'Bernhard', 'Carroll', 'Rhoda Fahey Sr.', 'M', to_date('08/10/1996', 'DD/MM/YYYY'), to_date('27/12/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '+1 (451) 521-4522', 'ava60@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (835, 'Little', 'Carter', 'Mia Rohan DDS', 'F', to_date('17/03/1981', 'DD/MM/YYYY'), to_date('21/02/2014', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '503-643-3446 x4647', 'bbraun@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (836, 'Hirthe', 'Mosciski', 'Destiney Crooks', 'M', to_date('03/04/1964', 'DD/MM/YYYY'), to_date('06/02/2013', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '18.138.101.183', 'santa42@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (837, 'Mayer', 'Crona', 'Ayla Muller', 'M', to_date('24/09/1946', 'DD/MM/YYYY'), to_date('15/09/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '12.814.000.407', 'cbrakus@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (838, 'Jacobson', 'Gottlieb', 'Lolita Hills', 'F', to_date('19/05/1982', 'DD/MM/YYYY'), to_date('01/12/2009', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '941-366-8814', 'roberta.connelly@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (839, 'Schoen', 'Hills', 'Jerod Hermiston Sr.', 'M', to_date('27/02/1972', 'DD/MM/YYYY'), to_date('13/04/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '(413) 230-7668', 'zspencer@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (840, 'Jerde', 'Corwin', 'Jeffrey Hodkiewicz', 'F', to_date('19/02/1968', 'DD/MM/YYYY'), to_date('23/09/2018', 'DD/MM/YYYY'), 'Liberia', 'Limon', '+1 (875) 387-5215', 'ima90@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (841, 'Wolf', 'Kunde', 'Stephon Ratke', 'M', to_date('08/09/1990', 'DD/MM/YYYY'), to_date('18/08/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '286-465-0552 x67062', 'ernesto.kilback@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (842, 'Hilpert', 'Gaylord', 'Lue Kassulke', 'F', to_date('19/01/1961', 'DD/MM/YYYY'), to_date('31/10/2012', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '(289) 989-7884 x7950', 'donato.mante@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (843, 'Brekke', 'Daugherty', 'Emmitt Eichmann', 'M', to_date('15/07/1993', 'DD/MM/YYYY'), to_date('12/07/2019', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '8.604.295.800', 'zulauf.darlene@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (844, 'Hodkiewicz', 'Toy', 'Dr. Mohammed Reilly DVM', 'M', to_date('01/11/1983', 'DD/MM/YYYY'), to_date('08/12/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '214-734-1631', 'jordi.willms@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (845, 'Sawayn', 'Graham', 'Eduardo Mueller', 'F', to_date('25/08/1963', 'DD/MM/YYYY'), to_date('18/12/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '308.325.6211 x797', 'aurore24@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (846, 'Tremblay', 'Farrell', 'Vernie Lueilwitz', 'M', to_date('21/08/1953', 'DD/MM/YYYY'), to_date('26/10/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Puntarenas', '546.496.1916 x11454', 'destiney.murray@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (847, 'Abernathy', 'Boehm', 'Dr. Doris Hyatt DVM', 'F', to_date('08/02/1980', 'DD/MM/YYYY'), to_date('26/12/2015', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '18.755.820.435', 'iokuneva@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (848, 'Herman', 'Klein', 'Benny Cremin', 'M', to_date('25/06/2008', 'DD/MM/YYYY'), to_date('09/09/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '613-428-0810 x50487', 'devante.skiles@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (849, 'Spencer', 'Lowe', 'Dorcas Stiedemann MD', 'F', to_date('18/06/1994', 'DD/MM/YYYY'), to_date('06/04/2013', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '(331) 477-9200 x446', 'lesch.carleton@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (850, 'Torphy', 'Franecki', 'Amalia Padberg', 'M', to_date('14/11/1957', 'DD/MM/YYYY'), to_date('11/05/2012', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '1-340-538-6540', 'ray39@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (851, 'Runolfsdottir', 'Becker', 'Dr. Carolyn Hermann DVM', 'M', to_date('25/06/1990', 'DD/MM/YYYY'), to_date('13/08/2013', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '736-696-2422 x853', 'ybayer@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (852, 'Okuneva', 'Rolfson', 'Adrienne Stark', 'F', to_date('21/09/1991', 'DD/MM/YYYY'), to_date('06/06/2012', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '487-528-2082 x3816', 'mercedes28@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (853, 'Pacocha', 'Okuneva', 'Jerod Bayer V', 'M', to_date('07/04/1950', 'DD/MM/YYYY'), to_date('20/09/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '486.471.0565 x4251', 'althea.gerhold@example.net',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (854, 'Strosin', 'Armstrong', 'Prof. Elaina Upton II', 'M', to_date('21/01/1962', 'DD/MM/YYYY'), to_date('04/07/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '748-233-3773', 'dmarquardt@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (855, 'Windler', 'Willms', 'Ms. Mylene West', 'F', to_date('04/02/1967', 'DD/MM/YYYY'), to_date('18/12/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '(650) 361-8402 x17094', 'mwillms@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (856, 'Pollich', 'Dibbert', 'Geo Adams', 'M', to_date('02/10/1995', 'DD/MM/YYYY'), to_date('21/05/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '2.966.615.074', 'iupton@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (857, 'Schuppe', 'Sanford', 'Mr. Edgar Thiel V', 'F', to_date('17/08/2001', 'DD/MM/YYYY'), to_date('01/08/2015', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '568-865-7753 x14391', 'kaitlin96@example.org',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (858, 'Bode', 'Will', 'Dr. Rebeca Yundt', 'M', to_date('30/03/1973', 'DD/MM/YYYY'), to_date('30/01/2019', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '1-858-495-5398 x933', 'cathy.muller@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (859, 'Bauch', 'Kub', 'Stan Reichel', 'F', to_date('22/10/1986', 'DD/MM/YYYY'), to_date('01/06/2011', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '17.153.806.491', 'king.mandy@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (860, 'Wiza', 'Luettgen', 'Mozell Reilly V', 'M', to_date('02/12/2001', 'DD/MM/YYYY'), to_date('14/08/2010', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '1-692-326-6895 x6572', 'isom11@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (861, 'Walter', 'Runolfsson', 'Corine Mraz', 'M', to_date('11/11/1943', 'DD/MM/YYYY'), to_date('12/06/2012', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '(548) 874-8815 x70478', 'lehner.philip@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (862, 'Schinner', 'Koelpin', 'Mrs. Hulda Steuber Sr.', 'F', to_date('17/04/1943', 'DD/MM/YYYY'), to_date('08/11/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'San Jose', '14396721310', 'larson.darby@example.net',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (863, 'Renner', 'Russel', 'Alfonso Mitchell', 'M', to_date('19/02/1988', 'DD/MM/YYYY'), to_date('18/03/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '-1901', 'wolff.bruce@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (864, 'Lesch', 'Feest', 'Vallie Torp II', 'F', to_date('16/04/1969', 'DD/MM/YYYY'), to_date('03/01/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '629.440.4979 x84797', 'rullrich@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (865, 'Pfannerstill', 'Turcotte', 'Prof. Karine Ziemann', 'M', to_date('21/06/1995', 'DD/MM/YYYY'), to_date('10/10/2018', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '608.455.9302 x4345', 'yfeest@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (866, 'Schowalter', 'Pacocha', 'Coby Schmeler', 'F', to_date('08/12/1979', 'DD/MM/YYYY'), to_date('27/11/2010', 'DD/MM/YYYY'), 'San Jose', 'Limon', '-3992', 'asha.jerde@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (867, 'Torphy', 'Boyle', 'Dr. Immanuel Anderson', 'M', to_date('10/04/1976', 'DD/MM/YYYY'), to_date('05/03/2018', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '836-518-7563 x6498', 'constantin.boyer@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (868, 'Mayer', 'Aufderhar', 'Jordy Graham', 'M', to_date('31/12/2008', 'DD/MM/YYYY'), to_date('11/08/2010', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '-1185', 'maymie89@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (869, 'Stamm', 'Gerhold', 'Rosalind Bergnaum', 'F', to_date('09/02/1942', 'DD/MM/YYYY'), to_date('02/01/2012', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '14.536.750.699', 'adella.kuphal@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (870, 'Ondricka', 'Legros', 'Kyle Predovic', 'M', to_date('01/12/2003', 'DD/MM/YYYY'), to_date('30/10/2014', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '1-483-753-4810', 'mariam.kassulke@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (871, 'Medhurst', 'Runte', 'Ryann Cruickshank', 'F', to_date('24/07/1994', 'DD/MM/YYYY'), to_date('19/08/2015', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '1-917-708-6472 x3489', 'lynn.hintz@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (872, 'Pfeffer', 'Hoeger', 'Dr. Zakary Kirlin V', 'M', to_date('24/08/2000', 'DD/MM/YYYY'), to_date('23/04/2016', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '753-257-4466 x439', 'marta.jaskolski@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (873, 'Donnelly', 'Pacocha', 'Arvilla Rau', 'F', to_date('07/07/1955', 'DD/MM/YYYY'), to_date('12/09/2015', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '762-376-6540 x478', 'milo.nitzsche@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (874, 'Howe', 'Kub', 'Andre Dare', 'M', to_date('03/03/1995', 'DD/MM/YYYY'), to_date('11/08/2011', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '358-773-8404 x433', 'maximillia.lesch@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (875, 'Waters', 'Mann', 'Ferne Shanahan', 'M', to_date('26/08/1978', 'DD/MM/YYYY'), to_date('08/06/2017', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '971.504.5870 x3474', 'rhianna88@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (876, 'Koch', 'Hodkiewicz', 'Ms. Daija Heidenreich', 'F', to_date('02/02/1972', 'DD/MM/YYYY'), to_date('11/06/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Heredia', '4.266.640.235', 'morar.okey@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (877, 'McKenzie', 'Hahn', 'Rowland Herman III', 'M', to_date('24/07/1970', 'DD/MM/YYYY'), to_date('03/01/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '617-460-0441 x03974', 'davon04@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (878, 'Ziemann', 'Hessel', 'Mr. Sammie Kessler IV', 'M', to_date('10/02/1941', 'DD/MM/YYYY'), to_date('27/06/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '-2888', 'gino.langosh@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (879, 'Witting', 'Tromp', 'Prof. Erika Bogan', 'F', to_date('05/04/1967', 'DD/MM/YYYY'), to_date('25/03/2019', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '-9368', 'lorenz.jones@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (880, 'Pollich', 'Larkin', 'Ms. Maritza White', 'M', to_date('05/08/1994', 'DD/MM/YYYY'), to_date('12/09/2018', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '298-983-5987', 'nader.selena@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (881, 'Cronin', 'Kuvalis', 'Mozelle Jenkins', 'F', to_date('04/08/2008', 'DD/MM/YYYY'), to_date('07/11/2014', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '16246650604', 'leonor98@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (882, 'Runolfsson', 'Legros', 'Dr. Nichole Schowalter', 'M', to_date('14/07/1961', 'DD/MM/YYYY'), to_date('05/03/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '324-360-4885', 'lauren.yost@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (883, 'Kihn', 'Rutherford', 'Arturo Wolf II', 'F', to_date('24/11/1939', 'DD/MM/YYYY'), to_date('08/02/2011', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '1-451-587-8317 x48367', 'esmitham@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (884, 'Ledner', 'Breitenberg', 'Mrs. Alejandra Mertz', 'M', to_date('05/09/1969', 'DD/MM/YYYY'), to_date('18/11/2017', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '2.379.229.064', 'berta.upton@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (885, 'Conroy', 'Mayert', 'Hermina Bechtelar', 'M', to_date('17/01/1990', 'DD/MM/YYYY'), to_date('24/01/2014', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '341-492-3803', 'oconnell.orie@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (886, 'Miller', 'Grant', 'Edna Pagac', 'F', to_date('06/02/1950', 'DD/MM/YYYY'), to_date('19/05/2010', 'DD/MM/YYYY'), 'San Jose', 'Limon', '3.902.012.176', 'elizabeth.wisoky@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (887, 'Lind', 'Skiles', 'Dr. Lyric Olson', 'M', to_date('07/08/1940', 'DD/MM/YYYY'), to_date('27/11/2014', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '18.153.024.075', 'obie00@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (888, 'Stracke', 'Bauch', 'Dorothy Padberg', 'F', to_date('13/12/1945', 'DD/MM/YYYY'), to_date('03/04/2018', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '1-476-730-6141 x2442', 'crystel.jakubowski@example.com',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (889, 'Streich', 'Grimes', 'Karen McKenzie', 'M', to_date('24/09/1964', 'DD/MM/YYYY'), to_date('05/12/2010', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '12605087570', 'llueilwitz@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (890, 'Dickens', 'Koelpin', 'Amira Russel', 'F', to_date('30/01/1983', 'DD/MM/YYYY'), to_date('10/05/2013', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '774-566-0667 x7066', 'aubrey66@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (891, 'Murray', 'Erdman', 'Mrs. Aaliyah Osinski', 'M', to_date('26/06/1943', 'DD/MM/YYYY'), to_date('15/03/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '513-894-7731 x192', 'vivienne26@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (892, 'Hermann', 'Jones', 'Dr. Delphine Lehner', 'M', to_date('26/08/1948', 'DD/MM/YYYY'), to_date('01/10/2011', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '(785) 303-1560 x398', 'lisette.walter@example.org',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (893, 'Kuhlman', 'Kulas', 'Kamille Stoltenberg IV', 'M', to_date('21/08/1990', 'DD/MM/YYYY'), to_date('05/12/2014', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '(807) 756-6071', 'sammie.gerlach@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (894, 'Kshlerin', 'Krajcik', 'Alessandra Blick Sr.', 'F', to_date('24/01/1969', 'DD/MM/YYYY'), to_date('13/10/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '18976078015', 'koreilly@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (895, 'Grant', 'Murray', 'Mr. Hardy Reilly', 'M', to_date('26/09/1974', 'DD/MM/YYYY'), to_date('04/08/2012', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '-10192', 'eda67@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (896, 'Ernser', 'Hegmann', 'Bryce Larson II', 'F', to_date('18/05/2006', 'DD/MM/YYYY'), to_date('23/07/2016', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '1-995-784-1206 x6648', 'jpfeffer@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (897, 'Ullrich', 'Tromp', 'Jay Sporer', 'M', to_date('12/04/1978', 'DD/MM/YYYY'), to_date('24/01/2018', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '273-959-0254', 'pouros.gabrielle@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (898, 'Moore', 'Ondricka', 'Germaine Wolf', 'F', to_date('27/12/1951', 'DD/MM/YYYY'), to_date('21/06/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Limon', '272.729.5683 x6188', 'jenifer.mitchell@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (899, 'Beatty', 'Pagac', 'Colten Langworth III', 'M', to_date('06/10/1949', 'DD/MM/YYYY'), to_date('27/01/2012', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '636-525-3543', 'lonzo48@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (900, 'Wisoky', 'Johnston', 'Jeremy Goldner', 'M', to_date('31/12/1944', 'DD/MM/YYYY'), to_date('25/08/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '-7555', 'edgar61@example.org', 6);

commit;

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (901, 'Cruickshank', 'Bosco', 'Jerry Reinger', 'F', to_date('18/07/1948', 'DD/MM/YYYY'), to_date('19/06/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '729-927-5962', 'lthiel@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (902, 'Murazik', 'Barrows', 'Greta Stiedemann III', 'M', to_date('14/04/1957', 'DD/MM/YYYY'), to_date('08/02/2012', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '(867) 373-8087', 'zgoldner@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (903, 'Mraz', 'Connelly', 'Viviane Hahn', 'F', to_date('04/02/1971', 'DD/MM/YYYY'), to_date('12/02/2019', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '518-636-4663', 'oleffler@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (904, 'Volkman', 'Kertzmann', 'Karley Murray', 'M', to_date('25/04/1967', 'DD/MM/YYYY'), to_date('05/03/2019', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '1-205-675-1655', 'kozey.wellington@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (905, 'Lind', 'Skiles', 'Blake Smith', 'F', to_date('01/11/1964', 'DD/MM/YYYY'), to_date('13/08/2011', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '(717) 851-2069 x06747', 'kohler.savanna@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (906, 'Terry', 'Leuschke', 'Alexandra Watsica III', 'M', to_date('07/12/2005', 'DD/MM/YYYY'), to_date('27/10/2011', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Limon', '(614) 572-4633', 'ohyatt@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (907, 'Fahey', 'Senger', 'Miss Brionna Monahan', 'M', to_date('23/12/2002', 'DD/MM/YYYY'), to_date('10/06/2019', 'DD/MM/YYYY'), 'Liberia', 'Cartago', '529.616.2835 x350', 'lizzie77@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (908, 'Ratke', 'Volkman', 'Emil Bahringer PhD', 'F', to_date('07/10/1956', 'DD/MM/YYYY'), to_date('30/04/2018', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '-2654', 'bethany.feil@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (909, 'Turner', 'Hamill', 'Miss Macy Funk IV', 'M', to_date('22/01/1967', 'DD/MM/YYYY'), to_date('23/06/2013', 'DD/MM/YYYY'), 'Liberia', 'Puntarenas', '647-343-3409', 'darlene.fisher@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (910, 'Mayert', 'Brakus', 'Maiya Runolfsson DVM', 'F', to_date('04/03/1971', 'DD/MM/YYYY'), to_date('26/02/2010', 'DD/MM/YYYY'), 'Liberia', 'Limon', '503-889-7523 x438', 'marlene64@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (911, 'Bayer', 'Wolff', 'Russ Smitham', 'M', to_date('13/05/1978', 'DD/MM/YYYY'), to_date('05/09/2013', 'DD/MM/YYYY'), 'Alajuela', 'Alajuela', '414-746-2869', 'dan.williamson@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (912, 'Stiedemann', 'Rosenbaum', 'Brittany Kuphal', 'F', to_date('20/09/1993', 'DD/MM/YYYY'), to_date('10/07/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '825-618-4394 x5382', 'dsauer@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (913, 'Murazik', 'Wiza', 'Dayna Monahan', 'M', to_date('30/08/1976', 'DD/MM/YYYY'), to_date('12/08/2018', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '1-542-508-1713', 'kunde.elmira@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (914, 'Parker', 'Wiza', 'Palma Runte', 'M', to_date('24/06/1991', 'DD/MM/YYYY'), to_date('04/10/2014', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '541-684-0582', 'fwilderman@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (915, 'Gleichner', 'Murazik', 'Lauryn Leffler', 'F', to_date('02/01/1950', 'DD/MM/YYYY'), to_date('28/02/2014', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '882.684.3663 x72491', 'jgislason@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (916, 'Morar', 'Conn', 'Chanelle Huel', 'M', to_date('21/08/1957', 'DD/MM/YYYY'), to_date('26/10/2018', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '1-567-804-8348 x74676', 'shanny.carroll@example.com',
   1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (917, 'Stiedemann', 'Orn', 'Mr. Hayley Hamill', 'M', to_date('06/01/1993', 'DD/MM/YYYY'), to_date('07/08/2017', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '12835400866', 'senger.alford@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (918, 'Hickle', 'Heller', 'Presley Ferry', 'F', to_date('12/01/1944', 'DD/MM/YYYY'), to_date('26/07/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '-9459', 'graham.alexandro@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (919, 'Gutmann', 'Lakin', 'Trevion Dooley', 'M', to_date('11/12/1981', 'DD/MM/YYYY'), to_date('27/11/2018', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '(240) 415-6480 x0894', 'vada46@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (920, 'Schaefer', 'Nicolas', 'Mr. Reid Gaylord', 'F', to_date('23/04/1957', 'DD/MM/YYYY'), to_date('13/09/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '1-365-370-5254 x496', 'amayer@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (921, 'Konopelski', 'Baumbach', 'Sterling Schaden', 'M', to_date('23/02/1998', 'DD/MM/YYYY'), to_date('04/02/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '14.954.045.963', 'stanton.liliane@example.com',
   6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (922, 'Quigley', 'Bergnaum', 'Prof. Osborne Homenick II', 'M', to_date('18/07/1948', 'DD/MM/YYYY'), to_date('06/04/2019', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '-5591', 'tony33@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (923, 'Leannon', 'Kozey', 'Kennedy Hyatt', 'F', to_date('05/03/1955', 'DD/MM/YYYY'), to_date('13/03/2019', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '(750) 814-9912', 'weber.kobe@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (924, 'Reynolds', 'Senger', 'Nia Funk', 'M', to_date('16/12/1998', 'DD/MM/YYYY'), to_date('21/12/2011', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '19.953.093.851', 'cullen.schinner@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (925, 'Boyle', 'Batz', 'Natasha Champlin', 'F', to_date('07/01/1985', 'DD/MM/YYYY'), to_date('15/02/2012', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Guanacaste', '9.157.755.334', 'michel.brown@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (926, 'Greenholt', 'Keeling', 'Dr. Titus Runolfsdottir DDS', 'M', to_date('29/09/1995', 'DD/MM/YYYY'), to_date('29/04/2010', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '724.434.1317 x9980', 'xokeefe@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (927, 'Fritsch', 'Auer', 'Anais Buckridge', 'F', to_date('06/06/1965', 'DD/MM/YYYY'), to_date('28/09/2013', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '269-736-9537', 'earl.jacobi@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (928, 'Rempel', 'Runolfsdottir', 'Justyn Shanahan Jr.', 'M', to_date('20/06/1956', 'DD/MM/YYYY'), to_date('24/03/2019', 'DD/MM/YYYY'), 'Aguas Zarcas', 'San Jose', '(975) 369-8442', 'shaniya.hand@example.net',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (929, 'Schaden', 'Wehner', 'Kristofer Bergstrom', 'M', to_date('02/05/1954', 'DD/MM/YYYY'), to_date('10/12/2009', 'DD/MM/YYYY'), 'San Jose', 'Puntarenas', '-9686', 'ijenkins@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (930, 'Padberg', 'Skiles', 'Jesse Legros', 'F', to_date('29/12/1966', 'DD/MM/YYYY'), to_date('16/05/2012', 'DD/MM/YYYY'), 'Alajuela', 'Guanacaste', '1-881-275-0302', 'syble.little@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (931, 'Adams', 'Moen', 'Floy Weber', 'M', to_date('30/04/1959', 'DD/MM/YYYY'), to_date('28/01/2019', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '(463) 681-8193', 'cynthia21@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (932, 'Feil', 'Cartwright', 'Kassandra Davis', 'F', to_date('02/10/1977', 'DD/MM/YYYY'), to_date('07/01/2010', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '601.770.1084 x197', 'willms.deanna@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (933, 'Schimmel', 'Wolff', 'Greta Crist', 'M', to_date('05/03/1993', 'DD/MM/YYYY'), to_date('09/01/2010', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '(209) 949-1586', 'bednar.emmie@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (934, 'Boyer', 'O''Connell', 'Hailee Morar', 'F', to_date('15/12/1958', 'DD/MM/YYYY'), to_date('23/05/2010', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '+1 (250) 879-6811', 'evert07@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (935, 'Legros', 'Vandervort', 'Mr. Kristofer Dietrich', 'M', to_date('19/07/1940', 'DD/MM/YYYY'), to_date('17/12/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '(297) 710-1795 x51259', 'khegmann@example.org',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (936, 'Maggio', 'Ziemann', 'Claudia Shanahan', 'M', to_date('19/08/1997', 'DD/MM/YYYY'), to_date('28/10/2016', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '852-210-9401 x149', 'raymond.murphy@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (937, 'Considine', 'Schumm', 'Kamron Borer', 'F', to_date('15/05/1978', 'DD/MM/YYYY'), to_date('14/09/2016', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '-6328', 'cromaguera@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (938, 'Schroeder', 'Stroman', 'Gerardo Ortiz Jr.', 'M', to_date('18/06/1942', 'DD/MM/YYYY'), to_date('20/04/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '1-841-437-2570', 'rickey.gottlieb@example.net',
   4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (939, 'Boyer', 'Harris', 'Douglas Kreiger', 'F', to_date('03/03/1983', 'DD/MM/YYYY'), to_date('11/12/2015', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '264-331-4321 x5826', 'filiberto.brekke@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (940, 'Bins', 'Hermiston', 'Gaylord Sauer', 'M', to_date('06/04/1983', 'DD/MM/YYYY'), to_date('14/08/2012', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '(521) 278-2512 x70987', 'prohaska.dejon@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (941, 'Kris', 'Strosin', 'Woodrow Wintheiser', 'F', to_date('02/01/1948', 'DD/MM/YYYY'), to_date('16/06/2014', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '6.282.539.117', 'wilton35@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (942, 'Sipes', 'Steuber', 'Autumn Ruecker DDS', 'M', to_date('23/06/1988', 'DD/MM/YYYY'), to_date('09/04/2017', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Alajuela', '(436) 962-0251 x935', 'ewisozk@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (943, 'Littel', 'Heathcote', 'Carol Volkman', 'M', to_date('29/05/1992', 'DD/MM/YYYY'), to_date('06/04/2013', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '2.345.656.696', 'astrid88@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (944, 'Parker', 'Bartell', 'Georgianna Dach', 'F', to_date('05/07/1985', 'DD/MM/YYYY'), to_date('04/10/2012', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '1-872-377-4348 x049', 'dakota11@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (945, 'Bergnaum', 'Greenfelder', 'Hubert Murazik', 'M', to_date('25/04/1968', 'DD/MM/YYYY'), to_date('19/11/2017', 'DD/MM/YYYY'), 'Esparza', 'Cartago', '12352548142', 'kovacek.gust@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (946, 'Lowe', 'Luettgen', 'Dr. Leonardo Heaney', 'M', to_date('16/09/1962', 'DD/MM/YYYY'), to_date('19/01/2013', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '1-285-401-8270 x12505', 'yasmin45@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (947, 'Ebert', 'White', 'Sherwood Reinger Sr.', 'F', to_date('13/02/1947', 'DD/MM/YYYY'), to_date('19/04/2012', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '12384303422', 'qmarquardt@example.com', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (948, 'Crooks', 'Mueller', 'Kavon Upton', 'M', to_date('25/10/1946', 'DD/MM/YYYY'), to_date('03/04/2017', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '610.412.3391 x450', 'avery39@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (949, 'Grimes', 'McCullough', 'Lonie Kessler', 'F', to_date('18/06/1965', 'DD/MM/YYYY'), to_date('26/10/2015', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '-3195', 'wendell25@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (950, 'Quitzon', 'Boyer', 'Jessika Moen', 'M', to_date('08/02/1992', 'DD/MM/YYYY'), to_date('30/12/2012', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '1-510-929-0831', 'jakubowski.enos@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (951, 'Thompson', 'Becker', 'Mckenzie Rau', 'F', to_date('07/05/1978', 'DD/MM/YYYY'), to_date('22/05/2017', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '(627) 820-8191', 'ybreitenberg@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (952, 'Kohler', 'Larkin', 'Prof. Ruth Sporer', 'M', to_date('01/03/2002', 'DD/MM/YYYY'), to_date('03/07/2014', 'DD/MM/YYYY'), 'Esparza', 'Puntarenas', '1-924-621-0202 x1038', 'bonita40@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (953, 'Marvin', 'Hintz', 'Angel Denesik', 'M', to_date('12/06/2006', 'DD/MM/YYYY'), to_date('14/11/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Heredia', '18.745.949.157', 'schulist.alayna@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (954, 'Aufderhar', 'Nader', 'Ralph Beier', 'F', to_date('10/02/1970', 'DD/MM/YYYY'), to_date('07/12/2017', 'DD/MM/YYYY'), 'San Jose', 'Cartago', '-6014', 'ransom32@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (955, 'Corkery', 'Hand', 'Anna Hane', 'M', to_date('14/08/1965', 'DD/MM/YYYY'), to_date('19/12/2014', 'DD/MM/YYYY'), 'Esparza', 'Guanacaste', '1-436-464-4271 x7823', 'uziemann@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (956, 'Kerluke', 'Homenick', 'Mr. Jack Greenfelder PhD', 'F', to_date('15/08/1974', 'DD/MM/YYYY'), to_date('07/06/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '538-541-4705', 'rau.karolann@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (957, 'Hackett', 'VonRueden', 'Raymond Kirlin', 'M', to_date('24/04/1942', 'DD/MM/YYYY'), to_date('23/04/2014', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '(208) 315-1946 x020', 'kameron15@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (958, 'Bauch', 'Lubowitz', 'Mikel Heller PhD', 'F', to_date('04/05/1982', 'DD/MM/YYYY'), to_date('22/04/2010', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '927-940-5129', 'isidro.mante@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (959, 'Gibson', 'VonRueden', 'Jesse Swaniawski', 'M', to_date('10/01/1954', 'DD/MM/YYYY'), to_date('26/01/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '1-326-537-1426 x862', 'thurman52@example.org', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (960, 'Smitham', 'Rosenbaum', 'Mr. Gunner Dickinson DVM', 'M', to_date('20/08/1946', 'DD/MM/YYYY'), to_date('03/12/2016', 'DD/MM/YYYY'), 'Liberia', 'Heredia', '(623) 404-0143', 'nella24@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (961, 'Sauer', 'Erdman', 'Dr. Justyn Schulist', 'F', to_date('15/10/1963', 'DD/MM/YYYY'), to_date('26/03/2016', 'DD/MM/YYYY'), 'Esparza', 'Limon', '1-365-529-5327 x64269', 'selina76@example.com', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (962, 'Weissnat', 'Bahringer', 'Theresia Dare', 'M', to_date('23/12/1942', 'DD/MM/YYYY'), to_date('22/07/2016', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '890-757-5420', 'ggibson@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (963, 'Bogan', 'Sipes', 'Mr. Alfred Hackett', 'F', to_date('25/11/1969', 'DD/MM/YYYY'), to_date('27/07/2019', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '5.309.270.982', 'ellsworth.fay@example.net', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (964, 'Pollich', 'Yost', 'Antwan Wilkinson', 'M', to_date('24/05/1982', 'DD/MM/YYYY'), to_date('21/09/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '13477567345', 'qbernhard@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (965, 'Little', 'Kuhlman', 'Toni Crooks', 'F', to_date('16/04/1953', 'DD/MM/YYYY'), to_date('06/06/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '252-899-9482 x339', 'cleveland.kerluke@example.org', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (966, 'Borer', 'Breitenberg', 'Miss Mathilde Keeling', 'M', to_date('20/09/1988', 'DD/MM/YYYY'), to_date('06/03/2014', 'DD/MM/YYYY'), 'Liberia', 'Alajuela', '4.678.965.747', 'johnny.roberts@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (967, 'Jakubowski', 'Rau', 'Mariana Kuvalis', 'M', to_date('01/11/2004', 'DD/MM/YYYY'), to_date('07/07/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '1-432-596-0464', 'qhoeger@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (968, 'Leannon', 'Beatty', 'Miss Callie Brekke', 'F', to_date('25/08/1948', 'DD/MM/YYYY'), to_date('18/09/2010', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '-7622', 'kris38@example.com', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (969, 'Bosco', 'Crist', 'Cayla Considine', 'M', to_date('27/01/1946', 'DD/MM/YYYY'), to_date('13/06/2019', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '1-227-633-1200 x559', 'shagenes@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (970, 'Stiedemann', 'Hirthe', 'May Marks', 'M', to_date('25/08/1999', 'DD/MM/YYYY'), to_date('07/07/2010', 'DD/MM/YYYY'), 'Alajuela', 'San Jose', '247-998-6119 x03512', 'colson@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (971, 'Medhurst', 'Considine', 'Mr. Tyrell Purdy V', 'F', to_date('17/11/1993', 'DD/MM/YYYY'), to_date('14/05/2014', 'DD/MM/YYYY'), 'Liberia', 'San Jose', '475-676-9650', 'zrosenbaum@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (972, 'Schulist', 'Wehner', 'Alan Johns', 'M', to_date('08/12/1943', 'DD/MM/YYYY'), to_date('11/01/2019', 'DD/MM/YYYY'), 'Liberia', 'Limon', '535-797-5048', 'mankunding@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (973, 'Zieme', 'Weimann', 'Dr. Shad Bernier', 'F', to_date('07/03/2008', 'DD/MM/YYYY'), to_date('10/07/2017', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Cartago', '(621) 293-6391 x431', 'theo78@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (974, 'Hill', 'Stoltenberg', 'Rosemary Turner', 'M', to_date('04/02/1976', 'DD/MM/YYYY'), to_date('30/05/2019', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '359-847-0179', 'bethel87@example.net', 4);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (975, 'Waelchi', 'DuBuque', 'Prof. Emerald Raynor DVM', 'F', to_date('10/02/1997', 'DD/MM/YYYY'), to_date('29/04/2017', 'DD/MM/YYYY'), 'San Jose', 'San Jose', '630-914-4489 x269', 'candelario19@example.com',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (976, 'Beatty', 'Rolfson', 'Jamison Ebert', 'M', to_date('12/10/1990', 'DD/MM/YYYY'), to_date('30/12/2014', 'DD/MM/YYYY'), 'San Jose', 'Limon', '-2559', 'wolff.diana@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (977, 'Kreiger', 'DuBuque', 'Emmy Bayer', 'M', to_date('14/03/1944', 'DD/MM/YYYY'), to_date('29/04/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '498-220-0916 x96086', 'cruickshank.sophia@example.org',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (978, 'Powlowski', 'Kreiger', 'Prof. Jennings Nitzsche II', 'F', to_date('29/05/1993', 'DD/MM/YYYY'), to_date('17/07/2014', 'DD/MM/YYYY'), 'San Jose', 'Heredia', '-9923', 'istreich@example.org', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (979, 'Hermiston', 'Bednar', 'Prof. Diego Luettgen DDS', 'M', to_date('13/05/1940', 'DD/MM/YYYY'), to_date('08/10/2018', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Puntarenas', '(768) 485-4869', 'wgaylord@example.net',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (980, 'Dicki', 'Little', 'Destinee Denesik', 'F', to_date('28/05/1949', 'DD/MM/YYYY'), to_date('18/01/2010', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '(527) 620-6782', 'tre.von@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (981, 'Spinka', 'Schneider', 'Samantha King', 'M', to_date('26/07/1995', 'DD/MM/YYYY'), to_date('24/05/2011', 'DD/MM/YYYY'), 'Tamarindo', 'Limon', '518.302.1204 x238', 'leuschke.etha@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (982, 'Farrell', 'Will', 'Prof. Tracey Lubowitz', 'F', to_date('12/07/1952', 'DD/MM/YYYY'), to_date('06/04/2011', 'DD/MM/YYYY'), 'San Jose', 'Guanacaste', '305.924.6239 x432', 'rhickle@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (983, 'Weissnat', 'Romaguera', 'Cathrine Klein', 'M', to_date('13/07/1986', 'DD/MM/YYYY'), to_date('20/05/2016', 'DD/MM/YYYY'), 'Aguas Zarcas', 'Heredia', '5.416.246.017', 'weimann.dawn@example.com', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (984, 'Raynor', 'Bogisich', 'Astrid Ledner DVM', 'M', to_date('26/02/2004', 'DD/MM/YYYY'), to_date('26/05/2014', 'DD/MM/YYYY'), 'Esparza', 'Limon', '413-937-6287 x33591', 'mollie71@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (985, 'O''Keefe', 'Ruecker', 'Pasquale Ernser', 'M', to_date('02/04/1961', 'DD/MM/YYYY'), to_date('15/02/2019', 'DD/MM/YYYY'), 'Esparza', 'San Jose', '(886) 438-4567 x56159', 'matteo33@example.org', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (986, 'Roob', 'Lakin', 'Pearlie Kovacek', 'F', to_date('26/12/1993', 'DD/MM/YYYY'), to_date('14/12/2011', 'DD/MM/YYYY'), 'Tamarindo', 'San Jose', '528.776.2505 x978', 'mariano.hirthe@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (987, 'Beer', 'Kshlerin', 'Eldred Dickens', 'M', to_date('18/08/1975', 'DD/MM/YYYY'), to_date('26/12/2017', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Alajuela', '1-493-574-6042 x6109', 'darby.lubowitz@example.com',
   3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (988, 'Kertzmann', 'Weissnat', 'Maxie Wisoky II', 'F', to_date('08/05/1983', 'DD/MM/YYYY'), to_date('25/11/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Alajuela', '385-720-4595 x934', 'ubaldo04@example.net', 5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (989, 'Bernhard', 'Schuster', 'William Willms', 'M', to_date('19/12/1987', 'DD/MM/YYYY'), to_date('04/02/2015', 'DD/MM/YYYY'), 'Alajuela', 'Cartago', '(304) 357-6085 x6928', 'pablo47@example.org', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (990, 'Oberbrunner', 'Rempel', 'Haskell Lowe', 'F', to_date('16/04/1993', 'DD/MM/YYYY'), to_date('16/08/2011', 'DD/MM/YYYY'), 'Alajuela', 'Heredia', '5.035.780.340', 'ilene.trantow@example.net', 1);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (991, 'Franecki', 'Aufderhar', 'Mrs. Theresia Donnelly DVM', 'M', to_date('22/03/1992', 'DD/MM/YYYY'), to_date('22/04/2016', 'DD/MM/YYYY'), 'Esparza', 'Alajuela', '(306) 267-9471 x69734', 'mae64@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (992, 'Emmerich', 'Satterfield', 'Prof. Angeline Schamberger', 'M', to_date('26/09/1996', 'DD/MM/YYYY'), to_date('03/05/2013', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Cartago', '563-363-9663 x58322', 'reta94@example.com',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (993, 'Paucek', 'Grady', 'Kimberly Kessler', 'F', to_date('27/01/2000', 'DD/MM/YYYY'), to_date('23/10/2017', 'DD/MM/YYYY'), 'San Jose', 'Alajuela', '873.638.4347 x21796', 'yrunte@example.net', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (994, 'Rath', 'Hessel', 'Mr. Eleazar Mohr PhD', 'M', to_date('08/08/1973', 'DD/MM/YYYY'), to_date('13/08/2012', 'DD/MM/YYYY'), 'Ciudad Quesada', 'Guanacaste', '(450) 284-8887', 'amayert@example.org', 3);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (995, 'Nolan', 'O''Connell', 'Dr. Larue Okuneva MD', 'F', to_date('15/02/1969', 'DD/MM/YYYY'), to_date('02/06/2014', 'DD/MM/YYYY'), 'Esparza', 'Heredia', '1-959-868-6742 x053', 'oerdman@example.com', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (996, 'VonRueden', 'Wyman', 'Prof. Eva Champlin II', 'M', to_date('16/01/1959', 'DD/MM/YYYY'), to_date('13/02/2013', 'DD/MM/YYYY'), 'Liberia', 'Guanacaste', '19.369.675.469', 'hermiston.olin@example.net',
   2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (997, 'McClure', 'Welch', 'Dolly O''Keefe', 'F', to_date('15/06/1959', 'DD/MM/YYYY'), to_date('29/05/2019', 'DD/MM/YYYY'), 'Tamarindo', 'Guanacaste', '+1 (375) 617-2393', 'sylvester53@example.com', 2);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (998, 'Howell', 'Brakus', 'Prof. Rick Schuppe', 'M', to_date('25/09/1955', 'DD/MM/YYYY'), to_date('01/06/2017', 'DD/MM/YYYY'), 'Tamarindo', 'Puntarenas', '(346) 347-1452 x26831', 'wendell67@example.com',
   5);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (999, 'Effertz', 'Kuhlman', 'Prof. Kaylin Rosenbaum', 'M', to_date('28/09/1954', 'DD/MM/YYYY'), to_date('03/06/2010', 'DD/MM/YYYY'), 'Alajuela', 'Limon', '4.143.854.690', 'angelita43@example.net', 6);

insert into eureka.cliente (id, apellido1, apellido2, nombre, genero, fecha_nacimiento, fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
values (1000, 'Howe', 'Hegmann', 'Dr. Matilda Corkery II', 'F', to_date('04/05/1987', 'DD/MM/YYYY'), to_date('15/12/2010', 'DD/MM/YYYY'), 'Tamarindo', 'Cartago', '(487) 324-2913', 'deckow.clara@example.org', 5);

commit;

