/*
Empresa        :  EurekaBank
Software       :  Sistema de Cuentas de Ahorro
DBMS           :  Orecle
Base de Datos  :  eurekabank
Script         :  Carga Datos
Responsable    :  Eric Gustavo Coronel Castillo
Email          :  gcoronelc@gmail.com
Sitio Web      :  www.desarrollasoftware.com
Blog           :  http://gcoronelc.blogspot.com
*/
CONNECT eureka/123456; 

-- =============================================
-- CARGAR DATOS DE PRUEBA
-- =============================================

-- Tabla: Moneda
insert into moneda values ( 1 , 'Colones', 'C',1 , 'A' );
insert into moneda values ( 2 , 'Dolares', '$',588.55, 'A' );

-- Tabla: TipoMovimiento
insert into Tipo_Movimiento values( 1 , 'Apertura de Cuenta', 'INGRESO', 'A' );
insert into Tipo_Movimiento values( 2 , 'Cancelar Cuenta', 'SALIDA', 'A' );
insert into Tipo_Movimiento values( 3 , 'Deposito', 'INGRESO', 'A' );
insert into Tipo_Movimiento values( 4 , 'Retiro', 'SALIDA', 'A' );
insert into Tipo_Movimiento values( 5 , 'Interes', 'INGRESO', 'A' );
insert into Tipo_Movimiento values( 6 , 'Mantenimiento', 'SALIDA', 'A' );
insert into Tipo_Movimiento values( 7 , 'ITF', 'SALIDA', 'A' );
insert into Tipo_Movimiento values( 8 , 'Transferencia', 'INGRESO', 'A' );
insert into Tipo_Movimiento values( 9 , 'Transferencia', 'SALIDA', 'A' );
insert into Tipo_Movimiento values( 10 , 'Cargo por Movimiento', 'SALIDA', 'A' );

-- Tabla: Sucursal

insert into sucursal values( 1 , 'Ciudad Quesada', 'Alajuela', 1 );
insert into sucursal values( 2 , 'Liberia', 'Guanacaste', 1 );
insert into sucursal values( 3 , 'Tamarindo', 'Guanacaste', 1 );
insert into sucursal values( 4 , 'San José', 'San José', 1 );
insert into sucursal values( 5 , 'Alajuela', 'Alajuela', 1 );
insert into sucursal values( 6 , 'Esparza', 'Puntarenas', 1 );


-- Tabla: Empleado

INSERT INTO empleado VALUES( 1 , 'Internet', 'Internet', 'internet', 'Internet', 'internet', 'internet', '1', 1 );
INSERT INTO empleado VALUES( 2 , 'Romero', 'Castillo', 'Carlos Alberto', 'Trujillo', 'Call1 1 Nro. 456', 'cromero', '1',2 );
INSERT INTO empleado VALUES( 3 , 'Castro', 'Vargas', 'Lidia', 'Lima', 'Federico Villarreal 456 - SMP', 'lcastro', '1',3 );
INSERT INTO empleado VALUES( 4 , 'Reyes', 'Ortiz', 'Claudia', 'Lima', 'Av. Aviación 3456 - San Borja', 'creyes', '1',4 );
INSERT INTO empleado VALUES( 5 , 'Ramos', 'Garibay', 'Angelica', 'Chiclayo', 'Calle Barcelona 345', 'aramos', '1',5 );
INSERT INTO empleado VALUES( 6 , 'Ruiz', 'Zabaleta', 'Claudia', 'Cusco', 'Calle Cruz Verde 364', 'cvalencia', '1',6 );
INSERT INTO empleado VALUES( 7 , 'Cruz', 'Tarazona', 'Ricardo', 'Areguipa', 'Calle La Gruta 304', 'rcruz', '1',1 );
INSERT INTO empleado VALUES( 8 , 'Torres', 'Diaz', 'Guino', 'Lima', 'Av. Salaverry 1416', 'gtorres', '1',2 );	

-- Tabla: Cliente

insert into cliente values( 1 , 'CORONEL', 'CASTILLO', 'ERIC GUSTAVO', 'M', 'LIMA', 'LOS OLIVOS', '9666-4457', 'gcoronel@viabcp.com',1 );
insert into cliente values( 2 , 'VALENCIA', 'MORALES', 'PEDRO HUGO', 'M', 'LIMA', 'MAGDALENA', '924-7834', 'pvalencia@terra.com.pe',2 );
insert into cliente values( 3 , 'MARCELO', 'VILLALOBOS', 'RICARDO', 'M', 'LIMA', 'LINCE', '993-62966', 'ricardomarcelo@hotmail.com',3 );
insert into cliente values( 4 , 'ROMERO', 'CASTILLO', 'CARLOS ALBERTO', 'M', 'LIMA', 'LOS OLIVOS', '865-84762', 'c.romero@hotmail.com',4 );
insert into cliente values( 6 , 'ARANDA', 'LUNA', 'ALAN ALBERTO', 'M', 'LIMA', 'SAN ISIDRO', '834-67125', 'a.aranda@hotmail.com',5 );
insert into cliente values( 7 , 'AYALA', 'PAZ', 'JORGE LUIS', 'M', 'LIMA', 'SAN BORJA', '963-34769', 'j.ayala@yahoo.com',6 );
insert into cliente values( 8 , 'CHAVEZ', 'CANALES', 'EDGAR RAFAEL', 'M', 'LIMA', 'MIRAFLORES', '999-96673', 'e.chavez@gmail.com',1 );
insert into cliente values( 9 , 'FLORES', 'CHAFLOQUE', 'ROSA LIZET', 'F', 'LIMA', 'LA MOLINA', '966-87567', 'r.florez@hotmail.com',2 );
insert into cliente values( 10 , 'FLORES', 'SHUTE', 'CRISTIAN RAFAEL', 'M', 'LIMA', 'LOS OLIVOS', '978-43768', 'c.flores@hotmail.com',3 );
insert into cliente values( 11 , 'GONZALES', 'GARCIA', 'GABRIEL ALEJANDRO', 'M', 'LIMA', 'SAN MIGUEL', '945-56782', 'g.gonzales@yahoo.es',4 );
insert into cliente values( 12 , 'LAY', 'VALLEJOS', 'JUAN CARLOS', 'M', 'LIMA', 'LINCE', '956-12657', 'j.lay@peru.com',5 );
insert into cliente values( 13 , 'MONTALVO', 'SOTO', 'DEYSI LIDIA', 'M', 'LIMA', 'SURCO', '965-67235', 'd.montalvo@hotmail.com',6 );
insert into cliente values( 14 , 'RICALDE', 'RAMIREZ', 'ROSARIO ESMERALDA', 'M', 'LIMA', 'MIRAFLORES', '991-23546', 'r.ricalde@gmail.com',1 );
insert into cliente values( 15 , 'RODRIGUEZ', 'RAMOS', 'ENRIQUE MANUEL', 'M', 'LIMA', 'LINCE', '976-82838', 'e.rodriguez@gmail.com',2 );
insert into cliente values( 16 , 'ROJAS', 'OSCANOA', 'FELIX NINO', 'M', 'LIMA', 'LIMA', '962-32158', 'f.rojas@yahoo.com',3 );
insert into cliente values( 17 , 'TEJADA', 'DEL AGUILA', 'TANIA LORENA', 'F', 'LIMA', 'PUEBLO LIBRE', '966-23854', 't.tejada@hotmail.com',4 );
insert into cliente values( 18 , 'VALDEVIESO', 'LEYVA', 'ROXANA', 'F', 'LIMA', 'SURCO', '956-78951', 'r.valdivieso@terra.com.pe',5 );
insert into cliente values( 19 , 'VALENTIN', 'COTRINA', 'JUAN DIEGO', 'M', 'LIMA', 'LA MOLINA', '921-12456', 'j.valentin@terra.com.pe',6 );
insert into cliente values( 20 , 'YAURICASA', 'BAUTISTA', 'YESABETH', 'F', 'LIMA', 'MAGDALENA', '977-75777', 'y.yauricasa@terra.com.pe',1 );
insert into cliente values( 21 , 'ZEGARRA', 'GARCIA', 'FERNANDO MOISES', 'F', 'LIMA', 'SAN ISIDRO', '936-45876', 'f.zegarra@hotmail.com',2 );

-- Tabla: Cuenta

insert into cuenta values( 1 ,'01','001','0004','00005',6900,to_date('20080106 16:27:48','YYYYMMDD HH24:MI:SS'),'ACTIVO',8,'123456');
insert into cuenta values( 2 ,'02','001','0004','00005',4500,to_date('20080108 14:21:12','YYYYMMDD HH24:MI:SS'),'ACTIVO',5,'123456');
insert into cuenta values( 3 ,'01','002','0001','00008',7000,to_date('20080105 13:15:30','YYYYMMDD HH24:MI:SS'),'ACTIVO',16,'123456');
insert into cuenta values( 4 ,'01','002','0001','00001',6800,to_date('20080109 10:30:25','YYYYMMDD HH24:MI:SS'),'ACTIVO',4,'123456');
insert into cuenta values( 5 ,'02','002','0001','00007',6000,to_date('20080111 15:45:12','YYYYMMDD HH24:MI:SS'),'ACTIVO',7,'123456');
insert into cuenta values( 6 ,'01','003','0002','00010',0000,to_date('20080107 12:45:12','YYYYMMDD HH24:MI:SS'),'CANCELADO',3,'123456');


-- Tabla: Movimiento

insert into movimiento values( 1 ,01,to_date('20080106 16:27:48','YYYYMMDD HH24:MI:SS'),'0004','001',2800,null);
insert into movimiento values( 2 ,02,to_date('20080115 13:47:31','YYYYMMDD HH24:MI:SS'),'0004','003',3200,null);
insert into movimiento values( 3 ,03,to_date('20080120 17:11:15','YYYYMMDD HH24:MI:SS'),'0004','004',0800,null);
insert into movimiento values( 4 ,04,to_date('20080214 12:12:12','YYYYMMDD HH24:MI:SS'),'0004','003',2000,null);
insert into movimiento values( 5 ,05,to_date('20080225 15:45:23','YYYYMMDD HH24:MI:SS'),'0004','004',0500,null);
insert into movimiento values( 6 ,06,to_date('20080303 11:17:19','YYYYMMDD HH24:MI:SS'),'0004','004',0800,null);
insert into movimiento values( 7 ,07,to_date('20080315 13:26:39','YYYYMMDD HH24:MI:SS'),'0004','003',1000,null);

insert into movimiento values( 1 ,01,to_date('20080108 14:21:12','YYYYMMDD HH24:MI:SS'),'0004','001',1800,null);
insert into movimiento values( 2 ,02,to_date('20080125 15:15:15','YYYYMMDD HH24:MI:SS'),'0004','004',1000,null);
insert into movimiento values( 3 ,03,to_date('20080213 11:12:56','YYYYMMDD HH24:MI:SS'),'0004','003',2200,null);
insert into movimiento values( 4 ,04,to_date('20080308 10:21:12','YYYYMMDD HH24:MI:SS'),'0004','003',1500,null);

insert into movimiento values( 1 ,01,to_date('20080105 13:15:30','YYYYMMDD HH24:MI:SS'),'0001','001',5000,null);
insert into movimiento values( 2 ,02,to_date('20080107 12:14:18','YYYYMMDD HH24:MI:SS'),'0001','003',4000,null);
insert into movimiento values( 3 ,03,to_date('20080109 09:34:12','YYYYMMDD HH24:MI:SS'),'0001','004',2000,null);
insert into movimiento values( 4 ,04,to_date('20080111 11:11:11','YYYYMMDD HH24:MI:SS'),'0001','003',1000,null);
insert into movimiento values( 5 ,05,to_date('20080113 16:16:16','YYYYMMDD HH24:MI:SS'),'0001','003',2000,null);
insert into movimiento values( 6 ,06,to_date('20080115 14:15:16','YYYYMMDD HH24:MI:SS'),'0001','004',4000,null);
insert into movimiento values( 7 ,07,to_date('20080119 18:34:12','YYYYMMDD HH24:MI:SS'),'0001','003',2000,null);
insert into movimiento values( 8 ,08,to_date('20080121 16:34:18','YYYYMMDD HH24:MI:SS'),'0001','004',3000,null);
insert into movimiento values( 9 ,09,to_date('20080123 10:23:56','YYYYMMDD HH24:MI:SS'),'0001','003',7000,null);
insert into movimiento values( 1 0,10,to_date('20080127 09:12:56','YYYYMMDD HH24:MI:SS'),'0001','004',1000,null);
insert into movimiento values( 1 1,11,to_date('20080130 16:34:50','YYYYMMDD HH24:MI:SS'),'0001','004',3000,null);
insert into movimiento values( 1 2,12,to_date('20080204 16:12:21','YYYYMMDD HH24:MI:SS'),'0001','003',2000,null);
insert into movimiento values( 1 3,13,to_date('20080208 17:21:23','YYYYMMDD HH24:MI:SS'),'0001','004',4000,null);
insert into movimiento values( 1 4,14,to_date('20080213 14:17:45','YYYYMMDD HH24:MI:SS'),'0001','003',2000,null);
insert into movimiento values( 1 5,15,to_date('20080219 17:32:23','YYYYMMDD HH24:MI:SS'),'0001','004',1000,null);

insert into movimiento values(1,01,to_date('20080109 10:30:25','YYYYMMDD HH24:MI:SS'),'0001','001',3800,null);
insert into movimiento values(2,02,to_date('20080120 14:56:23','YYYYMMDD HH24:MI:SS'),'0001','003',4200,null);
insert into movimiento values(3,03,to_date('20080306 13:58:58','YYYYMMDD HH24:MI:SS'),'0001','004',1200,null);

insert into movimiento values(1,01,to_date('20080111 15:45:12','YYYYMMDD HH24:MI:SS'),'0001','001',2500,null);
insert into movimiento values(2,02,to_date('20080117 14:17:12','YYYYMMDD HH24:MI:SS'),'0001','003',1500,null);
insert into movimiento values(3,03,to_date('20080120 14:12:12','YYYYMMDD HH24:MI:SS'),'0001','004',0500,null);
insert into movimiento values(4,04,to_date('20080209 15:45:34','YYYYMMDD HH24:MI:SS'),'0001','004',0500,null);
insert into movimiento values(5,05,to_date('20080225 11:18:20','YYYYMMDD HH24:MI:SS'),'0001','003',3500,null);
insert into movimiento values(6,06,to_date('20080311 10:56:23','YYYYMMDD HH24:MI:SS'),'0001','004',0500,null);

insert into movimiento values(1,01,to_date('20080107 12:45:12','YYYYMMDD HH24:MI:SS'),'0002','001',5600,null);
insert into movimiento values(2,02,to_date('20080118 11:17:12','YYYYMMDD HH24:MI:SS'),'0002','003',1400,null);
insert into movimiento values(3,03,to_date('20080125 15:12:12','YYYYMMDD HH24:MI:SS'),'0002','002',7000,null);

commit;