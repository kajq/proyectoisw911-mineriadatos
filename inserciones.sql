/*
Empresa        :  EurekaBank
Software       :  Sistema de Cuentas de Ahorro
DBMS           :  Orecle
Base de Datos  :  eurekabank
Script         :  Carga Datos
Responsable    :  Eric Gustavo Coronel Castillo
Email          :  gcoronelc@gmail.com
Sitio Web      :  www.desarrollasoftware.com
Blog           :  http://gcoronelc.blogspot.com
*/
CONNECT eureka/123456; 

-- =============================================
-- CARGAR DATOS DE PRUEBA
-- =============================================

-- Tabla: Moneda
insert into moneda values ( 1 , 'Colones', 'C',1 , 'A' );
insert into moneda values ( 2 , 'Dolares', '$',588.55, 'A' );

-- Tabla: TipoMovimiento 
insert into Tipo_Movimiento values( 1 , 'Apertura de Cuenta', 'INGRESO', 'A' );
insert into Tipo_Movimiento values( 2 , 'Cancelar Cuenta', 'SALIDA', 'A' );
insert into Tipo_Movimiento values( 3 , 'Deposito', 'INGRESO', 'A' );
insert into Tipo_Movimiento values( 4 , 'Retiro', 'SALIDA', 'A' );
insert into Tipo_Movimiento values( 5 , 'Interes', 'INGRESO', 'A' );
insert into Tipo_Movimiento values( 6 , 'Mantenimiento', 'SALIDA', 'A' );
insert into Tipo_Movimiento values( 7 , 'ITF', 'SALIDA', 'A' );
insert into Tipo_Movimiento values( 8 , 'Transferencia', 'INGRESO', 'A' );
insert into Tipo_Movimiento values( 9 , 'Transferencia', 'SALIDA', 'A' );
insert into Tipo_Movimiento values( 10 , 'Cargo por Movimiento', 'SALIDA', 'A' );

-- Tabla: Sucursal

insert into sucursal values( 1 , 'Ciudad Quesada', 'Alajuela', 1 );
insert into sucursal values( 2 , 'Liberia', 'Guanacaste', 1 );
insert into sucursal values( 3 , 'Tamarindo', 'Guanacaste', 1 );
insert into sucursal values( 4 , 'San José', 'San José', 1 );
insert into sucursal values( 5 , 'Alajuela', 'Alajuela', 1 );
insert into sucursal values( 6 , 'Esparza', 'Puntarenas', 1 );


-- Tabla: Empleado

INSERT INTO empleado VALUES( 1 , 'Internet', 'Internet', 'internet', 'Internet', 'internet', 'internet', '1', 1 );
INSERT INTO empleado VALUES( 2 , 'Romero', 'Castillo', 'Carlos Alberto', 'Trujillo', 'Call1 1 Nro. 456', 'cromero', '1',2 );
INSERT INTO empleado VALUES( 3 , 'Castro', 'Vargas', 'Lidia', 'Lima', 'Federico Villarreal 456 - SMP', 'lcastro', '1',3 );
INSERT INTO empleado VALUES( 4 , 'Reyes', 'Ortiz', 'Claudia', 'Lima', 'Av. Aviación 3456 - San Borja', 'creyes', '1',4 );
INSERT INTO empleado VALUES( 5 , 'Ramos', 'Garibay', 'Angelica', 'Chiclayo', 'Calle Barcelona 345', 'aramos', '1',5 );
INSERT INTO empleado VALUES( 6 , 'Ruiz', 'Zabaleta', 'Claudia', 'Cusco', 'Calle Cruz Verde 364', 'cvalencia', '1',6 );
INSERT INTO empleado VALUES( 7 , 'Cruz', 'Tarazona', 'Ricardo', 'Areguipa', 'Calle La Gruta 304', 'rcruz', '1',1 );
INSERT INTO empleado VALUES( 8 , 'Torres', 'Diaz', 'Guino', 'Lima', 'Av. Salaverry 1416', 'gtorres', '1',2 );

--Tabla: alerta fraudes

INSERT INTO tipo_alerta VALUES( 1 , 'Movimiento de dudosa procedecia', 1);
INSERT INTO tipo_alerta VALUES( 2 , 'Conexion desde dispositivo nuevo', 1);
INSERT INTO tipo_alerta VALUES( 3 , 'Coneccion desde otro pais', 1);
INSERT INTO tipo_alerta VALUES( 4 , 'Demasiados intentos de inicio de sesion', 1);
INSERT INTO tipo_alerta VALUES( 5 , 'Intento de cambio de clave', 1);
INSERT INTO tipo_alerta VALUES( 6 , 'Fecha y hora de movimientos realizados', 1);


--Tabla: tipo_movsucursal

INSERT INTO tipo_movsucursal VALUES( 1 , 'Interses generados por créditos','ingresos', 1);
INSERT INTO tipo_movsucursal VALUES( 2 , 'Comision por trasacciones entre entidades','ingresos', 1);
INSERT INTO tipo_movsucursal VALUES( 3 , 'Rediientos he intrumentos del capital','ingresos', 1);
INSERT INTO tipo_movsucursal VALUES( 4 , 'Ganancias del tipo de cambio','ingresos', 1);
INSERT INTO tipo_movsucursal VALUES( 5 , 'Interes ganados por tarjetas de credito','ingresos', 1);
INSERT INTO tipo_movsucursal VALUES( 7 , 'Pagos de planilla al personal','gastos', 1);
INSERT INTO tipo_movsucursal VALUES( 8 , 'Pagos de ahorros a plazo de clientes','gastos', 1);
INSERT INTO tipo_movsucursal VALUES( 9 , 'Gastos operativos de las oficinas','gastos', 1);
INSERT INTO tipo_movsucursal VALUES( 6 , 'Perdidas porducidas por el tipo de cambio','gastos', 1);
INSERT INTO tipo_movsucursal VALUES( 10 , 'Gastos de Interes en cuentas de ahorro corrientes','gastos', 1);


--Tabla:productos 

INSERT INTO productos VALUES( 1 , 'Ahorro a plazo a 1 mes', 1.8 , 'G');
INSERT INTO productos VALUES( 2 , 'Ahorro a plazo a 6 mes', 8.0 , 'G');
INSERT INTO productos VALUES( 3 , 'Ahorro a plazo a un 1 anno', 15.0 , 'G');
INSERT INTO productos VALUES( 4 , 'Hipoteca compra de casa', 12.0 , 'I');
INSERT INTO productos VALUES( 5 , 'Hipoteca compra de lote', 11.0 , 'I');
INSERT INTO productos VALUES( 6 , 'Hipoteca construccion en lote propio', 10.0 , 'I');
INSERT INTO productos VALUES( 7 , 'Hipoteca en gastos multipropositos', 15.0 , 'I');
INSERT INTO productos VALUES( 8 , 'Cuenta de ahorros', 1.2 , 'G');
INSERT INTO productos VALUES( 9 , 'Tarjetas de credito', 40.0 , 'I');
INSERT INTO productos VALUES( 10 , 'Prestamo personal', 20.0 , 'I');

--Tabla: tipo_visita

INSERT INTO tipo_visita VALUES( 10 , 'Aplicacion', 20.0 , 'A');
INSERT INTO tipo_visita VALUES( 10 , 'Pagina transaccional', 20.0 , 'A');
INSERT INTO tipo_visita VALUES( 10 , 'Facebook', 20.0 , 'A');
INSERT INTO tipo_visita VALUES( 10 , 'Google', 20.0 , 'A');
INSERT INTO tipo_visita VALUES( 10 , 'Intagram', 20.0 , 'A');
INSERT INTO tipo_visita VALUES( 10 , 'Url directo', 20.0 , 'A');


commit;