USE MASTER;
--Se elimina la base datos
drop database EurekaBankSA;
GO
--Se vuelve a crear la base de datos
create database EurekaBankSA;
GO

--Se cambia de base de datos	
USE EurekaBankSA;
GO

--Creacion de tablas SA
CREATE TABLE cliente (
  ID int PRIMARY KEY,
  APELLIDO1 nvarchar(255),
  APELLIDO2 nvarchar(255),
  NOMBRE nvarchar(255),
  GENERO nvarchar(1),
  FECHA_NACIMIENTO date,
  FECHA_AFILIACION date,
  CIUDAD nvarchar(255),
  PROVINCIA nvarchar(255),
  TELEFONO nvarchar(255),
  EMAIL nvarchar(255),
  SUCURSAL_ID int
)
GO

CREATE TABLE movimiento_sucursal(
  ID int PRIMARY KEY,
  DESCRIPCION nvarchar(255),
  MONTO FLOAT,
  FECHA date,
  SUCURSAL_ID int,
  TIPO_MOVIMIENTO_ID int
)
GO

CREATE TABLE tipo_movsucursal(
  ID int PRIMARY KEY,
  DESCRIPCION nvarchar(255),
  TIPO_ACCION nvarchar(255),
  ESTADO nvarchar(1)
)
GO

CREATE TABLE alerta_fraude(
  ID int PRIMARY KEY,
  FECHA DATE,
  DETALLE nvarchar(255),
  TIPO nvarchar(255),
  CLIENTE_ID INT
)
GO

CREATE TABLE visita_sitio_web(
  ID int PRIMARY KEY,
  FECHA DATE,
  PAIS_ORIGEN nvarchar(255),
  DISPOSITIVO nvarchar(255),
  SO nvarchar(255),
  CLIENTE_ID INT,
  TIPO nvarchar(255)
)
GO

CREATE TABLE cuenta(
  ID int PRIMARY KEY,
  CLIENTE_ID INT,
  FECHA_CREACION DATE,
  ESTADO nvarchar(1),
  PRODUCTO_ID INT,
  MONEDA nvarchar(255)
)
GO

CREATE TABLE movimiento(
  ID int PRIMARY KEY,
  DETALLE nvarchar(255),
  MONTO FLOAT,
  SALDO FLOAT,
  FECHA DATE,
  CUENTA_ID INT,
  EMPLEADO_ID INT,
  TIPO_MOVIMIENTO nvarchar(255),
  ACCION nvarchar(255)
)
GO

----------------------------------------------------------------------------------------------------------------------------------------------
USE MASTER;
--Se elimina la base datos
drop database EurekaBankDW;
GO
--Se vuelve a crear la base de datos
create database EurekaBankDW;
GO

--Se cambia de base de datos	
USE EurekaBankDW;
GO

--Se crean las tablas de dimenciones
CREATE TABLE dim_cliente (
  id int PRIMARY KEY,
  nombre nvarchar(255),
  genero nvarchar(255),
  edad int,
  id_fecha_afiliacion int,
  ciudad nvarchar(255),
  provincia nvarchar(255),
  telefono nvarchar(255),
  email nvarchar(255),
  sucursal_id int
)
GO

CREATE TABLE dim_tiempo (
  id int PRIMARY KEY IDENTITY(1, 1),
  fecha datetime,
  annio int,
  vmes nvarchar(255),
  mes int,
  semana int,
  dia int,
  trimestre int
)
GO

CREATE TABLE fact_ingresos (
  id int PRIMARY KEY,
  monto float,
  fecha_id int,
  sucursal_id int,
  tipo_movimiento nvarchar(255)
)
GO

CREATE TABLE fact_gastos (
  id int PRIMARY KEY,
  monto float,
  fecha_id int,
  sucursal_id int,
  tipo_movimiento nvarchar(255)
)
GO

CREATE TABLE fact_visitas_sitio (
  id int PRIMARY KEY,
  fecha_id int,
  pais_origen nvarchar(255),
  dispositivo nvarchar(255),
  so nvarchar(255),
  cliente_id int,
  tipo nvarchar(255)
)
GO

CREATE TABLE fact_alerta_fraude (
  id int PRIMARY KEY,
  fecha_id int,
  detalle nvarchar(255),
  tipo nvarchar(255),
  cliente_id int
)
GO

CREATE TABLE fact_cuentas (
  id int PRIMARY KEY,
  fecha_creacion_id int,
  estado nvarchar(15),
  id_producto int,
  moneda nvarchar(255),
  cliente_id int
)
GO

CREATE TABLE dim_sucursal (
  id int PRIMARY KEY,
  ciudad nvarchar(255),
  provincia nvarchar(255),
  encargado_id int
)
GO

CREATE TABLE dim_producto (
  id int PRIMARY KEY,
  descripcion nvarchar(255),
  tasa float,
  tipo_interes nvarchar(255)
)
GO

CREATE TABLE fact_movimiento (
  id int PRIMARY KEY,
  detalle nvarchar(255),
  monto float,
  saldo float,
  fecha_id int,
  cuenta_id int,
  tipo_movimiento nvarchar(255),
  accion nvarchar(255)
)
GO

ALTER TABLE dim_cliente ADD FOREIGN KEY (id_fecha_afiliacion) REFERENCES dim_tiempo (id)
GO

ALTER TABLE fact_ingresos ADD FOREIGN KEY (fecha_id) REFERENCES dim_tiempo (id)
GO

ALTER TABLE fact_gastos ADD FOREIGN KEY (fecha_id) REFERENCES dim_tiempo (id)
GO

ALTER TABLE fact_visitas_sitio ADD FOREIGN KEY (fecha_id) REFERENCES dim_tiempo (id)
GO

ALTER TABLE fact_alerta_fraude ADD FOREIGN KEY (fecha_id) REFERENCES dim_tiempo (id)
GO

ALTER TABLE fact_cuentas ADD FOREIGN KEY (fecha_creacion_id) REFERENCES dim_tiempo (id)
GO

ALTER TABLE fact_alerta_fraude ADD FOREIGN KEY (cliente_id) REFERENCES dim_cliente (id)
GO

ALTER TABLE fact_visitas_sitio ADD FOREIGN KEY (cliente_id) REFERENCES dim_cliente (id)
GO

ALTER TABLE fact_cuentas ADD FOREIGN KEY (cliente_id) REFERENCES dim_cliente (id)
GO

ALTER TABLE fact_ingresos ADD FOREIGN KEY (sucursal_id) REFERENCES dim_sucursal (id)
GO

ALTER TABLE fact_gastos ADD FOREIGN KEY (sucursal_id) REFERENCES dim_sucursal (id)
GO

ALTER TABLE dim_cliente ADD FOREIGN KEY (sucursal_id) REFERENCES dim_sucursal (id)
GO

ALTER TABLE fact_cuentas ADD FOREIGN KEY (id_producto) REFERENCES dim_producto (id)
GO

ALTER TABLE fact_movimiento ADD FOREIGN KEY (fecha_id) REFERENCES dim_tiempo (id)
GO

ALTER TABLE fact_movimiento ADD FOREIGN KEY (cuenta_id) REFERENCES fact_cuentas (id)
GO

-- LLENADO DE LA TABLE TIEMPO
BEGIN
DECLARE @PFECINI DATE;
DECLARE @PFECFIN DATE;
SET @PFECINI='2009-01-01';
SET @PFECFIN='2019-11-01';
--SELECT @PFECINI=MIN(CAST(FECHA AS DATE)) FROM UTNSA.DBO.VENTAS;
WHILE @PFECINI <= @PFECFIN 
    BEGIN
    INSERT INTO DIM_TIEMPO(
    FECHA ,
    ANNIO ,
    vMES ,
    MES ,
    DIA ,
    SEMANA ,
    TRIMESTRE )

    SELECT 
    CAST(@PFECINI AS DATE),
    DATEPART(YEAR,@PFECINI),
    DATENAME(MONTH,@PFECINI),
    DATEPART(MONTH,@PFECINI),
    DATEPART(DAY,@PFECINI),
    DATEPART(WEEK,@PFECINI),
    DATEPART(QUARTER,@PFECINI)
    SET @PFECINI=DATEADD(DAY,1,@PFECINI);
    
    END; 
    
END;
GO
--CREAR VISTA CLIENTES CON EurekaBankSA
CREATE VIEW clientes AS
select ID, APELLIDO1, APELLIDO2, NOMBRE, GENERO, FECHA_NACIMIENTO, FECHA_AFILIACION, CIUDAD, PROVINCIA, TELEFONO, EMAIL, SUCURSAL_ID from EurekaBankSA.dbo.CLIENTE;
GO

--Crear vista movimiento_sucursal
CREATE VIEW movimiento_sucursal AS
select ID, DESCRIPCION, MONTO, FECHA, SUCURSAL_ID, TIPO_MOVIMIENTO_ID from EurekaBankSA.dbo.movimiento_sucursal; 
GO

--Crear vista tipo_movsucursal
CREATE VIEW tipo_movsucursal AS
select ID, DESCRIPCION, TIPO_ACCION, ESTADO from EurekaBankSA.dbo.tipo_movsucursal;
GO

--Crear vista alerta_fraude
CREATE VIEW alerta_fraude AS
	select ID, FECHA, DETALLE, TIPO, CLIENTE_ID from EurekaBankSA.dbo.alerta_fraude;
GO

--Crear vista visita_sitio_web
CREATE VIEW visita_sitio_web AS
	select ID, FECHA, PAIS_ORIGEN, DISPOSITIVO, SO, CLIENTE_ID, TIPO from EurekaBankSA.dbo.visita_sitio_web;
GO	

--crear vista de cuentas
CREATE VIEW cuentas AS
	select ID, CLIENTE_ID, FECHA_CREACION, ESTADO, PRODUCTO_ID, MONEDA from EurekaBankSA.dbo.cuenta;
GO

--Crear vista de movimientos
CREATE VIEW movimientos AS 
	select ID, DETALLE, MONTO, SALDO, FECHA, CUENTA_ID, TIPO_MOVIMIENTO, ACCION from EurekaBankSA.dbo.movimiento;
GO

--crear procedimiento de carga de DIM_CLIENTE
create procedure paCargaDimCliente as
begin
	insert into dim_cliente (id, nombre, genero, edad, id_fecha_afiliacion, ciudad, provincia, telefono, email, sucursal_id)
	  select c.id, CONCAT(c.nombre, ' ', c.apellido1, ' ', c.apellido2) AS 'nombre', 
	  CASE
		WHEN c.genero = 'M' THEN 'MASCULINO'
		WHEN c.genero = 'F' THEN 'FEMENINO'
		ELSE 'OTROS'
	  END AS genero, (DATEPART(YEAR, GETDATE())  - DATEPART(YEAR, c.fecha_nacimiento)) as edad, t.id as id_fecha_afiliacion,
	  c.ciudad, c.provincia, c.telefono, c.email, c.sucursal_id
		from clientes c
	   left join dim_tiempo t
		  on t.fecha = c.fecha_afiliacion
		where c.id not in (select id from dim_cliente) 
end
GO



--crear procedimiento de carga de fact_ingreso
create procedure paCargaFact_Ingreso as
begin
	insert into fact_ingresos(id, monto, fecha_id, sucursal_id, tipo_movimiento)
	  select m.id, m.monto, t.id as fecha_id, m.sucursal_id, tm.descripcion as tipo_movimiento
	  from movimiento_sucursal m
	   left join dim_tiempo t
		  on t.fecha = m.fecha
	   left join tipo_movsucursal tm
	      on tm.id = m.tipo_movimiento_id
	   where m.id not in (select id from fact_ingresos) and tm.tipo_accion = 'I'
end
GO

--crear procedimiento de carga de fact_gastos
create procedure paCargaFact_Gastos as
begin
	insert into fact_gastos(id, monto, fecha_id, sucursal_id, tipo_movimiento)
	  select m.id, m.monto, t.id as fecha_id, m.sucursal_id, tm.descripcion as tipo_movimiento
	  from movimiento_sucursal m
	   left join dim_tiempo t
		  on t.fecha = m.fecha
	   left join tipo_movsucursal tm
	      on tm.id = m.tipo_movimiento_id
	   where m.id not in (select id from fact_gastos) and tm.tipo_accion = 'G'
end
GO

--Crear procedimiento de carga fact_alerta_fraude
create procedure paCargaAlerta_Fraude as
begin
	insert into fact_alerta_fraude(id, fecha_id, detalle, tipo, cliente_id)
		select  a.ID, t.id as FECHA_ID, a.DETALLE, a.TIPO, a.CLIENTE_ID 
		 from alerta_fraude a
		 left join dim_tiempo t
		  on t.fecha = a.fecha
		where a.id not in (select id from fact_alerta_fraude) 
end
GO

--Crear procedimiento de carga fact_visita_sitio
create procedure paCargaVisita_Sitio_Web as
begin
	insert into fact_visitas_sitio(id, fecha_id, pais_origen, dispositivo, so, cliente_id, tipo)
		select  v.ID, t.id as FECHA_ID, v.pais_origen, v.dispositivo, v.so, v.CLIENTE_ID, v.tipo 
		 from visita_sitio_web v
		 left join dim_tiempo t
		  on t.fecha = v.fecha
		where v.id not in (select id from fact_visitas_sitio) 
end
GO

--Crear procedimiento de carga fact_cuentas
create procedure paCargaCuentas as
begin
	insert into fact_cuentas(id, fecha_creacion_id, estado, id_producto, moneda, cliente_id)
		select  c.ID, t.id as FECHA_CREACION_ID, CASE
			WHEN c.ESTADO = 'A' THEN 'ACTIVO'
			WHEN c.ESTADO = 'I' THEN 'INACTIVO'
			ELSE 'ACTIVO'
			END AS ESTADO, 
		C.PRODUCTO_ID AS ID_PRODUCTO, C.MONEDA, C.CLIENTE_ID
		 from cuentas c
		 left join dim_tiempo t
		  on t.fecha = c.fecha_creacion
		where c.id not in (select id from fact_cuentas) 
end
GO

--Crear procedimiento de carga fact_movimientos
create procedure paCargaFact_Movimientos as
begin
	insert into fact_movimiento(id, detalle, monto, saldo, fecha_id, cuenta_id, tipo_movimiento, accion)
		select m.id, m.detalle, m.monto, m.saldo, t.id as fecha_id, m.cuenta_id, m.tipo_movimiento, m.accion
		 from movimientos m
		 left join dim_tiempo t
		  on t.fecha = m.fecha
		where m.id not in (select id from fact_movimiento) 
end
GO
