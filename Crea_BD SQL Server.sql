/*
Empresa        :  EurekaBank
Software       :  Sistema de Cuentas de Ahorro
DBMS           :  SQLServer
Base de Datos  :  eurekabankdw
Script         :  Crea la Base de Datos
Creado por     :  Keilor Jiménez Quesada
*/
ALTER SESSION SET "_ORACLE_SCRIPT"=TRUE;

-- =============================================
-- CREACIÓN TABLAS DE DIMENCIONES
-- =============================================
1.dim_empleados
2.dim_cliente (kpi fidelidad, longevidd, mora,indice de clientes)
3.dim_producto
4.dim_sucursal
5.dim_tiempo
-- =============================================
-- CREACIÓN TABLAS DE HECHOS
-- =============================================
6.fact_ingresos (kpi ingresos)
7.fact_gastos (kpi gastos, indice costo marketing)
8.fact_visitas_sitio (kpi visitas)
9.fact_alerta_fraude (kpi indice fraude)
10.fact_cuentas


